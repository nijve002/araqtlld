import os
import re

from django.shortcuts import render, HttpResponse
from django.conf import settings

import main
import suggestions
from main.models import Experiment, GeneInfo, Species, Marker
from main import utils

import numpy as np

# Create your views here.

class TraitMarker:
    type = ""
    description = ""
    trait_name = ""
    LOD = 0
    chr = 0
    start = 0
    end = 0

class enriched_go_term:
    name = ""
    accession = ""
    domain = ""
    definition = ""
    pvalue = 1

def selectMarker(request):
    '''
    select a marker, experiment and threshold
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if request.GET.get('experiment_name') and request.GET.get('query'):
            experiment_name = request.GET.get('experiment_name')
            query = request.GET.get('query').strip()

            if not query:
                return render(request, 'peakmarker.html', {'experiments': experiments})

            lodthld = Experiment.objects.get(name=experiment_name).lodthld

            if request.GET.get('thld'):
                try:
                    lodthld = float(request.GET.get('thld'))
                except ValueError:
                    return HttpResponse('<h1> invalid LOD threshold </h1>')

            min_dist = 0

            if request.GET.get('min_dist'):
                try:
                    min_dist = int(request.GET.get('min_dist'))
                except ValueError:
                    min_dist = 0

            marker_name = None

            if Marker.objects.filter(name=query, experiment__name=experiment_name):
                marker_name = Marker.objects.filter(name=query)[0].name
            elif Marker.objects.filter(name__iexact=query, experiment__name=experiment_name):
                markers = Marker.objects.filter(name__iexact=query)
                marker_name = markers[0].name
            elif Marker.objects.filter(name__iexact=query):
                markers = Marker.objects.filter(name__iexact=query)
                marker = markers[0]
                chromosome = marker.chromosome.name
                position = marker.start
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
            elif re.match("(chr)?(\d+):(\d+)$", query,re.I):
                genomicLocation = re.match("(chr)?(\d+):(\d+)$", query,re.I)
                chromosome = genomicLocation.group(2)
                position = genomicLocation.group(3)
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name
            elif GeneInfo.objects.filter(gene_id = query):
                geneInfo = GeneInfo.objects.get(gene_id = query)
                chromosome = geneInfo.chr
                position = geneInfo.start
                closestMarker = utils.get_closest_marker(experiment_name, chromosome, position)
                if closestMarker:
                    marker_name = closestMarker.name

            if marker_name == None:
                ret = suggestions.views.suggest(query, experiment_name, "coregulation")
                if ret:
                    return ret
                else:
                    return main.views.no_results(query, "coregulation")

            traits = get_peaks_for_marker(experiment_name, marker_name,lodthld)
            
            marker = Marker.objects.get(name=marker_name,experiment__name=experiment_name)

            trait_list = list()
            gene_set = list()
            for trait in traits:
                t = TraitMarker()
                t.trait_id = trait
                t.LOD = traits[trait]
                gi = GeneInfo.objects.filter(gene_id=trait)
                if gi:
                    t.description = gi[0].description
                    t.type = "gene"
                    t.name = gi[0].gene_name
                    t.chr = gi[0].chr
                    t.start = gi[0].start
                    t.end = gi[0].end
                    if min_dist and t.start:
                        dist = abs(int(t.start) - marker.start)/1000000
                        if t.chr == marker.chromosome.name and dist < min_dist:
                            continue
                    gene_set.append(trait)
                    
                trait_list.append(t)

            enriched_set = utils.GO_enrichment(gene_set,experiment_name)

            enriched_go_terms = []
            for es in enriched_set:
                (go,pvalue) = enriched_set[es]
                e = enriched_go_term()
                e.name = go.name
                e.domain = go.domain
                e.definition = go.definition
                e.pvalue = pvalue
                enriched_go_terms.append(e)

            return render(request, 'peakmarker.html', {'experiment_name': experiment_name,
                                                          'experiments': experiments,
                                                          'species': species_short_name,
                                                          'marker': marker,
                                                          'title': marker.name,
                                                          'min_dist': min_dist,
                                                          'enriched_go_terms': enriched_go_terms,
                                                          'trait_list': sorted(trait_list, key=lambda x: x.LOD,
                                                                              reverse=True),
                                                          'lodthld': lodthld})

        else:
            return render(request, 'peakmarker.html', {'experiments': experiments})


def get_peaks_for_marker(experiment_name, marker_name, lodthld):
    marker = Marker.objects.get(experiment__name = experiment_name, 
        name = marker_name)
    markers = Marker.objects.filter(experiment__name = experiment_name, 
        chromosome = marker.chromosome)
    
    marker_names = [m.name for m in markers]
    lod_scores = utils.get_lodscores_for_experiment_pd(experiment_name)
    lod_scores_chr = lod_scores[marker_names]
    lod_scores_chr = lod_scores_chr[lod_scores_chr[marker_name].abs() > float(lodthld)]

    traits = lod_scores_chr[marker_name][lod_scores_chr.abs().max(axis=1) \
        - lod_scores_chr[marker_name].abs()  < 1.0]

    return traits.to_dict()

def get_peaks_for_marker_old(experiment_name, marker_name,lodthld):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    traits_pickle_path = os.path.join(experiment_data_path, '%s' % "traits.npy")
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")

    lod_scores = np.load(lod_pickle_path,allow_pickle=True)
    traits = np.load(traits_pickle_path,allow_pickle=True)
    markers = np.load(markers_pickle_path,allow_pickle=True).tolist()

    (before_marker,after_marker) = utils.get_neighboring_markers(experiment_name, marker_name)

    mi = markers.index(marker_name)
    marker_lodscores = dict(zip(traits,np.absolute(lod_scores[:,mi])))
    
    if before_marker:
	    mb = markers.index(before_marker.name)
	    before_lodscores = dict(zip(traits,np.absolute(lod_scores[:,mb])))

    if after_marker:
	    ma = markers.index(after_marker.name)
	    after_lodscores = dict(zip(traits,np.absolute(lod_scores[:,ma])))

    traits = {t: l for (t, l) in marker_lodscores.items() if l >= lodthld}

    # if the lod scores before or after the marker are higher, this is not a peak, 
    # so remove the gene from the list
    traits_to_remove = []
    for trait in traits:
        if (before_marker and marker_lodscores[trait] < before_lodscores[trait]) \
            or (after_marker and marker_lodscores[trait] < after_lodscores[trait]):
           traits_to_remove.append(trait)

    for trait in traits_to_remove:
        del traits[trait]

    return traits
