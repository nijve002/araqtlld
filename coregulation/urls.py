'''
Created on Apr 29, 2015

@author: jiao
'''

from django.urls import include, re_path
from coregulation.views import selectMarker
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = [
    re_path(r'^', selectMarker),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
