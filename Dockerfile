FROM ubuntu/apache2

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y ntp nano python3 python3-pip python3-venv libapache2-mod-wsgi-py3 libpq-dev

RUN python3 -m venv venv && \
    . venv/bin/activate && \
    pip install --upgrade pip && \
    pip install joblib goatools django django-cprofile-middleware pandas psycopg2 sqlalchemy SPARQLWrapper

EXPOSE 80
CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
