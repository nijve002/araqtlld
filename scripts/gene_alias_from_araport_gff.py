from sys import argv
import django
import re
import sys,os
sys.path.append('/local/prog/www/django/AraQTLdev')
os.environ['DJANGO_SETTINGS_MODULE'] = 'AraQTLdev.settings'
django.setup()

from main.models import GeneInfo
from main.models import GeneAlias
from django.core.exceptions import ObjectDoesNotExist


with open(argv[1]) as GFF_file:
    for line in GFF_file:
        fields = line.strip().split("\t",9)
        if len(fields) < 9 or fields[2] != "gene":
            continue 
        options = fields[8].split(";") 
        gene_info = dict([(option.split('=')) for option in options])
        chr = fields[0][3:]

        start = int(fields[3])
        end = int(fields[4])
        ID = gene_info["ID"]
        name = gene_info["Name"]
        symbol = gene_info.get("symbol","")
        description = gene_info.get("curator_summary",gene_info.get("computational_description",gene_info.get("Note","")))
        aliases = gene_info.get("Alias","")
        
        if aliases:
            try:
                gi = GeneInfo.objects.get(gene_id = ID)
            except ObjectDoesNotExist:
                print("creating new gene with ID: ",ID)
                gi = GeneInfo()
                gi.gene_id = ID
                gi.gene_name = symbol
                gi.description = description
                gi.chr = chr
                gi.start = start
                gi.end = end
                gi.save()
                
            for alias in re.split(r',\s*(?![^()]*\))',aliases):
                GeneAlias.objects.get_or_create(alias = alias, geneinfo = gi)
