from sys import argv
import re

with open(argv[1]) as GFF_file:
	for line in GFF_file:
		fields = line.strip().split("\t",9)
		if len(fields) < 9 or fields[2] != "gene":
			continue 
		options = fields[8].split(";") 
		gene_info = dict([(option.split('=')) for option in options])
		chr = fields[0][3:]
		
		start = int(fields[3])
		end = int(fields[4])
		ID = gene_info["ID"]
		name = gene_info["Name"]
		symbol = gene_info.get("symbol","")
		description = gene_info.get("curator_summary",gene_info.get("computational_description",gene_info.get("Note","")))
		print 'update main_geneinfo set chr = "%s", start = %d, end = %d, description = "%s", gene_name = "%s"  where gene_id = "%s";'%(chr, start, end, description, symbol, ID)
