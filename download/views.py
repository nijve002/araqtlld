from django.shortcuts import HttpResponse
import re

def index(request):
    download_data = ""
    if request.method == 'GET':
        if request.GET.get('data'):
            download_data = request.GET.get('data');
            download_data = re.sub(",","\n",download_data)

    return HttpResponse(download_data, content_type='text/plain')
