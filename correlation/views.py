import os

from django.shortcuts import render, HttpResponse
from django.conf import settings

from suggestions.views import suggest
from main.views import no_results
from main import utils
from main.models import Experiment, GeneInfo, Species, Metabolite, Phenotype

import numpy
import re
import json

import numpy as np
from scipy import stats


# Create your views here.

class trait_info:
    trait_id = ""
    name = ""
    description = ""
    chr = 0
    start = 0
    correlation = 0
    type = ""

class enriched_go_term:
    name = ""
    accession = ""
    domain = ""
    definition = ""
    pvalue = 1

def correlation(request):
    '''
    select a gene and experiment, and find the top genes that show correlation
    
    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    methods = ("Pearson","Spearman")

    if request.method == 'GET':
        if request.GET.get('query'):
            if request.GET.get('experiment_name'):
                experiment_name = request.GET.get('experiment_name')
            else:
                experiment_name = experiments[0].name
            query_id = request.GET.get('query').strip()

            if request.GET.get('method'):
                method = request.GET.get('method')
            else:
                method = methods[0]

            if not query_id:
                return render(request, 'correlation.html', {'experiments': experiments})

            query_metabolite = utils.get_metabolite(query_id, experiment_name)
            query_phenotype = utils.get_phenotype(query_id, experiment_name)
            query_gene = utils.get_gene(query_id, experiment_name)

            query_trait = dict()

            if query_metabolite:
                query_trait["trait_id"] = query_metabolite.name
            elif query_phenotype:
                query_trait["trait_id"] = query_phenotype.name
            elif query_gene:
                query_trait["trait_id"] = query_gene.gene_id
                query_trait["name"] = query_gene.gene_name
                query_trait["chromosome"] = query_gene.chr
                query_trait["start"] = query_gene.start
            else:
                ret = suggest(query_id, experiment_name, "correlation")
                if ret:
                    return ret
                else:
                    return no_results(query_id, "correlation")

            corrthld = 0.9
            if request.GET.get('corrthld'):
                try:
                    corrthld = float(request.GET.get('corrthld'))
                    if corrthld > 1:
                        corrthld = 1
                    elif corrthld < -1:
                        corrthld = -1
                except ValueError:
                    return HttpResponse('<h1> invalid correlation threshold </h1>')

            traits = getCorrelatingTraits(query_trait["trait_id"].upper(),experiment_name,corrthld,method)

            trait_list = list()

            gene_set = []

            for trait in traits:
                ti = trait_info()
                ti.trait_id = trait
                ti.correlation = traits[trait]

                gene = utils.get_gene(trait)
                metabolite = utils.get_metabolite(trait, experiment_name)
                phenotype = utils.get_phenotype(trait, experiment_name)

                if gene:
                    gene_set.append(gene.gene_id)
                    ti.trait_id = gene.gene_id
                    ti.name = gene.gene_name
                    ti.description = gene.description
                    ti.chr = gene.chr
                    ti.start = gene.start
                    ti.type = "gene"
                elif phenotype:
                    ti.description = phenotype.description
                    ti.type = "phenotype"
                elif metabolite:
                    ti.description = metabolite.description
                    ti.type = "Metabolite"
                trait_list.append(ti)
            
            enriched_set = utils.GO_enrichment(gene_set,experiment_name)
            
            enriched_go_terms = []
            for es in enriched_set:
                (go,pvalue) = enriched_set[es]
                e = enriched_go_term()
                e.name = go.name
                e.domain = go.domain
                e.definition = go.definition
                e.pvalue = pvalue
                enriched_go_terms.append(e)
            

            return render(request, 'correlation.html', {'experiment_name': experiment_name,
                                                           'experiments': experiments,
                                                           'species': species_short_name,
                                                           'query_trait': query_trait,
                                                           'trait_list': sorted(trait_list, key=lambda x: x.correlation,
                                                                               reverse=True),
                                                           'enriched_go_terms': sorted(enriched_go_terms, key=lambda x: x.pvalue),
                                                           'methods':methods,
                                                           'current_method':method,
                                                           'corrthld': corrthld})
        else:
            return render(request, 'correlation.html', {'experiments': experiments})


def getCorrelatingTraits(trait, exp_name, corrthld,method="Pearson"):
    lod_scores = utils.get_lodscores_for_experiment_np(exp_name)
    if method == "Spearman":
        lod_scores = np.apply_along_axis(stats.rankdata,1,lod_scores,"min")
    traits = utils.load_traitlist_for_experiment(exp_name)

    ms = lod_scores.mean(axis=1)[(slice(None, None, None), None)]
    datam = lod_scores - ms
    datass = np.sqrt(np.sum(datam*datam, axis=1))
    trait_index = traits.tolist().index(trait)
    temp = np.dot(datam, datam[trait_index].T)
    rs = temp / (datass * datass[trait_index])

    return dict(zip(traits[rs>corrthld],rs[rs>corrthld]))



