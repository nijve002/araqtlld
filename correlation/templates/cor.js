(function() {
var draw;

draw = function(data) {
var altpink, axislabels, bigRad, bottom, c, checkerboard, chrGap, chrLowXScale, chrXScale, chrYScale, chrindex, 
    ci, cj, cur, darkGray, darkblue, darkgreen, darkred, draw_probe, g, gene, h, hLegend, i, j, labelcolor, legend, 
    left, leftLegend, lightGray, m, maincolor, martip, maxlod, newg, nodig, onedig, p, pad, pink, probe, purple, 
    right, rightLegend, svg, tickHeight, titlecolor, top, topLegend, totalChrLength, totalh, totalw, twodig, 
    w, wLegend, xloc, yloc, _i, _j, _k, _l, 
    _len, _len1, _len10, _len2, _len3, _len4, _len5, _len6, _len7, _len8, _len9, 
     _m, _n, _o, _p, _q, _r, _ref, _ref1, _ref10, _ref4, _ref5, _ref8, _ref9, _s;

totalw = 1000;
totalh = 400;
chrGap = 8;
bigRad = 8;

pad = {
  left: 60,
  top: 40,
  right: 40,
  bottom: 40,
  inner: 10
};

left = 60;
right = 760;
leftLegend = right + 10;
top = 41;
topLegend = 41;
bottom = 341;
h = 300;
hLegend = 200;
w = 700;
wLegend = 300;

//NUMBER FORMATING////////////////// 
nodig = d3.format(".0f");
onedig = d3.format(".1f");
twodig = d3.format(".2f");
//END///////////////////////////////

//SET DEFAULT COLOR/////////////////
lightGray = d3.rgb(230, 230, 230);
darkGray = d3.rgb(200, 200, 200);
darkblue = "darkslateblue";
darkgreen = "darkslateblue";
pink = "hotpink";
altpink = "#E9CFEC";
purple = "#8C4374";
darkred = "crimson";
labelcolor = "black";
titlecolor = "blue";
	maincolor = "darkblue";
	//END///////////////////////////////
	
	svg = d3.select("div#multiexperimentplot").append("svg").attr("width", totalw).attr("height", totalh);
	svg.append("rect").attr("x", left).attr("y", top).attr("height", h).attr("width", w).attr("class", "innerBox");
	svg.append("rect").attr("x", leftLegend).attr("y", topLegend).attr("height", hLegend).attr("width", wLegend).attr("class", "innerBox");
	
	//#######################end 1###################################################
	
	//###########################2.define chessboard layout##########################
	
	checkerboard = svg.append("g").attr("id", "checkerboard");// define plot as checkerboard
	legend = svg.append("g").attr("id", "legend");// define legend
	
	//##########  Calculate total chromosome length (in bp) SUM #########
	totalChrLength = 0;
	_ref = data.chrnames; //{"chr" : {"1": {"start": 3631,"end": 30425192}...}
	for (_i = 0, _len = _ref.length; _i < _len; _i++) {
	  c = _ref[_i];
	  //chromosome length in cM
	  data.chr[c].length_bp = data.chr[c].end - data.chr[c].start;
	  totalChrLength += data.chr[c].length_bp;
	}
	//#################################end###############################
	
       
	//################################lower plot chess board box property############################
	chrLowXScale = {};  
	cur = Math.round(pad.left + chrGap / 2); // (60+8/2)
	_ref4 = data.chrnames;
	for (_k = 0, _len4 = _ref4.length; _k < _len4; _k++) {
	  c = _ref4[_k];
	  data.chr[c].start_lowerXpixel = cur; // 64
	  // 64 + round(900-8*nr_chr/total_chr_length*chr_cM)
	  data.chr[c].end_lowerXpixel = cur + Math.round((w - chrGap * data.chrnames.length) / totalChrLength * data.chr[c].length_bp);
	  chrLowXScale[c] = d3.scale.linear().domain([data.chr[c].start, data.chr[c].end]).range([data.chr[c].start_lowerXpixel, data.chr[c].end_lowerXpixel]);
	  cur = data.chr[c].end_lowerXpixel + chrGap; // each loop cursor move to the end position +2
	}
	//#####################################end#######################################
	
	//###################draw chess board layout for lower plot#################
	_ref5 = data.chrnames;
	for (i = _p = 0, _len5 = _ref5.length; _p < _len5; i = ++_p) {
	  ci = _ref5[i];
	  if (i % 2 === 0) {
	checkerboard.append("rect").
	    attr("x", data.chr[ci].start_lowerXpixel - chrGap / 2).
	    attr("width", data.chr[ci].end_lowerXpixel - data.chr[ci].start_lowerXpixel + chrGap).
	    attr("y", top).
	    attr("height", h).
	    attr("stroke", "none").
	    attr("fill", darkGray).
	    style("pointer-events", "none");
	  }
	}
	//###################################end#########################################
	
	//#########################draw axis of plot###########################
	
	//################################### X axis #######################################

	axislabels = svg.append("g").attr("id", "axislabels").style("pointer-events", "none");
	
	//################################### END #######################################
	
	//################################### Y axis #######################################
	
	//################# x axis text of plot################
	axislabels.append("g").attr("id", "bottomX").selectAll("empty").data(data.chrnames).enter().append("text").text(function(d) {
	  return d;
	}).attr("x", function(d) {
	  return (data.chr[d].start_lowerXpixel + data.chr[d].end_lowerXpixel) / 2;
	}).attr("y", bottom + pad.bottom * 0.3).attr("fill", labelcolor);

//################################### END #######################################

//############################### add x-axis name #######################################
axislabels.append("text").text("Marker position").
    attr("x", (left + right) / 2).
    attr("y", bottom + pad.bottom * 0.75).
    attr("fill", titlecolor).
    attr("text-anchor", "middle");

//############################### add Y-axis name #######################################
xloc = left - pad.left * 0.65;
yloc = (top + bottom) / 2;
axislabels.append("text").text("LOD score").
    attr("x", xloc).
    attr("y", yloc).
    attr("transform", "rotate(270," + xloc + "," + yloc + ")").
    style("text-anchor", "middle").
    attr("fill", titlecolor);
//################################### END #######################################



//marker tooltip//////////////////////
martip = d3.tip().attr("class", "d3-tip")
	.offset([-10, 0])
	.html(function(y,z) {
  return "marker: " +y + " (" + z + ")<br><i>click for genes peaking here</i>";
})
//end/////////////////////////////////

cur = 0;
_ref8 = data.chrnames;
for (_q = 0, _len8 = _ref8.length; _q < _len8; _q++) {
  c = _ref8[_q];
  _ref9 = data.pmarknames[c];
  for (_r = 0, _len9 = _ref9.length; _r < _len9; _r++) {
    p = _ref9[_r];
    data.pmark[p].index = cur;
    cur++
  }
}

//######################draw dot plot#########################
draw_plot = function(error, probe_data_set) {
  if (error) throw error; 
  
  var chr, curves, ensembl, lod, lodcurve, lodcurve_yScale, colors,
	markerClick, minlod,minlod_marker,maxlod_marker, meanmarks, mgi, 
	pos, probeaxes, ticks, title, titletxt, xlink, yaxis, probe_data,
	_len10, _len11, _len12, _len13, _ref10, _ref11, _ref12,_ref13, _s, _t, _u, _v;

  probe_data = probe_data_set[0]
  maxlod = -1;
  maxlod_marker = null;
  _ref10 = data.markers;
  for (_s = 0, _len10 = _ref10.length; _s < _len10; _s++) {
    m = _ref10[_s];
    _ref13 = probe_data_set
    for (_v = 0, _len13 = _ref13.length; _v < _len13; _v++) { 
      lod = probe_data_set[_v].lod[data.pmark[m].index];
      if (maxlod < lod) {
	maxlod = lod;
	maxlod_marker = m;
      }
    }
  }
  //############################map y axis range to the width of the lower plot#############################
  
  lodcurve_yScale = d3.scale.linear().domain([0, maxlod * 1.05]).range([bottom, top]);//540,620
  //add a container to group objects, in this case add y axis of lower plot
  yaxis = svg.append("g").attr("class", "probe_data").attr("id", "loweryaxis");
  ticks = lodcurve_yScale.ticks(6);
  yaxis.selectAll("empty").data(ticks).enter().append("line").attr("y1", function(d) {
    return lodcurve_yScale(d);
  }).attr("y2", function(d) {
    return lodcurve_yScale(d);
  }).attr("x1", left).attr("x2", right).attr("stroke", "white").attr("stroke-width", "1");
  yaxis.selectAll("empty").data(ticks).enter().append("text").text(function(d) {
    if (maxlod > 10) { // for the sacle of y axis of lower plot
      return nodig(d);
    } else {
      return onedig(d); // if maxlod<10: rescale y axis with one digit numbers
    }
  }).attr("y", function(d) {
    return lodcurve_yScale(d);
  }).attr("x", left - pad.left * 0.1).style("text-anchor", "end").attr("fill", labelcolor);
  lodcurve = function(c,i) {
    return d3.svg.line().x(function(p) {
      return chrLowXScale[c](data.pmark[p].start);
    }).y(function(p) {
      return lodcurve_yScale(probe_data_set[i].lod[data.pmark[p].index]);                          
    });
  };
  
  //#######################################end#################################################
  
  
//##########  draw legend #########
 
  colors = d3.scale.category10();

  _ref13 = probe_data_set;
  
  for (_v = 0, _len13 = _ref13.length; _v < _len13; _v++) {
	  legend.append("line").
	    attr("x1",leftLegend + 10).
	    attr("y1",topLegend + 18*_v + 15).
	    attr("x2",leftLegend + 40).
	    attr("y2",topLegend + 18*_v + 15).
	    attr("class", "thickline").
	    attr("stroke", colors(_v));

	    legend.append("text").text(probe_data_set[_v].probe).
	    attr("x", leftLegend + 50).
	    attr("y", topLegend + 18*_v+ 15).
	    style("text-anchor", "start").
	    attr("fill", titlecolor);
  }

//#################################end###############################

  
  curves = svg.append("g").attr("id", "curves").attr("class", "probe_data");
  _ref11 = data.chrnames;
  _ref13 = probe_data_set;

  for (_v = 0, _len13 = _ref13.length; _v < _len13; _v++) {
  for (_t = 0, _len11 = _ref11.length; _t < _len11; _t++) {
    c = _ref11[_t];
    curves.append("path").datum(data.pmarknames[c]).
	attr("d", lodcurve(c,_v)).
	attr("class", "thickline").
	attr("stroke", colors(_v)).
	style("pointer-events", "none").attr("fill", "none");
  }
  }
  titletxt = probe_data_set[0].probe.toUpperCase();
  
  probeaxes = svg.append("g").attr("id", "probe_data_axes").attr("class", "probe_data");
  trans = data.spot[titletxt].transcript;
  var link_pre;
  if (data["spec"] == "Arabidopsis Thaliana"){
    link_pre = "http://plants.ensembl.org/Arabidopsis_thaliana/Gene/Summary?g=";
  }
  if (data["spec"] == "Caenorhabditis Elegans"){
    link_pre = "http://www.wormbase.org/species/c_elegans/gene/";
  }
  var hyper_link = link_pre+ trans;
  if (trans !== null) {
    titletxt += " (" + trans + ")";
    //open window in a new tab <a href="#" target="_blank">Link</a>
    xlink = probeaxes.append("a").attr("target","_blank").attr("xlink:href", hyper_link);
    xlink.append("text").text(titletxt).
	attr("x", (left + right) / 2).
	attr("y", top - pad.top / 2).
	attr("fill", maincolor).style("font-size", "18px");
  } else {
    probeaxes.append("text").text(titletxt).
	attr("x", (left + right) / 2).
	attr("y", top - pad.top / 2).
	attr("fill", maincolor).style("font-size", "18px");
  }
  svg.append("rect").
    attr("class", "probe_data").
    attr("x", left).
    attr("y", top).
    attr("height", h).
    attr("width", w).
    attr("class", "outerBox");

  svg.append("circle").attr("class", "probe_data").
    attr("id", "probe_circle").
    attr("cx", chrLowXScale[data.spot[probe_data_set[0].probe.toUpperCase()].chr](data.spot[probe_data_set[0].probe.toUpperCase()].start)).
    attr("cy", top).
    attr("r", bigRad).
    attr("fill", pink).
    attr("stroke", darkblue).
    attr("stroke-width", 1).
    attr("opacity", 1);

  //############# Calculate the number of markers and save to markerClick with default value of 0 ###################
  markerClick = {};
  
  _ref12 = data.markers;
  for (_u = 0, _len12 = _ref12.length; _u < _len12; _u++) {
    m = _ref12[_u];
    markerClick[m] = 0;
  }
  //########################## end ############################
  
  _ref13 = probe_data_set;
  svg.call(martip)
  {% for gene in gene_list|slice:":10" %}
	  svg.append("g").
	    attr("id", "markerCircle").
	    attr("class", "probe_data").selectAll("empty").
	    data(data.markers).enter().append("circle").
	    attr("class", "probe_data").attr("id", function(td) {
		return "marker_" + td;
	    }).attr("cx", function(td) {
		return chrLowXScale[data.pmark[td].chr](data.pmark[td].start);
	    }).attr("cy", function(td) {
		return lodcurve_yScale(probe_data_set[{{ forloop.counter0 }}].lod[data.pmark[td].index]);
	    }).attr("r", bigRad).attr("fill", purple).attr("stroke", "none").attr("stroke-width", "2").attr("opacity", 0).on("mouseover", function(td) {
		if (!markerClick[td]) {
		    d3.select(this).attr("opacity", 1);
		}
		return martip.show(td,probe_data_set[{{ forloop.counter0 }}].probe);
	    }).on("mouseout", function(td) {
		d3.select(this).attr("opacity", markerClick[td]);
		return martip.hide();
	    }).on("click", function(d) {
		location.href="/AraQTLdev/coregulation/?experiment_name={{ experiment_name }}&marker="+d;
	    });
	    //end draw LOD score plot
 {% endfor %}
};

    data_set = [];

    queue()
    {% for gene in gene_list|slice:":10" %}
       .defer(d3.json,"../media/data/{{ experiment_name }}/probe/{{ gene.transcript_name|upper }}.json")
    {% endfor %}
       .awaitAll(draw_plot);

};
d3.json("../media/data/{{ experiment_name }}/lod.json", draw);
}).call(this);




