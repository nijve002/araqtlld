import urllib

from django.shortcuts import render, HttpResponse

from main.models import GeneInfo, GO, GeneGO, Experiment, Metabolite, Phenotype

class GOTerm:
    accesssion = ""
    name = ""
    definition = ""
    domain = ""
    gene_count = 0

def index(request):
    if request.method == 'GET':

        experiment_name = None
        if request.GET.get('query'):
            query = request.GET.get('query')
            query = urllib.parse.unquote(query).strip()
            if  request.GET.get('experiment_name'):
                experiment_name = request.GET.get('experiment_name')

            return suggest(query,experiment_name, None)

    return suggest(None,None,None)

def suggest(query, experiment_name, mode):
    species_name = "Arabidopsis thaliana"
    experiments = Experiment.objects.filter(species__species_name=species_name)

    if not query:
        return render(None, 'suggestions.html', {'experiments': experiments})

    GO_terms = list()
    genes = list()
    metabolites = list()
    phenotypes = list()

    go_results = GO.objects.raw(
        f"select * from main_go WHERE to_tsvector(name || definition) @@ plainto_tsquery('{query}')")

    for go in go_results:
        go_term = GOTerm()
        go_term.accession = go.accession
        go_term.name = go.name
        go_term.definition = go.definition
        go_term.domain = go.domain
        go_term.gene_count = GeneGO.objects.filter(term_accession=go_term.accession)\
            .values("geneinfo_id").distinct().count()
        GO_terms.append(go_term)

    gene_results = GeneInfo.objects.raw(
        "select * from main_geneinfo WHERE to_tsvector(gene_name || description) @@ plainto_tsquery('%s')" % query)

    for gene in gene_results:
        genes.append(gene)

    gene_substring_results = GeneInfo.objects.filter(gene_name__icontains = query)

    for gene in gene_substring_results:
        genes.append(gene)

    genes = list(set(genes))

    metabolite_substring_results = Metabolite.objects.raw(
        "select * from main_metabolite WHERE to_tsvector(name || description) @@ plainto_tsquery('%s')" % query)

    for metabolite in metabolite_substring_results:
        metabolites.append(metabolite)

    phenotype_substring_results = Phenotype.objects.raw(
        "select * from main_phenotype WHERE to_tsvector(name || description) @@ plainto_tsquery('%s')" % query)

    for phenotype in phenotype_substring_results:
        phenotypes.append(phenotype)

    if len(genes) == 0 and len(GO_terms) == 0 and len(phenotypes) == 0 and len(metabolites) == 0:
        return None

    if  not experiment_name:
        experiment_name = experiments[0].name

    return render(None, 'suggestions.html', {'Genes': genes,
                                                   'GOterms': GO_terms,
                                                   'Metabolites': metabolites,
                                                   'Phenotypes': phenotypes,
                                                   'mode': mode,
                                                   'experiments': experiments,
                                                   'experiment_name': experiment_name})


