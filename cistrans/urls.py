'''
Created on Apr 29, 2015

@author: jiao
'''

from django.urls import include, re_path
from cistrans.views import eQTLPlotView
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = [
    re_path(r'^', eQTLPlotView),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
