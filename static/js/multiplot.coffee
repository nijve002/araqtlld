# multiplot.coffee
#
# Interactive eQTL plot
#
# adapted from https://github.com/kbroman/d3examples/tree/master/cistrans
#

# function that does all of the work
peak_chr = 0
left_position = 0 
right_position = 0
chrXScale = {}

draw = (data) ->
  bLegend = traits.length > 1
  d3.select("div#loading").remove()
  d3.select("div#legend").style("opacity", 1) if traits.length > 1
  d3.select("div#geneinput").style("opacity", 1)

  # dimensions of panels
  w = 700
  h = 300
  pad = {left:60, top:40, right:40, bottom: 40, inner: 10}

  left = 60
  right = left + w
  top =  41
  bottom = top + h
  if bLegend
      leftLegend = right + 10
      topLegend = top
      hLegend = h
      wLegend = 230
      maxLegendLines = 15

  totalw = w + if bLegend then wLegend + 110 else 100
  totalh = h + 100
  peakRad = 2
  bigRad = 5

  # gap between chromosomes
  chrGap = 8

  # height of marker ticks
  tickHeight = (bottom - top)*0.02
  nodig = d3.format(".0f")
  onedig = d3.format(".1f")
  twodig = d3.format(".2f")

  # colors definitions
  darkGray = d3.rgb(200, 200, 200)
  pink = "hotpink"
  darkblue = "darkslateblue"
  purple = "#8C4374"
  labelcolor = "black"   # "white"
  titlecolor = "blue"    # "Wheat"
  maincolor = "darkblue" # "Wheat"

  # calculate X and Y scales, using bp positions
  totalChrLength = 0
  for c in data.chrnames
    data.chr[c].length_bp = data.chr[c].end - data.chr[c].start
    totalChrLength += data.chr[c].length_bp

  curXPixel = left+peakRad
  curYPixel = bottom-peakRad
  for c in data.chrnames
    data.chr[c].length_pixel = Math.round((w-peakRad*2) * data.chr[c].length_bp / totalChrLength)
    data.chr[c].start_Xpixel = curXPixel
    data.chr[c].end_Xpixel = curXPixel + data.chr[c].length_pixel - 1
    data.chr[c].start_Ypixel = curYPixel
    data.chr[c].end_Ypixel = curYPixel - (data.chr[c].length_pixel - 1)

    curXPixel += data.chr[c].length_pixel
    curYPixel -= data.chr[c].length_pixel

  # slight adjustments
  data.chr["1"].start_Xpixel = left
  data.chr["1"].start_Ypixel = bottom

  # chr scales 
  cur = Math.round(pad.left + chrGap/2)
  for c in data.chrnames
    data.chr[c].start_Xpixel = cur
    data.chr[c].end_Xpixel = cur + Math.round((w-chrGap*(data.chrnames.length))/totalChrLength*data.chr[c].length_bp)
    chrXScale[c] = d3.scaleLinear()
                        .domain([data.chr[c].start, data.chr[c].end])
                        .rangeRound([data.chr[c].start_Xpixel, data.chr[c].end_Xpixel])
                        .clamp(true)
    cur = data.chr[c].end_Xpixel + chrGap

  # create SVG
  d3.select("svg").remove()
  svg = d3.select("div#multiplot").append("svg")
       .attr("preserveAspectRatio", "xMinYMin meet")
       .attr("viewBox", "0 0 #{totalw} #{totalh}")
       .classed("svg-content", true);

  # gray backgrounds
  svg.append("rect")
       .attr("id","plotarea")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "innerBox")

  if bLegend
      svg.append("rect")
        .attr("x", leftLegend)
        .attr("y", topLegend)
        .attr("height", hLegend)
        .attr("width", wLegend)
        .attr("class", "innerBox")
      legend = svg.append("g").attr("id", "legend")

  # add dark gray rectangles to define chromosome boundaries as checkerboard
  checkerboard = svg.append("g").attr("id", "checkerboard")
  for ci,i in data.chrnames
      if(i % 2 == 0)
        checkerboard.append("rect")
           .attr("x", data.chr[ci].start_Xpixel - chrGap/2)
           .attr("width", data.chr[ci].end_Xpixel - data.chr[ci].start_Xpixel + chrGap)
           .attr("y", top)
           .attr("height", h)
           .attr("stroke", "none")
           .attr("fill", darkGray)
           .style("pointer-events", "none")

  axislabels = svg.append("g").attr("id", "axislabels").style("pointer-events", "none")
  axislabels.append("g").attr("id", "bottomX").selectAll("empty")
     .data(data.chrnames)
     .enter()
     .append("text")
     .text((d) -> d)
     .attr("x", (d) -> (data.chr[d].start_Xpixel + data.chr[d].end_Xpixel)/2)
     .attr("y", bottom + pad.bottom*0.3)
     .attr("fill", labelcolor)
  axislabels.append("text")
     .text("Chromosome")
     .attr("x", (left + right)/2)
     .attr("y", bottom + pad.bottom*0.75)
     .attr("fill", titlecolor)
     .attr("text-anchor", "middle")
  xloc = left - pad.left*0.65
  yloc = (top + bottom)/2
  axislabels.append("text")
     .text("LOD score")
     .attr("x", xloc)
     .attr("y", yloc)
     .attr("transform", "rotate(270,#{xloc},#{yloc})")
     .style("text-anchor", "middle")
     .attr("fill", titlecolor)


  # function for drawing lod curve for trait
  draw_trait = (trait_data_set) ->
    martip = d3.tip()
             .attr("class", "d3-tip")
         .offset([-10, 0])
             .html((EVENT,d) ->  "marker: " + d[0] + " (" + d[3] + ")<br>position: " + d[1] + 
				"<br>LOD score: " + d[2] + "<br><i>click for traits peaking here</i>")

    # find marker with maximum LOD score
    maxlod = -1
    for pd in trait_data_set
       for marker in Object.keys(pd.lodscores)
          lod = pd.lodscores[marker].lod 
          if maxlod < Math.abs(lod)
             maxlod = Math.abs(lod)
             peakmarker = marker

    # y-axis scale
    lodcurve_yScale = d3.scaleLinear()
                        .domain([0, maxlod*1.05])
                        .range([bottom, top])

    # y-axis
    yaxis = svg.append("g").attr("class", "trait_data").attr("id", "yaxis")
    ticks = lodcurve_yScale.ticks(6)
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("line")
         .attr("y1", (d) -> lodcurve_yScale(d))
         .attr("y2", (d) -> lodcurve_yScale(d))
         .attr("x1", left)
         .attr("x2", right)
         .attr("stroke", "white")
         .attr("stroke-width", "1")
    yaxis.selectAll("empty")
         .data(ticks)
         .enter()
         .append("text")
         .text((d) ->
            return if maxlod > 10 then nodig(d) else onedig(d))
         .attr("y", (d) -> lodcurve_yScale(d))
         .attr("x", left - pad.left*0.1)
         .style("text-anchor", "end")
         .attr("fill", labelcolor)

    # lod curves by chr
    lineColors = d3.scaleOrdinal(d3.schemeCategory10)

    lodcurve = (c,ls) ->
        d3.line()
          .x((p) -> chrXScale[c](ls[p].start))
          .y((p) -> lodcurve_yScale(Math.abs(ls[p].lod)))
    curves = svg.append("g").attr("id", "curves").attr("class", "trait_data")
    for trait_data, i in trait_data_set
      for c in data.chrnames
        markers_for_chromosome = do(c)->m for m,ls of trait_data.lodscores when ls.chr is c
        markers_for_chromosome.sort((a,b)-> trait_data.lodscores[a].start - trait_data.lodscores[b].start)

        prev_marker = null
        start_chr = data.chr[c].start
        end_chr = data.chr[c].end

        markers_for_chromosome.forEach((m) => 
          trait_data.lodscores[m].prev = start_chr;
          trait_data.lodscores[m].next = end_chr;
          if prev_marker
            trait_data.lodscores[m].prev = trait_data.lodscores[prev_marker].start
            trait_data.lodscores[prev_marker].next = trait_data.lodscores[m].start
          prev_marker = m
        )

        curves.append("path")
            .datum(markers_for_chromosome)
            .attr("id",trait_data.ID+"-"+trait_data.experiment)
            .attr("d", lodcurve(c,trait_data.lodscores))
            .attr("class", "thickline")
            .attr("stroke", lineColors(i))
            .style("pointer-events", "none")
            .attr("fill", "none")

        if trait_data_set.length == 1
           pd = trait_data_set[0]
           if pd.lodscores[peakmarker].chr == c
              peak_chr = pd.lodscores[peakmarker].chr
              left_position = pd.lodscores[peakmarker].prev
              right_position = pd.lodscores[peakmarker].next

    # black border
    svg.append("rect").attr("class", "trait_data")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "outerBox")

    if dot? && dot.chr? && dot.chr != 0
      dottip = d3.tip()
         .attr("class", "d3-tip").offset([-10, 0])
         .html((EVENT,d) ->  dot.label + " (" + dot.chr + ":" + dot.pos + ")")

      svg.call(dottip)
      svg.append("circle").attr("class", "trait_data")
         .attr("id", "circle")
         .attr("cx", chrXScale[dot.chr](dot.pos))
         .attr("cy", top)
         .attr("r", bigRad)
         .attr("fill", pink)
         .attr("stroke", darkblue)
         .attr("stroke-width", 1)
         .attr("opacity", 1)
         .on "mouseover", () -> dottip.show(event)
         .on "mouseout", () -> dottip.hide()
    else
      for trait_data,i in trait_data_set
         if trait_data.chr?
            do(trait_data)->
              svg.append("circle")
                 .attr("id", "trait_#{trait_data.ID}")
                 .attr("cx", chrXScale[trait_data.chr](trait_data.start))
                 .attr("cy", top)
                 .attr("r", bigRad)
                 .attr("fill",  lineColors(i))
                 .attr("stroke", darkblue)
                 .attr("stroke-width", 1)
                 .attr("opacity", 1)

    svg.call(martip)

    # dots at markers on LOD curves
    for trait_data in trait_data_set
      do(trait_data)->
        svg.append("g").attr("id", "markerCircle").attr("class", "trait_data")
         .selectAll("empty")
         .data(Object.entries(trait_data.lodscores))
         .enter()
         .append("circle")
         .attr("class", "trait_data")
         .attr("id", (td) -> "marker_#{td[0]}")
         .attr("cx", (td) -> chrXScale[td[1].chr](td[1].start))
         .attr("cy", (td) -> lodcurve_yScale(Math.abs(td[1].lod)))
         .attr("r", bigRad)
         .attr("fill", purple)
         .attr("stroke", "none")
         .attr("stroke-width", "2")
         .attr("opacity", 0)
         .on "mouseover", (event,td) ->
              d3.select(this).attr("opacity", 1) 
              if multiexperiment
                  label = trait_data.experiment
              else
                  label = trait_data.ID

              martip.show(event,[td[0],td[1].chr+":"+td[1].start,Math.round(td[1].lod*100)/100,label])
          .on "mouseout", ((td) ->
              d3.select(this).attr("opacity", 0)
              martip.hide())
          .on "click", (event,td) -> 
              el = document.getElementById("collapse-form")
              if ($("#network-modal").is(":visible"))
                 start = td[1].prev
                 end = td[1].next
                 document.getElementById("chromosome").selectedIndex = td[1].chr - 1
                 document.getElementById("start_position").value = start
                 document.getElementById("end_position").value = end
                 create_selection_box(td[1].chr,td[1].prev,td[1].next)
                 $('#ld_form').submit()
              else 
                location.href="/AraQTLld/coregulation/?experiment_name="+trait_data.experiment+"&query="+td[0]
          .on "contextmenu", (event,td) ->
              event.preventDefault()
              el = document.getElementById("collapse-form")
              if ($("#network-modal").is(":visible"))
                start = td[1].prev
                end = td[1].next
                document.getElementById("chromosome").selectedIndex = td[1].chr - 1
                document.getElementById("start_position").value = start
                document.getElementById("end_position").value = end
                create_selection_box(td[1].chr,td[1].prev,td[1].next)
                $('#ld_form').submit()


    traitaxes = svg.append("g").attr("id", "trait_data_axes").attr("class", "trait_data")
    traitaxes.append("text").text(titletxt)
      .attr("x", (left + right) / 2)
      .attr("y", top - pad.top / 2)
      .attr("fill", maincolor).style("font-size", "18px")

    if bLegend
        legendtip = d3.tip()
                 .attr("class", "d3-tip")
                 .offset([-10, 150])
                 .html((EVENT,d) ->  d)

        svg.call(legendtip)

        for trait_data,i  in trait_data_set
          if i < maxLegendLines
            legend.append("line")
              .attr("x1",leftLegend + 10)
              .attr("y1",topLegend + 18*i + 15)
              .attr("x2",leftLegend + 40)
              .attr("y2",topLegend + 18*i + 15)
              .attr("class", "thickline")
              .attr("stroke", lineColors(i))

          linkUrl = ""
          legendtiptext = ""
          label = ""
          if i == maxLegendLines
            label = "..."
          else if multiexperiment
            label = trait_data.experiment
            linkUrl = "/AraQTLld/multiplot/?query="+trait_data.ID+"&experiment_name="+trait_data.experiment
            legendtiptext = "<i>click to only show this profile</i>"
          else if trait_data.ID
            if trait_data.name
              label = trait_data.ID+" ("+trait_data.name+")"
            else 
              label = trait_data.ID
            linkUrl = "/AraQTLld/multiplot/?query="+trait_data.ID+"&experiment_name="+trait_data.experiment
            legendtiptext = "<i>click to only show this profile</i>"
          else if trait_data.name
            label = trait_data.name 

          xlink = traitaxes.append("a").attr("xlink:href", linkUrl).attr("xlink:show","new")
          do(trait_data,i)->
              xlink.append("text")
                .text(label)
                .attr("x", leftLegend + 50)
                .attr("y", topLegend + 18*i + 15)
                .attr("fill", titlecolor)
                .style("text-anchor", "start")
                .on "mouseover", (event) ->
                    svg.selectAll('path').attr("opacity", 0.1)
                    svg.selectAll('path#'+trait_data.ID+'-'+trait_data.experiment).attr("opacity", 1)
                    legendtip.show(event,legendtiptext) if legendtiptext
                .on "mouseout", () ->
                    svg.selectAll('path').attr("opacity", 1)
                    legendtip.hide()

          if i == maxLegendLines
            break


  # black borders
    svg.append("rect")
       .attr("x", left)
       .attr("y", top)
       .attr("height", h)
       .attr("width", w)
       .attr("class", "outerBox")

    svg.append("rect")
       .attr("x", leftLegend)
       .attr("y", topLegend)
       .attr("height", hLegend)
       .attr("width", wLegend)
       .attr("class", "outerBox")

  $("body").css("cursor", "progress");
  promises = []
  for t,trait of traits
     promises.push(d3.json("../traitdata/?experiment_name=" + trait.experiment + "&query=" + trait.trait_id.toUpperCase())) if trait.show

  Promise.allSettled(promises).then((results) =>
    data_sets=[];
    results.forEach((result) =>
        if (result.value)
            data_sets.push(result.value)
        else
            console.log(result.error)
        )
    draw_trait(data_sets)
    )
  $("body").css("cursor", "default");

# load json file and call draw function

draw_qtl_plot = () -> d3.json("../media/species/"+species+".json").then(draw)
