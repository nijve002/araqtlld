/* ------------------------------ */
/* ---------- Div to hide ---- */
$("#container-network").hide();
$("#cy-panel").show();

$('.modal.draggable>.modal-dialog').draggable({
    cursor: 'move',
    handle: '.modal-header',
    bounds: { minX: 0, minY: 0, maxX: 200, maxY: 200 }
});
$('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');

var cy = null;
/* ------------------------------ */
/* ---------- Add first peak to form ---- */

function add_peak(bRemove = true) {
    let id = Math.random().toString(36).substring(7);
    let removeButton = '';
    if (bRemove) {
        removeButton = `<input type="button" class="delete" id="btnRemove_${id}" value="-"></input>`;
    }
    let html = `
    <div id="${id}" class="peak_selection" previous_id="${active_peak_id}">
    <label for="chromosome_${id}">chr</label>
    <select id="chromosome_${id}" class="form-select" name="chromosome_${id}">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select>:
    <input id="start_position_${id}" class="position_input" type="text" name="start_position_${id}" title="eQTL start position, e.g. 2000000">&dash;
    <input id="end_position_${id}" class="position_input" type="text" name="end_position_${id}" title="eQTL end position: , e.g. 4000000">
    ${removeButton}
    </div>
    `
    $('#peak_selections').append(html);
    active_peak_id = id;
}
add_peak(false);

$('#peak_selections').on("click", ".delete", function (event) {
    event.preventDefault();
    let id = $(this).parent('div').attr('id');
    let svg = d3.select("svg");
    svg.select("#selection" + id).remove();
    svg.select("#leftpull" + id).remove();
    svg.select("#rightpull" + id).remove();
    active_peak_id = $(this).parent('div').attr('previous_id');
    $(this).parent('div').remove();
    $('#ld_form').submit();
});

/*----------------------------------------*/
SS = "OFF";
SE = "OFF";
function select_start() {
    SS = "ON";
    //$(".network-modal").modal('hide');
}
function select_end() {
    SE = "ON";
}

/* -------------------------------------- */
/* ---------- Manage form submission ---- */
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const url_chr = urlParams.get('chr');
const url_start = urlParams.get('start');
const url_end = urlParams.get('end');
const url_chr2 = urlParams.get('chr2');
const url_start2 = urlParams.get('start2');
const url_end2 = urlParams.get('end2');
const show_network = urlParams.get('network')
var url_click = false;


window.onload = function waitForPlotarea(){
    if(typeof plotarea !== "undefined") {
        if (url_chr != null && url_start != null && url_end != null){
            if (url_chr2 != null && url_start2 != null && url_end2 != null){
                add_peak();
            }
            url_click = true;
            var peak_ids = document.getElementsByClassName("peak_selection");
            var peak_id = peak_ids[0].id;
            $('input[name=start_position_' + peak_id + ']').val(url_start);
            $('input[name=end_position_' + peak_id + ']').val(url_end);
            $('select[name=chromosome_' + peak_id).val(url_chr);
            create_selection_box(peak_id, url_chr, url_start, url_end);
            
            if (url_chr2 != null && url_start2 != null && url_end2 != null){
                var peak_id2 = peak_ids[1].id;
                $('input[name=start_position_' + peak_id2 + ']').val(url_start2);
                $('input[name=end_position_' + peak_id2 + ']').val(url_end2);
                $('select[name=chromosome_' + peak_id2).val(url_chr2);
                create_selection_box(peak_id2, url_chr2, url_start2, url_end2);
            }
            document.getElementById('btnSubmit').click();
        } else if (show_network != null) {
            $('input[name="trait_checkbox"]').prop("checked", true);
            resize_plot();
        }
    }
    else {
        setTimeout(waitForPlotarea, 250);
    }
}

$('#ld_form').on('submit', function (event) {
    event.preventDefault();
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    if (url_click == false){
        var start_position = $('input[name=start_position_' + active_peak_id + ']').val();
        var end_position = $('input[name=end_position_' + active_peak_id + ']').val();
        var chromosome = $('select[name=chromosome_' + active_peak_id).prop('selectedIndex') + 1;
        create_selection_box(active_peak_id, chromosome, start_position, end_position);
    } else {
        $('#network-modal').show();
        url_click = false;
    }
    var targets =  $('input[name=targets]').val();
    
    var peak_ids = document.getElementsByClassName("peak_selection");
    var peaks = []
    var i;

    for (i = 0; i < peak_ids.length; i++) {
        var peak_id = peak_ids[i].id;
        var start_position = $('input[name=start_position_' + peak_id + ']').val();
        var end_position = $('input[name=end_position_' + peak_id + ']').val();
        var chromosome = $('select[name=chromosome_' + peak_id).prop('selectedIndex') + 1;
        peaks[i] = {};
        peaks[i]["chromosome"] = parseInt(chromosome);
        peaks[i]["start_position"] = parseInt(start_position);
        peaks[i]["end_position"] = parseInt(end_position);
    }

    var formData = {
        experiment_name: experiment_name,
        targets: targets,
        qtls: peaks
    };

    $.ajax({
        headers: { 'X-CSRFToken': csrftoken },
        method: 'POST',
        url: '/AraQTL/arald/get_interactions/',
        dataType: 'json',
        data: JSON.stringify(formData),
        beforeSend: function () {
            // Show message
            $('#text_message').html("Query running and network loading...");
            $('#text_message').show();
        },
        success: function (data) {
            // this gets called when server returns an OK response
            //$("#sheet").hide();
            $('#text_message').hide();
            $("#container-network").show();
            loadNetwork(data);
        },
        complete: function (data) {
            // Hide image container
            $('#text_message').hide();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $('#text_message').html("An error occured");
            $('#text_message').show();
        }
    });

});

$('#btnAdd').on('click', function (event) {
    add_peak();
});

$('#container_network').on('mousedown', function (event) {
    event.stopPropagation();
});

$('#toggle-cypanel').on('click', function (event) {
    if ($('#cy-panel').is(":visible")) {
        $('#cy-panel').hide();
        $('#toggle-cypanel').html("&lt;&lt;");
    } else {
        $('#cy-panel').show();
        $('#toggle-cypanel').html("&gt;&gt;");
    }
});

$('#close_cypanel').on('click', function (event) {
    $('#toggle_network').html("Show interaction network");
    var svg = d3.select("svg");
    svg.select("#selection").remove();
    svg.select("#leftpull").remove();
    svg.select("#rightpull").remove();
    $('#network-modal').hide();
    active_peak_id = 0;
});

var cy_panel_top = 0;
var cy_panel_left = 0;
var cy_panel_height = 200;
var cy_panel_width = 200;

$('#maximize_cypanel').on('click', function (event) {
    cy_panel_top = $('#network-modal').offset().top;
    cy_panel_left = $('#network-modal').offset().left;
    cy_panel_height = $('#network-modal').height();
    cy_panel_width = $('#network-modal').width();
    var top = $(window).scrollTop();
    var left = $(window).scrollLeft();
    var height = $(window).height();
    var width = $(window).width();
    $('#network-modal').css({ 'top': top, 'left': left });
    $('#network-modal').height(height);
    $('#network-modal').width(width);
});

$('#minimize_cypanel').on('click', function (event) {
    $('#network-modal').css({ 'top': cy_panel_top, 'left': cy_panel_left });
    $('#network-modal').height(cy_panel_height);
    $('#network-modal').width(cy_panel_width);
    $('#zoom').click();
});


/* ---------- loadNetwork ---------------------- */
/* Initialize a cytoscape network from inputData */
/* --------------------------------------------- */
function loadNetwork(inputData) {
    var eles = inputData['results_sparql'].elements;
    var cis_lod = inputData['cis_lod'];
    var locs = inputData['locs'];
    var eqtl_genes = inputData['total_candidate']
    document.getElementById("eqtl_genes").innerHTML  = eqtl_genes;

    var loc_colors = ["#4285F4",  "#FBBC05", "#34A853", "#EA4335"]; //google colors
    var styles = [];
    for (const [loc_index, loc] of locs.entries()) {
        styles.push(
            {
                selector: `node[loc = '${loc}']`,
                style: {
                    "background-color": loc_colors[loc_index],
                }
            }
        );
    }

    if (cy != null) {
        cy.destroy();
    }

    cy = cytoscape({
        container: document.getElementById('cy'), // container to render in
        wheelSensitivity: 0.7,
        style: [ // the stylesheet for the graph
            {
                selector: 'node',
                style: {
                    "content": "data(label)",
                    "font-size": "12px",
                    "text-valign": "center",
                    "text-halign": "center",
                    "background-color": "#555",
                    "text-outline-color": "#555",
                    "text-outline-width": "2px",
                    "color": "#fff",
                    //"opacity": "0.75",
                    "overlay-padding": "6px",
                    "z-index": "1"
                }
            },
            {
                selector: "node[role='candidate']",
                style: {
                    "width": 'mapData(degree, 1, 6, 30, 60)',
                    "height": 'mapData(degree, 1, 6, 30, 60)',
                    "font-size": 'mapData(degree, 1, 6, 12, 18)',
                    "text-outline-width": 'mapData(degree, 1, 6, 2, 3)',
                }
            },
            {
                selector: "node[role='candidate'][degree=0]",
                style:{ 'opacity': '0.1', }
            },
            {
                selector: "node[?tf]",
                style: { "shape": "triangle" }
            },
            {
                selector: 'edge',
                style: {
                    //'width': 'data(width)',
                    "curve-style": "bezier",
                    "haystack-radius": "0.5",
                    //"opacity": "0.4",
                    "line-color": "#bbb",
                    "width": "0.3",
                    "overlay-padding": "3px"
                }
            },
            {
                selector: 'node.highlight',
                style: {
                    'border-color': '#000000',
                    'border-width': '2px'
                }
            },
            {
                selector: 'node.semitransp',
                style:{ 'opacity': '0.1' }
            },
            {
                selector: 'edge.highlight',
                style: { 'mid-target-arrow-color': '#000000' }
            },
            {
                selector: 'edge.semitransp',
                style:{ 'opacity': '0.1' }
            }
        ].concat(styles)
    });

    cy.add(eles);

    cy.nodes().forEach(function(node) {
        if (node.data("id") in cis_lod) {
            node.data("cis_lod", cis_lod[node.data("id")]);
        } else if (node.data('role') == 'candidate'){
            node.data("cis_lod",0);
        }
    });

    var databases = {};
    cy.edges().forEach(function (ele) {
        let database = ele.data('database');
        if (database in databases) {
            var max_score = Math.max(databases[database], ele.data('score'))
            databases[database] = max_score;
        } else {
            databases[database] = ele.data('score');
        };
     });


    function updateLayout() {
        if ($('#cy_layout').val() == 'bipartite-eQTG-Finder') {
            var nodes = cy.nodes().sort(function (a, b) { return b.data("gene_prob") - a.data("gene_prob") });
        } else if ($('#cy_layout').val() == 'bipartite-cis-eqtl') {
            var nodes = cy.nodes().sort(function (a, b) { return b.data("cis_lod") - a.data("cis_lod")  });
        } else {
            var nodes = cy.nodes().sort(function (a, b) { return b.degree() - a.degree() });
        }
        //let nodes = cy.nodes().sort(function (a, b) { return b.degree() - a.degree() });
        //let nodes = cy.nodes().sort(function (a, b) { return b.data("gene_prob") - a.data("gene_prob") });
        //let nodes = cy.nodes().sort(function (a, b) { return b.neighbourhood('node').length - a.neighbourhood('node').length });
        let left_row = 0;
        let right_row = 0;

        nodes.forEach(function(node) {
            //let col = (node.classes().includes("node-target")) ? 0 : 1;
            let col = Object.values(locs).findIndex(x => x == node.data("loc"));
            let row = (col == 1) ? left_row++ : right_row++;
            node.data('row', row);
            node.data('col', col);
            node.data('degree',node.degree());
        });

        let layouts = {};
        layouts['cola'] = {
            name: 'cola',
            animate: false,
            nodeSpacing: 5,
            edgeLengthVal: 45,
            randomize: false
        };
        layouts['cise'] = {
            name: 'cise',
            animate: false,
            refresh: 100,
            maxRatioOfNodesInsideCircle: 0,
            idealInterClusterEdgeLengthCoefficient: 1.4,
            nodeSeparation: 5,
            allowNodesInsideCircle: false,
            nodeRepulsion: 0,
            clusters: function( node ) {
                return Object.values(locs).findIndex(x => x == node.data("loc"));
            }
        };
        layouts['bipartite-degree'] = {
            name: 'grid',
            cols: 2,
            position: function (node) {
                col = node.data('col');
                row = node.data('row');
                return { row, col };
            },
        };

        layouts['bipartite-eQTG-Finder'] = {
            name: 'grid',
            cols: 2,
            position: function (node) {
                col = node.data('col');
                row = node.data('row');
                return { row, col };
            },
        };

        layouts['bipartite-cis-eqtl'] = {
            name: 'grid',
            cols: 2,
            position: function (node) {
                col = node.data('col');
                row = node.data('row');
                return { row, col };
            },
        };

        if ($('#cy_layout').val() in layouts) {
            let layout_params = layouts[$('#cy_layout').val()];
            let layout = cy.elements().layout(layout_params);
            layout.run();
        }
        // Count candidates
        var candidates = cy.nodes().filter("node[role='candidate'][degree>0]").size()
        document.getElementById("candidates").innerHTML = candidates;
    }


    updateLayout();

    /*--------------------------*/
    // create tooltip for each edge and node
    cy.ready(function() {
        cy.edges().forEach(function(ele) {
            makePopper(ele);
        });
        cy.nodes().forEach(function(ele) {
            makePopper(ele);
        });
    });
    cy.edges().unbind('mouseover');
    cy.edges().bind('mouseover', (event) => event.target.tippy.show());
    cy.edges().unbind('mouseout');
    cy.edges().bind('mouseout', (event) => event.target.tippy.hide());

    cy.nodes().unbind('mouseover');
    cy.nodes().bind('mouseover', (event) => event.target.tippy.show());
    cy.nodes().unbind('mouseout');
    cy.nodes().bind('mouseout', (event) => event.target.tippy.hide());

    var selected_node = null;
    cy.on('mousedown', 'node', function(event){
        cy.elements().removeClass('semitransp');
        cy.elements().removeClass('highlight');
        var sel = event.target;
        if (sel == selected_node){
            selected_node = null;
            return;
        }
        selected_node = sel;
        cy.elements().difference(sel.outgoers()
        .union(sel.incomers()))
        .not(sel)
        .addClass('semitransp');
        sel.addClass('highlight').outgoers()
        .union(sel.incomers())
        .addClass('highlight');
    });
    //try to open an url
    cy.on('dblclick', 'node', function(){
        try { // your browser may block popups
          window.open( this.data('TAIR_link') );
        } catch(e){ // fall back on url change
          window.location.href = this.data('TAIR_link');
        }
      });


    /*-------------------------*/
    // create table of results
    var table_content = "";
    $("#result_table_network").empty(); //reset table
    table_content += '<table id="network_table">';
    table_content += '<thead><tr><th>Candidate</th><th>Label</th><th>Target</th><th>Score</th><th>Evidence</th><th>Sources</th></tr></thead>';
    table_content += '<tbody>';
    // iterate over edges to fill in the table
    cy.edges().forEach(function (ele) {
        table_content += '<tr><td>' + ele.source().id() + '</td>';
        table_content += '<td>' + ele.source().data('label') + '</td>';
        table_content += '<td>' + ele.target().id() + '</td>';
        table_content += '<td>' + ele.data('score') + '</td>';
        table_content += '<td>' + ele.data('interaction_evidence') + '</td>';
        table_content += '<td>' + ele.data('database') + '</td>';
        table_content += '</tr>';
    });
    table_content += '</tbody></table>';
    $("#result_table_network").append(table_content);
    $('#network_table').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        paging: false,
        dom: 'Bfrtip',
        autowidth: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ],
    });
    $('#zoom').on('click', function (event) { cy.reset() });
    /* ------------------------ */


    var node_filters = {};
    var edge_filters = {};
    var complete_network = cy.elements();
    function filter_network() {
        complete_network.restore();

        for (const [key,filter] of Object.entries(edge_filters)) {
            cy.remove(cy.filter(filter));
        }

        for (const [key,filter] of Object.entries(node_filters)) {
            cy.remove(cy.filter(filter).connectedEdges());
        }
        updateLayout();
        for (const [database, max_score] of Object.entries(databases)) {
            let database_nospace = database.replace(/ /g,"_");
            var total_edges = cy.edges().filter('edge[database="'+database+'"]').size();
            $("#edge_count_"+database_nospace).val(total_edges);
        }
    }


    /* ------------------------ */
    // Checkbox database

    database_list = {
        'STRING': '#85e62c',
        'AraNet': '#1796ae',
        'AGRIS': '#6789a6',
        'PlantRegMap': '#a254ee',
        'PlantCistromeDB': '#a020f0',
        'PlantPan': '#838bf4',
        'AraCyc': '#7451f9',
        'KEGG': '#daf257',
        'Gene Ontology': '#ebff0a'
    }

    database_desc = {
        'STRING': 'Protein-protein interaction from STRING (string-db.org)',
        'AraNet': 'Protein-protein interaction from AraNet (inetbio.org/aranet)',
        'AGRIS': 'Transcription factor - target interaction from AGRIS (agris-knowledgebase.org)',
        'PlantRegMap': 'Transcription factor - target interaction from PlantRegMap (plantregmap.gao-lab.org)',
        'PlantCistromeDB': 'Transcription factor - target interaction from Plant Cistrome Database (neomorph.salk.edu/dap_web/pages/index.php)',
        'PlantPan': 'Transcription factor - target inference using MAST (https://meme-suite.org/meme/doc/mast.html) and binding site from PlantPAN 3.0 (plantpan.itps.ncku.edu.tw)',
        'AraCyc': 'Similarity in metabolic pathway annotation using data from AraCyc (plantcyc.org/typeofpublication/aracyc)',
        'KEGG': 'Similarity in metabolic pathway annotation using data from KEGG (www.genome.jp/kegg)',
        'Gene Ontology': 'Similarity in biological process annotation using data from Gene Ontology (geneontology.org)'
    }

    let safe_row = $('#layout_row').index();
    $("#cypanel_table").find("tr:lt("+safe_row+")").remove();

    // add LOD score slider
    let max_lod = cy.nodes().max(function(node) {
        return Math.abs(node.data('cis_lod'));
    });
    
    new_row  = ' <tr class="cypanel_row"><td class="cypanel_cell" colspan = 2>\
        <b><i>cis</i>-eQTL</b><br>\
        Lod score cutoff: <output id="lod_score_cutoff" style="display: inline; font-size: 10px;">0</output>\
        <input id="slider-lod-cutoff" type="range" min="0" max="'+Math.floor(max_lod.value)+'" step="1" value="0" class="slider" \
        oninput="lod_score_cutoff.value = this.value"></td></tr>';

    $("#cypanel_table tr:first").before(new_row);

    $('#slider-lod-cutoff').on('change', function (event) {
        var value = (this.value-this.min)/(this.max-this.min)*100
        let cutoff = this.value
        this.style.background = 'linear-gradient(to right, #DEE2E6  0%, #DEE2E6  ' + value + '%, #4285F4  ' + value + '%, #4285F4  100%)'
        node_filters["lod-slider"] = function(element, i) {
            return element.isNode() && Math.abs(element.data('cis_lod')) < cutoff;
        }
        //filters["lod-slider"] =  cy.elements("node[role='candidate'][cis_lod < " + this.value + "]").connectedEdges()
        lod_score_cutoff.value=this.value;
        filter_network();
    });

    // add eqtg-finder score slider
    let max_prob = cy.nodes().max(function(node) {
        return Math.abs(node.data('gene_prob'));
    });
    
    new_row  = ' <tr class="cypanel_row"><td class="cypanel_cell" colspan = 2>\
        <b>eQTG-Finder score</b><br>\
        eQTG-Finder score cutoff: <output id="eqtg_score_cutoff" style="display: inline; font-size: 10px;">0.007</output>\
        <input id="slider-eqtg-cutoff" type="range" min="0" max="'+(max_prob.value-0.0001)+'" step="0.0001" value="0.007" class="slider" \
        oninput="eqtg_score_cutoff.value = this.value"></td></tr>';

    $("#cypanel_table tr:first").before(new_row);

    $('#slider-eqtg-cutoff').on('change', function (event) {
        let value = (this.value-this.min)/(this.max-this.min)*100
        let cutoff = this.value
        this.style.background = 'linear-gradient(to right, #DEE2E6  0%, #DEE2E6  ' + value + '%, #4285F4  ' + value + '%, #4285F4  100%)'
        node_filters["eqtg-slider"] = function(element, i) {
            return element.isNode() && element.data('role') == 'candidate' && element.data('gene_prob') < cutoff;
        }
        eqtg_score_cutoff.value=this.value;
        filter_network();
    });    

    // add all databases for which there are edges
    for (const [database, max_score] of Object.entries(databases)) {
        let database_nospace = database.replace(/ /g,"_");
        var total_edges = cy.edges().filter('edge[database="'+database+'"]').size();

        new_row = '<tr class="cypanel_row">\
        <td class="cypanel_cell" colspan="2">';
        /* <span data-tooltip="'+database_desc[database]+'">?</span>\ */
        new_row += '<input class="form-check-input" type="checkbox" id="check-'+database_nospace+'" value="'+database+'" checked>\
            <label "class="form-check-label" for="check-'+database_nospace+'" >'+database+'</label>\
            <span data-tooltip="'+database_desc[database]+'">?</span>\
            <span style="border: 2px solid black;background: '+database_list[database]+'">&nbsp;&nbsp;&nbsp;&nbsp;</span>\
            edges: <output id="edge_count_'+database_nospace+'" style="display: inline; font-size: 10px;">'+ total_edges +'</output>';

        if (database=='PlantPan') {
            let step = 0.001;
            new_row += '<div id="collapse'+database_nospace+'">\
                Threshold: <output id="score'+database_nospace+'" style="display: inline; font-size: 10px;">1</output>\
                <input id="slider-score-'+database_nospace+'" type="range" min="0" max="1" step="0.01"  value="1" class="slider" list="tickmarks">\
            </div>';
        } else if (max_score > 0) {
            let step = (max_score > 1) ? 1 : 0.1;
            new_row += '<div id="collapse'+database_nospace+'">\
                Threshold: <output id="score'+database_nospace+'" style="display: inline; font-size: 10px;">0</output>\
                <input id="slider-score-'+database_nospace+'" type="range" min="0" max="'+(Math.ceil(max_score) + 1)+'" step="'+ step + '" \
                value="0" class="slider" list="tickmarks">\
            </div>';
        }

        new_row += '</td></tr>';

        $("#cypanel_table tr:first").before(new_row);

        $('#check-'+database_nospace).on('change', function (event) {
            if ($(this).is(':checked')) {
                delete edge_filters["check-"+database_nospace];
            } else {
                // filtering edges
                edge_filters["check-"+database_nospace]  = '[database="' + this.value + '"]';
            }
            filter_network();
        });
    
        if (database == 'PlantPan') {
            $('#slider-score-'+ database_nospace).on('change', function (event) {
                var value = (this.value-this.min)/(this.max-this.min)*100
                this.style.background = 'linear-gradient(to right, #4285F4  0%, #4285F4  ' + value + '%, #DEE2E6  ' + value + '%, #DEE2E6  100%)'
                edge_filters[database_nospace+"-slider"] = 'edge[database="' + database +'"][score > ' + this.value + ']';
                $("#score"+database_nospace).val(this.value);
                filter_network();
            });
        } else if (max_score > 0) {
            $('#slider-score-'+ database_nospace).on('change', function (event) {
                var value = (this.value-this.min)/(this.max-this.min)*100
                this.style.background = 'linear-gradient(to right, #DEE2E6  0%, #DEE2E6  ' + value + '%, #4285F4  ' + value + '%, #4285F4  100%)'
                edge_filters[database_nospace+"-slider"] = 'edge[database="' + database +'"][score < ' + this.value + ']';
                $("#score"+database_nospace).val(this.value);
                filter_network();
            });
        };
    };

    // Count candidates
    var candidates = cy.nodes().filter("node[role='candidate'][degree>0]").size()
    document.getElementById("candidates").innerHTML = candidates;

    /*--------------------------*/
    $('#cy_layout').on('change', function (e) {
        updateLayout();
    });

    //console.log(GO_list);
    cy.style().selector('edge').style({ 'line-color': '#bbb', "width": "1" }); // reset to default color
    for (key in database_list) {
        if (!(key in databases)) {
            continue;
        }
        cy.style()
            .selector('edge[database="' + key + '"]').style({
                'line-color': database_list[key],
            })
    }
    cy.style().update();
}

/* ---------------------------------------------- */
/* ---------- Retrieve unique value of array ---- */
const unique = (value, index, self) => {
    return self.indexOf(value) === index
}

function download(content, mimeType, filename) {
    var a = document.createElement('a')
    var blob = new Blob([content], { type: mimeType })
    var url = URL.createObjectURL(blob)
    a.setAttribute('href', url)
    a.setAttribute('download', filename)
    a.click()
}

function makePopper(ele) {
    let ref = ele.popperRef(); // used only for positioning
    if (ele.isEdge()) {
      ele.tippy = tippy(ref, { // tippy options:
          content: () => {
              let content = document.createElement('div');
              //content.style.zIndex = "90";
              let popper_text = ele.data("interaction_evidence");
              popper_text += " (" + ele.data("database")  + ")<br>"
              var score = ele.data("score").toFixed(2);
              if (score >= 0) {
                  popper_text += " score: " + ele.data("score").toFixed(2);
              }
              content.innerHTML = popper_text;
              return content;
          },
          trigger: 'manual' // probably want manual mode
      });
  } else if (ele.isNode()) {
      ele.tippy = tippy(ref, { // tippy options:
          content: () => {
              let content = document.createElement('div');
              let popper_text = "";
              if (ele.data("label") == ele.data("id")) {
                  popper_text = ele.data("label");
              } else {
                  popper_text = ele.data("label") + " (" + ele.data("id") + ")";
              }
              popper_text += "<br>" + ele.data("role");
              popper_text += "<br>" + ele.data("TAIR_link");
              if (ele.data("cis_lod")) {
                popper_text += "<br>LOD score: " + ele.data("cis_lod").toFixed(2);
              }
              popper_text += "<br>eQTG-Finder score: " + ele.data("gene_prob");
              popper_text += "<br>Degree: " + ele.degree();
              content.innerHTML = popper_text;
              return content;
          },
          trigger: 'manual' // probably want manual mode
      });
  }
}

function create_selection_box(id, chromosome, start, end) {
    var svg = d3.select("svg");
    svg.select("#selection_" + id).remove();
    svg.select("#leftpull_" + id).remove();
    svg.select("#rightpull_" + id).remove();
    var top = d3.select("#plotarea").attr("y");
    var height = d3.select("#plotarea").attr("height");
    var halve_height = 1 * top + height / 2;
    var left = chrXScale[chromosome](start);
    var width = chrXScale[chromosome](end) - left;
    var pull_r = 5
    svg.append("rect").attr("id", "selection_" + id)
        .attr("x", chrXScale[chromosome](start))
        .attr("y", top).attr("height", height)
        .attr("width", width).attr("stroke", "none")
        .attr("fill", "#8C4374").attr("opacity", 0.5)
        .attr("chromosome", chromosome);
    svg.append("circle").attr("id", "leftpull_" + id)
        .attr("cx", chrXScale[chromosome](start) - pull_r)
        .attr("cy", halve_height).attr("r", pull_r)
        .attr("fill", "#8C4374").attr("fill-opacity", "0.1")
        .attr("stroke", "#8C4374").attr("stroke-opacity", "0.6")
        .on("mouseenter mouseleave", function (EVENT) {
            var el = d3.select(this), isEntering = EVENT.type === "mouseenter";
            el.attr(
                "fill-opacity", isEntering ? "0.5" : "0.1"
            );
        });
    svg.append("circle").attr("id", "rightpull_" + id)
        .attr("cx", chrXScale[chromosome](end) + pull_r)
        .attr("cy", halve_height).attr("r", pull_r)
        .attr("fill", "#8C4374").attr("fill-opacity", "0.1")
        .attr("stroke", "#8C4374").attr("stroke-opacity", "0.6")
        .on("mouseenter mouseleave", function (EVENT) {
            var el = d3.select(this), isEntering = EVENT.type === "mouseenter";
            el.attr(
                "fill-opacity", isEntering ? "0.5" : "0.1"
            );
        });
    var dragHandlerLeft = d3.drag()
        .on("drag", function (event) {
            var selection = svg.select("#selection_" + id);
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var newX = chrXScale[chrom](chrXScale[chrom].invert(event.x));
            var newWidth = 1.0 * curWidth + 1.0 * curX - newX;
            console.log(curWidth, curX, event.x, newWidth);
            if (newWidth <= 0) {
                newWidth = curWidth;
                newX = curX;
            }
            selection.attr("x", newX).attr("width", newWidth);
            svg.select("#leftpull_" + id).attr("cx", newX - pull_r);
        })
        .on("end", function (event) {
            active_peak_id = id;
            var selection = svg.select("#selection_" + id);
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var start = Math.round(chrXScale[chrom].invert(curX));
            var end = Math.round(chrXScale[chrom].invert(1.0 * curX + 1.0 * curWidth));
            document.getElementById("start_position_" + id).value = start;
            document.getElementById("end_position_" + id).value = end;
            $('#ld_form').submit();
        });
    dragHandlerLeft(svg.select("#leftpull_" + id));
    var dragHandlerRight = d3.drag()
        .on("drag", function (event) {
            var selection = svg.select("#selection_" + id);
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            if (curX >= event.x) {
                return;
            }
            var newX = chrXScale[chrom](chrXScale[chrom].invert(event.x));
            var newWidth = newX - 1.0 * curX;
            selection.attr("width", newWidth);
            svg.select("#rightpull_" + id).attr("cx", newX + pull_r);
        })
        .on("end", function (event) {
            active_peak_id = id;
            var selection = svg.select("#selection_" + id);
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var start = Math.round(chrXScale[chrom].invert(curX));
            var end = Math.round(chrXScale[chrom].invert(1.0 * curX + 1.0 * curWidth));
            document.getElementById("start_position_" + id).value = start;
            document.getElementById("end_position_" + id).value = end;
            $('#ld_form').submit();
        });
    dragHandlerRight(svg.select("#rightpull_" + id));
    $('#text_message').hide();

}

