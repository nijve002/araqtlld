/* ------------------------------ */
/* ---------- Div to hide ---- */
$("#container-network").hide();
$("#cy-panel").hide();

$('.modal.draggable>.modal-dialog').draggable({
    cursor: 'move',
    handle: '.modal-header'
});
$('.modal.draggable>.modal-dialog>.modal-content>.modal-header').css('cursor', 'move');

/*----------------------------------------*/
SS = "OFF";
SE = "OFF";
function select_start(){
    SS = "ON";
    //$(".network-modal").modal('hide');
}
function select_end(){
    SE = "ON";
}
/* -------------------------------------- */
/* ---------- Manage form submission ---- */
$('#ld_form').on('submit', function(event) {
    event.preventDefault();
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    var start_position = $('input[name=start_position]').val();
    var end_position = $('input[name=end_position]').val();
    var chromosome = $('select[name=chromosome').prop('selectedIndex') + 1;
    create_selection_box(chromosome,start_position,end_position);
    var formData = {
	    'chromosome'         : $('select[name=chromosome').val(),
        'start_position'     : $('input[name=start_position]').val(),
	    'end_position'       : $('input[name=end_position]').val(),
        'targets'            : $('input[name=targets]').val(),
	};
	$.ajax({
	headers: { "X-CSRFToken": csrftoken },
        method: 'POST',
        url: '/AraQTLld/arald/get_interaction_from_multiple_targets/',
        data: { formData },
            beforeSend: function(){
		// Show message
                $('#text_message').html("Query running and network loading...");
                $('#text_message').show();
	    },
            success: function (data) {
            	// this gets called when server returns an OK response
	    	//$("#sheet").hide();
                $('#text_message').hide();
                $("#container-network").show();
            	loadNetwork(data);
            },
	    complete:function(data){
		// Hide image container
		$('#text_message').hide();
	    },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            $('#text_message').html("An error occured");
            $('#text_message').show();
        }
    });
});

$('#container_network').on('mousedown', function(event) {
    event.stopPropagation();
});

$('#toggle-cypanel').on('click', function(event) {
    if ($('#cy-panel').is(":visible")) {
        $('#cy-panel').hide();
        $('#toggle-cypanel').html("&lt;&lt;");
    } else {
        $('#cy-panel').show();
        $('#toggle-cypanel').html("&gt;&gt;");
    }
});

$('#close_cypanel').on('click', function(event) {
    $('#toggle_network').html("Show interaction network");
    var svg = d3.select("svg");
    svg.select("#selection").remove();
    svg.select("#leftpull").remove();
    svg.select("#rightpull").remove();
    $('#network-modal').hide();
    $("#result_table_network").hide();
});

var cy_panel_top = 0;
var cy_panel_left = 0;
var cy_panel_height = 200;
var cy_panel_width = 300;

$('#maximize_cypanel').on('click', function(event) {
    cy_panel_top = $('#network-modal').offset().top;
    cy_panel_left = $('#network-modal').offset().left;
    cy_panel_height = $('#network-modal').height();
    cy_panel_width = $('#network-modal').width();
    var top = $(window).scrollTop();
    var left = $(window).scrollLeft();
    var height = $(window).height();
    var width = $(window).width();
    $('#network-modal').css({'top': top, 'left' : left});
    $('#network-modal').height(height);
    $('#network-modal').width(width);
});
http://thornton.bioinformatics.nl:8086/ (only accessible from the university network)
$('#minimize_cypanel').on('click', function(event) {
    $('#network-modal').css({'top': cy_panel_top, 'left' : cy_panel_left});
    $('#network-modal').height(cy_panel_height);
    $('#network-modal').width(cy_panel_width);
    $('#zoom').click();
});

/* ---------- loadNetwork ---------------------- */
/* Initialize a cytoscape network from inputData */
/* --------------------------------------------- */
function loadNetwork(inputData) {
    var eles = inputData['results_sparql'].elements;
	var cy = cytoscape({
		container: document.getElementById('cy'), // container to render in
                wheelSensitivity: 0.7,
		style: [ // the stylesheet for the graph
		   {
                    selector: 'node',
                    style: {
                    //"width": "mapData([score], 0, 0.005, 20, 60)",
                    //"height": "mapData([score], 0, 0.005, 20, 60)",
                    "content": "data(label)",
                    "font-size": "12px",
                    "text-valign": "center",
                    "text-halign": "center",
                    "background-color": "red",
                    "text-outline-color": "#555",
                    "text-outline-width": "2px",
                    "color": "#fff",
                    "opacity": "0.7",
                    "overlay-padding": "6px",
                    "z-index": "1"
			    }
			},
            {
                selector: "node[role = 'candidate']",
                style: {
                    "background-color": "#de425b",
                    "width": "mapData([[degree]], 1, 5, 10, 100)",
                    "height": "mapData([degree], 1, 5, 10, 100)",
                    "content": "data(label)",
                    "font-size": "12px",
                    "text-valign": "center",
                    "text-halign": "center",
                    "background-color": "red",
                    "text-outline-color": "#555",
                    "text-outline-width": "2px",
                    "color": "#fff",
                    "overlay-padding": "6px",
                    "z-index": "10"
              }},
           {
                selector: "node[role = 'target']",
                style: {
                    "background-color": "#3f7a38", //"#9fba90",
                    "content": "data(label)",
                    "width": "30",
                    "height": "30"
              }},


		{
		  selector: 'edge',
		  style: {
			'width': 'data(width)',
                        "curve-style": "bezier",
                        "haystack-radius": "0.5",
                        "opacity": "0.4",
                        "line-color": "#bbb",
                        "width": "2",
                        "overlay-padding": "3px"
		  }
	     }]
	});
        layout_params = {
            name: 'grid',
            cols: 2,
            rows: row_count,
            position: function( node ) {
                col = node.data('col');
                row = node.data('row');
                return { row, col };
            },
        }
    cy.add(eles);
	networkInput = inputData['results_sparql'];
        console.log(networkInput);
	// if edge does not exist, add to network
	// else add edge's values
        var  shared_BP_GO_term_list = [];
        var interaction_evidence_list = [];
        // for(let i=0; i<networkInput.length; i++){
        //     let gene_id1 = networkInput[i]["gene_id1"]
        //     let gene_id2 = networkInput[i]["gene_id2"]
        //     let node1 = cy.$id(gene_id1); // get node from id
        //     if (node1.empty()) {
        //         cy.add({
        //             nodes :[
        //             {
        //                 data: {
        //                    id: networkInput[i]["gene_id1"],
        //                    label: networkInput[i]["gene_label1"],
        //                    role: networkInput[i]["role1"],
        //                 }
        //             }]
        //         });
        //     }

        //     let node2 = cy.$id(gene_id2); // get node from id
        //     if (node2.empty()) {
        //         cy.add({
        //             nodes :[
        //             {
        //                 data: {
        //                    id: networkInput[i]["gene_id2"],
        //                    label: networkInput[i]["gene_label2"],
        //                    role: networkInput[i]["role2"],
        //                 }
        //             }]
        //         });
        //     }
        //     // generate random id for edge
        //     let id = Math.random().toString(36).substring(7);
        //     let score;
        //     // check if score exists in input
        //     if ("score" in networkInput[i]){ score = networkInput[i]["score"]}else{ score = ""; }
        //     interaction_evidence = networkInput[i]["interaction_evidence"];
        //     // create list of unique interaction evidences
        //     if(interaction_evidence_list.includes(interaction_evidence)){}else{interaction_evidence_list.push(interaction_evidence);}
        //     if ("go_id1" in networkInput[i]){ shared_BP_GO_term = networkInput[i]["go_id1"]["value"] }else{ shared_BP_GO_term = ""};
        //     if(shared_BP_GO_term_list.includes(shared_BP_GO_term)){}else{shared_BP_GO_term_list.push(shared_BP_GO_term);}
        //     if ("go_id2" in networkInput[i]){ shared_BP_GO_term = networkInput[i]["go_id2"]["value"] }else{ shared_BP_GO_term = ""};
        //     // create list of unique gene ontology terms
        //     if(shared_BP_GO_term_list.includes(shared_BP_GO_term)){}else{shared_BP_GO_term_list.push(shared_BP_GO_term);}
        //     // add updated edge's values
        //     cy.add({
        //         edges: [{
        //             // Directed edge
        //             data: {
        //                     id: id,
        //                     source: networkInput[i]["gene_id1"],
        //                     target: networkInput[i]["gene_id2"],
        //                     interaction_evidence: networkInput[i]["interaction_evidence"],
        //                     shared_BP_GO_term: networkInput[i]["go_id1"]["value"] + "/" + networkInput[i]["go_id2"]["value"],
        //                     score: score,
        //                     database: networkInput[i]["database"]
        //             }
        //         }]
        //     });
        // }
        
        //let nodes = cy.nodes().sort(function(a,b){return b.degree( )- a.degree()});

        //var pr = cy.elements().pageRank();
        //let nodes = cy.nodes().sort(function(a,b){return pr.rank(b) - pr.rank(a)});

        let nodes = cy.nodes().sort(function(a,b){return b.outgoers('node').length - a.outgoers('node').length});
        let left_row = 0;
        let right_row = 0;
        for( let i = 0; i < nodes.length; i++ ){
            let node = nodes[ i ];
            let col = (node.data('role') == 'target') ? 0 : 1;
            let row = (col == 1) ? left_row++ : right_row++;
            node.data('row',row);
            node.data('col',col);
        }
        var row_count = Math.max(left_row,right_row);
	var layout = cy.elements().layout(layout_params);
	layout.run();
        /*--------------------------*/
        // create tooltip for each edge
        cy.ready(function() {
            cy.edges().forEach(function(ele) {
                makePopper(ele);
            });
            cy.nodes().forEach(function(ele) {
                makePopper(ele);
            });
        });
        cy.edges().unbind('mouseover');
        cy.edges().bind('mouseover', (event) => event.target.tippy.show());
        cy.edges().unbind('mouseout');
        cy.edges().bind('mouseout', (event) => event.target.tippy.hide());

        cy.nodes().unbind('mouseover');
        cy.nodes().bind('mouseover', (event) => event.target.tippy.show());
        cy.nodes().unbind('mouseout');
        cy.nodes().bind('mouseout', (event) => event.target.tippy.hide());

        /*-------------------------*/
        // create table of results
        var table_content = "";
        $("#result_table_network").empty(); //reset table
        table_content += '<table>';
        table_content += '<thead><tr><th>Candidate</th><th>Label</th><th>Target</th><th>Label</th><th>Score</th><th>Evidence</th><th>Sources</th></tr></thead>';
        table_content += '<tbody>';
        // iterate over edges to fill in the table
        cy.edges().forEach(function( ele ){
            table_content += '<tr><td>' + ele.source().id() + '</td>'; //candidate
            table_content += '<td>' + ele.source().data('label') + '</td>'; // candidate label
            table_content += '<td>' + ele.target().id() + '</td>'; // target
            table_content += '<td>' + ele.target().data('label') + '</td>'; // target label
            table_content += '<td>' + ele.data('score') + '</td>'; //score
            table_content += '<td>' + ele.data('interaction_evidence') + '</td>'; //evidence
            table_content += '<td>' + ele.data('database') + '</td>'; //database
            table_content += '</tr>';
        });
        table_content += '</tbody></table>';
        $("#result_table_network").append(table_content);
        $('#zoom').on('click', function(event) { cy.reset() });
        /* ------------------------ */
        // Create score slider
        var filteredEdgesSg =  [];
        $('#slider-score-string').on('change', function(event) {
            // putting back the previously removed edges
            cy.add(filteredEdgesSg);
            // filtering edges
            filteredEdgesSg = cy.filter('edge[database="STRING"]');
            filteredEdgesSg = filteredEdgesSg.filter('edge[score> ' + this.value + ']');
            // Removing filteredEdges from graph
            cy.remove(filteredEdgesSg);
            $('#cy_layout').trigger("change");
        });
        var filteredEdgesGO =  [];
        $('#slider-score-go').on('change', function(event) {
            // putting back the previously removed edges
            cy.add(filteredEdgesGO);
            // filtering edges
            filteredEdgesGO = cy.filter('edge[database="Gene Ontology"]')
            filteredEdgesGO = filteredEdgesGO.filter('edge[score> ' + this.value + ']');
            // Removing filteredEdges from graph
            cy.remove(filteredEdgesGO);
            $('#cy_layout').trigger("change");
        });

        /* ------------------------ */
        // Checkbox database
        var filterString =  [];
        $('#check-string').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterString);
            }else{
                // filtering edges
                filterString = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterString);
            }
            $('#cy_layout').trigger("change");
        });
        var filterAranet =  [];
        $('#check-aranet').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterAranet);
            }else{
                // filtering edges
                filterAranet = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterAranet);
            }
            $('#cy_layout').trigger("change");
        });
        var filterAgris =  [];
        $('#check-agris').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterAgris);
            }else{
                // filtering edges
                filterAgris = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterAgris);
            }
            $('#cy_layout').trigger("change");
        });
        var filterGO =  [];
        $('#check-geneontology').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterGO);
            }else{
                // filtering edges
                filterGO = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterGO);
            }
            $('#cy_layout').trigger("change");
        });
        var filterAracyc =  [];
        $('#check-aracyc').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterAracyc);
            }else{
                // filtering edges
                filterAracyc = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterAracyc);
            }
            $('#cy_layout').trigger("change");
        });
        var filterPP =  [];
        $('#check-plantpan').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterPP);
            }else{
                // filtering edges
                filterPP = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterPP);
            }
        });
        var filterKegg =  [];
        $('#check-kegg').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterKegg);
            }else{
                // filtering edges
                filterKegg = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterKegg);
            }
        });
        var filterPRM =  [];
        $('#check-PlantRegMap').on('change', function(event) {
            if( $(this).is(':checked') ){
                cy.add(filterPRM);
            }else{
                // filtering edges
                filterPRM = cy.filter('[database="' + this.value + '"]');
                cy.remove(filterPRM);
            }
        });
        /*--------------------------*/
        $('#cy_layout').on('change', function(e) {

            let nodes = cy.nodes().sort(function(a,b){return b.outgoers('node').length - a.outgoers('node').length});
            let left_row = 0;
            let right_row = 0;
            for( let i = 0; i < nodes.length; i++ ){
                let node = nodes[ i ];
                let col = (node.data('role') == 'target') ? 0 : 1;
                let row = (col == 1) ? left_row++ : right_row++;
                node.data('row',row);
                node.data('col',col);
            }

            let layouts = {};
            layouts['cola'] =  {
                name: 'cola',
                animate: false,
                nodeSpacing: 5,
                edgeLengthVal: 45,
                randomize: false
            };
            layouts['cise'] = {
                name: 'cise',
                animate: false,
                refresh: 100,
                fit: true,
                clusters: function( node ) {
                    //return 1;
                    cluster = roles[node.data("role")];
                    return cluster;
                },
                
            };
            layouts['bipartite'] = {
                name: 'grid',
                cols: 2,
                rows: row_count,
                position: function( node ) {
                    col = node.data('col');
                    row = node.data('row');
                    return { row, col };
                },
            };

            if (this.value in layouts) {
                let layout_params = layouts[this.value];
                let layout = cy.elements().layout(layout_params);
                layout.run();
            }
        });
        // Edge color on change
        evidence_list = {}
        for(var i = 0; i < interaction_evidence_list.length; i++){
            if(interaction_evidence_list[i] != ""){
                evidence_list[interaction_evidence_list[i]] = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substr(1,6);
            }
        }
        GO_list = {}
        for(var i = 0; i < shared_BP_GO_term_list.length; i++){
            if(shared_BP_GO_term_list[i] != ""){
                shared_BP_GO_term_list[shared_BP_GO_term_list[i]] = '#'+(0x1000000+Math.random()*0xffffff).toString(16).substr(1,6);
            }
        }
        database_list = {
            'STRING': '#001462',
            'AraNet': '#3d4c87',
            'AGRIS': '#6789a6',
            'PlantRegMap': '#91cbba',
            'PlantPan': '#d9cd9b',
            'AraCyc': '#d88a72',
            'KEGG': '#c64354',
            'Gene Ontology': '#93003a'
        }
        $('input:radio[name="radioColor"]').on('change', function(e) {
            if(this.value == "evidence"){
                cy.style().selector('edge').style({'line-color': '#bbb', "width": "2"}); // reset to default color
                var legend_content = "<ul style='list-style: none;'>";
                $("#network_legend").empty();
                for (key in evidence_list){
                    cy.style()
                      .selector('edge[interaction_evidence="' + key + '"]').style({
                         'line-color': evidence_list[key],
                         'width': '3',
                         "opacity": "0.8"
                      })
                      legend_content += "<li><span style='display: inline-block;float: left;margin-top: 5px;margin-right: 5px;height: 10px;width: 15px;background:" + evidence_list[key] + ";'></span> " + key + "</li>";
                }
                legend_content += "</ul>";
                $("#network_legend").append(legend_content);
                cy.style().update();
            }else if(this.value == "GO"){
                cy.style().selector('edge').style({'line-color': '#bbb', "width": "2"}); // reset to default color
                var legend_content = "<ul style='list-style: none;'>";
                $("#network_legend").empty();
                for (key in GO_list){
                    cy.style()
                      .selector('edge[shared_BP_GO_term="' + key + '"]').style({
                         'line-color': GO_list[key],
                         "width": "3",
                         "opacity": "0.8"
                      })
                      legend_content += "<li><span style='display: inline-block;float: left;margin-top: 5px;margin-right: 5px;height: 10px;width: 15px;background:" + GO_list[key] + ";'></span> " + key + "</li>";
                }
                legend_content += "<li><span style='display: inline-block;float: left;margin-top: 5px;margin-right: 5px;height: 10px;width: 15px;background:#bbb;'></span> none</li>";
                legend_content += "</ul>";
                $("#network_legend").append(legend_content);
                cy.style().update();
            }else if(this.value == "database"){
                cy.style().selector('edge').style({'line-color': '#bbb', "width": "2"}); // reset to default color
                var legend_content = "<ul style='list-style: none;'>";
                $("#network_legend").empty();
                for (key in database_list){
                    cy.style()
                      .selector('edge[database="' + key + '"]').style({
                         'line-color': database_list[key],
                         "width": "3",
                         "opacity": "0.8"
                      })
                      legend_content += "<li><span style='display: inline-block;float: left;margin-top: 5px;margin-right: 5px;height: 10px;width: 15px;background:" + database_list[key] + ";'></span> " + key + "</li>";
                }
                legend_content += "</ul>";
                $("#network_legend").append(legend_content);
                cy.style().update();
            }
        });
 }

/* ---------------------------------------------- */
/* ---------- Retrieve unique value of array ---- */
const unique = (value, index, self) => {
  return self.indexOf(value) === index
}

function download(content, mimeType, filename){
  var a = document.createElement('a')
  var blob = new Blob([content], {type: mimeType})
  var url = URL.createObjectURL(blob)
  a.setAttribute('href', url)
  a.setAttribute('download', filename)
  a.click()
}

function makePopper(ele) {
      let ref = ele.popperRef(); // used only for positioning
      if (ele.isEdge()) {
        ele.tippy = tippy(ref, { // tippy options:
            content: () => {
                let content = document.createElement('div');
                //content.style.zIndex = "90";
                let popper_text = ele.data("interaction_evidence");
                popper_text += " (" + ele.data("database")  + ")<br>"
                popper_text += " score: " + ele.data("score").toFixed(2);
                content.innerHTML = popper_text;
                return content;
            },
            trigger: 'manual' // probably want manual mode
        });
    } else if (ele.isNode()) {
        ele.tippy = tippy(ref, { // tippy options:
            content: () => {
                let content = document.createElement('div');
                let popper_text = "";
                if (ele.data("label") == ele.data("id")) {
                    popper_text = ele.data("label");
                } else {
                    popper_text = ele.data("label") + " (" + ele.data("id") + ")";
                }
                content.innerHTML = popper_text;
                return content;
            },
            trigger: 'manual' // probably want manual mode
        });
    }
}

function create_selection_box(chromosome, start, end) {
    var svg = d3.select("svg");
    svg.select("#selection").remove();
    svg.select("#leftpull").remove();
    svg.select("#rightpull").remove();
    var top = d3.select("#plotarea").attr("y");
    var height = d3.select("#plotarea").attr("height");
    var halve_height = 1*top + height/2;
    var left = chrXScale[chromosome](start);
    var width = chrXScale[chromosome](end) - left;
    var pull_r = 5
    svg.append("rect").attr("id", "selection")
            .attr("x",chrXScale[chromosome](start))
            .attr("y",top).attr("height",height)
            .attr("width",width).attr("stroke", "none")
            .attr("fill", "#8C4374").attr("opacity",0.5)
            .attr("chromosome",chromosome);
    svg.append("circle").attr("id", "leftpull")
            .attr("cx", chrXScale[chromosome](start) - pull_r)
            .attr("cy", halve_height).attr("r", pull_r)
            .attr("fill", "#8C4374").attr("fill-opacity", "0.1")
            .attr("stroke", "#8C4374").attr("stroke-opacity", "0.6")
            .on("mouseenter mouseleave", function(EVENT) {
                var el = d3.select(this), isEntering = EVENT.type === "mouseenter";
                el.attr(
                    "fill-opacity", isEntering ? "0.5" : "0.1" 
                );
            });
    svg.append("circle").attr("id", "rightpull")
            .attr("cx", chrXScale[chromosome](end) + pull_r)
            .attr("cy", halve_height).attr("r", pull_r)
            .attr("fill", "#8C4374").attr("fill-opacity", "0.1")
            .attr("stroke", "#8C4374").attr("stroke-opacity", "0.6")
            .on("mouseenter mouseleave", function(EVENT) {
                var el = d3.select(this), isEntering = EVENT.type === "mouseenter";
                el.attr(
                    "fill-opacity", isEntering ? "0.5" : "0.1" 
                );
            });
    var dragHandlerLeft = d3.drag()
        .on("drag", function(EVENT) {
            var selection = svg.select("#selection");
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var newX = chrXScale[chrom](chrXScale[chrom].invert(EVENT.x));
            var newWidth = 1.0 * curWidth + 1.0 * curX - newX;
            console.log(curWidth,curX,EVENT.x,newWidth);
            if (newWidth <= 0) {
                newWidth = curWidth;
                newX = curX;
            }
            selection.attr("x",newX).attr("width", newWidth);
            svg.select("#leftpull").attr("cx", newX - pull_r);
        })
        .on("end",function(EVENT) {
            var selection = svg.select("#selection");
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var start = Math.round(chrXScale[chrom].invert(curX));
            var end = Math.round(chrXScale[chrom].invert(1.0*curX + 1.0*curWidth));
            document.getElementById("start_position").value = start;
            document.getElementById("end_position").value = end;
            $('#ld_form').submit();
        }); 
    dragHandlerLeft(svg.select("#leftpull"));
    var dragHandlerRight = d3.drag()
        .on("drag", function(EVENT) {
            var selection = svg.select("#selection");
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            if (curX >= EVENT.x) {
                return;
            }
            var curWidth = selection.attr("width");
            var newX = chrXScale[chrom](chrXScale[chrom].invert(EVENT.x));
            var newWidth = newX - 1.0 * curX;
            selection.attr("width", newWidth);
            svg.select("#rightpull").attr("cx", newX + pull_r);
        })
        .on("end",function(EVENT) {
            var selection = svg.select("#selection");
            var chrom = selection.attr("chromosome");
            var curX = selection.attr("x");
            var curWidth = selection.attr("width");
            var start = Math.round(chrXScale[chrom].invert(curX));
            var end = Math.round(chrXScale[chrom].invert(1.0*curX + 1.0*curWidth));
            document.getElementById("start_position").value = start;
            document.getElementById("end_position").value = end;
            $('#ld_form').submit();
        }); 
    dragHandlerRight(svg.select("#rightpull"));
    $('#text_message').hide();
}
