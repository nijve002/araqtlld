// Generated by CoffeeScript 1.12.7
var chrXScale, draw, draw_qtl_plot, left_position, peak_chr, right_position, active_peak_id;

peak_chr = 0;

left_position = 0;

right_position = 0;

active_peak_id = 0;

chrXScale = {};

draw = function(data) {
  var axislabels, bLegend, bigRad, bottom, c, checkerboard, chrGap, ci, cur, curXPixel, curYPixel, darkGray, darkblue, draw_trait, h, hLegend, i, j, k, l, labelcolor, left, leftLegend, legend, len, len1, len2, len3, maincolor, maxLegendLines, n, nodig, onedig, pad, peakRad, pink, promises, purple, ref, ref1, ref2, ref3, right, svg, t, tickHeight, titlecolor, top, topLegend, totalChrLength, totalh, totalw, trait, twodig, w, wLegend, xloc, yloc;
  bLegend = traits.length > 1;
  d3.select("div#loading").remove();
  if (traits.length > 1) {
    d3.select("div#legend").style("opacity", 1);
  }
  d3.select("div#geneinput").style("opacity", 1);
  w = 700;
  h = 300;
  pad = {
    left: 60,
    top: 40,
    right: 40,
    bottom: 40,
    inner: 10
  };
  left = 60;
  right = left + w;
  top = 41;
  bottom = top + h;
  if (bLegend) {
    leftLegend = right + 10;
    topLegend = top;
    hLegend = h;
    wLegend = 230;
    maxLegendLines = 15;
  }
  totalw = w + (bLegend ? wLegend + 110 : 100);
  totalh = h + 100;
  peakRad = 2;
  bigRad = 5;
  chrGap = 8;
  tickHeight = (bottom - top) * 0.02;
  nodig = d3.format(".0f");
  onedig = d3.format(".1f");
  twodig = d3.format(".2f");
  darkGray = d3.rgb(200, 200, 200);
  pink = "hotpink";
  darkblue = "darkslateblue";
  purple = "#8C4374";
  labelcolor = "black";
  titlecolor = "blue";
  maincolor = "darkblue";
  totalChrLength = 0;
  ref = data.chrnames;
  for (j = 0, len = ref.length; j < len; j++) {
    c = ref[j];
    data.chr[c].length_bp = data.chr[c].end - data.chr[c].start;
    totalChrLength += data.chr[c].length_bp;
  }
  curXPixel = left + peakRad;
  curYPixel = bottom - peakRad;
  ref1 = data.chrnames;
  for (k = 0, len1 = ref1.length; k < len1; k++) {
    c = ref1[k];
    data.chr[c].length_pixel = Math.round((w - peakRad * 2) * data.chr[c].length_bp / totalChrLength);
    data.chr[c].start_Xpixel = curXPixel;
    data.chr[c].end_Xpixel = curXPixel + data.chr[c].length_pixel - 1;
    data.chr[c].start_Ypixel = curYPixel;
    data.chr[c].end_Ypixel = curYPixel - (data.chr[c].length_pixel - 1);
    curXPixel += data.chr[c].length_pixel;
    curYPixel -= data.chr[c].length_pixel;
  }
  data.chr["1"].start_Xpixel = left;
  data.chr["1"].start_Ypixel = bottom;
  cur = Math.round(pad.left + chrGap / 2);
  ref2 = data.chrnames;
  for (l = 0, len2 = ref2.length; l < len2; l++) {
    c = ref2[l];
    data.chr[c].start_Xpixel = cur;
    data.chr[c].end_Xpixel = cur + Math.round((w - chrGap * data.chrnames.length) / totalChrLength * data.chr[c].length_bp);
    chrXScale[c] = d3.scaleLinear().domain([data.chr[c].start, data.chr[c].end]).rangeRound([data.chr[c].start_Xpixel, data.chr[c].end_Xpixel]).clamp(true);
    cur = data.chr[c].end_Xpixel + chrGap;
  }
  d3.select("svg").remove();
  svg = d3.select("div#multiplot").append("svg").attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox", "0 0 " + totalw + " " + totalh).classed("svg-content", true);
  svg.append("rect").attr("id", "plotarea").attr("x", left).attr("y", top).attr("height", h).attr("width", w).attr("class", "innerBox");
  if (bLegend) {
    svg.append("rect").attr("x", leftLegend).attr("y", topLegend).attr("height", hLegend).attr("width", wLegend).attr("class", "innerBox");
    legend = svg.append("g").attr("id", "legend");
  }
  checkerboard = svg.append("g").attr("id", "checkerboard");
  ref3 = data.chrnames;
  for (i = n = 0, len3 = ref3.length; n < len3; i = ++n) {
    ci = ref3[i];
    if (i % 2 === 0) {
      checkerboard.append("rect").attr("x", data.chr[ci].start_Xpixel - chrGap / 2).attr("width", data.chr[ci].end_Xpixel - data.chr[ci].start_Xpixel + chrGap).attr("y", top).attr("height", h).attr("stroke", "none").attr("fill", darkGray).style("pointer-events", "none");
    }
  }
  axislabels = svg.append("g").attr("id", "axislabels").style("pointer-events", "none");
  axislabels.append("g").attr("id", "bottomX").selectAll("empty").data(data.chrnames).enter().append("text").text(function(d) {
    return d;
  }).attr("x", function(d) {
    return (data.chr[d].start_Xpixel + data.chr[d].end_Xpixel) / 2;
  }).attr("y", bottom + pad.bottom * 0.3).attr("fill", labelcolor);
  axislabels.append("text").text("Chromosome").attr("x", (left + right) / 2).attr("y", bottom + pad.bottom * 0.75).attr("fill", titlecolor).attr("text-anchor", "middle");
  xloc = left - pad.left * 0.65;
  yloc = (top + bottom) / 2;
  axislabels.append("text").text("LOD score").attr("x", xloc).attr("y", yloc).attr("transform", "rotate(270," + xloc + "," + yloc + ")").style("text-anchor", "middle").attr("fill", titlecolor);
  draw_trait = function(trait_data_set) {
    var curves, dottip, end_chr, fn, fn1, label, legendtip, legendtiptext, len10, len4, len5, len6, len7, len8, len9, lineColors, linkUrl, lod, lodcurve, lodcurve_yScale, marker, markers_for_chromosome, martip, maxlod, o, pd, peakmarker, prev_marker, q, r, ref4, ref5, s, start_chr, ticks, trait_data, traitaxes, u, v, x, xlink, yaxis;
    martip = d3.tip().attr("class", "d3-tip").offset([-10, 0]).html(function(EVENT, d) {
      return "marker: " + d[0] + " (" + d[3] + ")<br>position: " + d[1] + "<br>LOD score: " + d[2] + "<br><i>click for traits peaking here</i>";
    });
    maxlod = -1;
    for (o = 0, len4 = trait_data_set.length; o < len4; o++) {
      pd = trait_data_set[o];
      ref4 = Object.keys(pd.lodscores);
      for (q = 0, len5 = ref4.length; q < len5; q++) {
        marker = ref4[q];
        lod = pd.lodscores[marker].lod;
        if (maxlod < Math.abs(lod)) {
          maxlod = Math.abs(lod);
          peakmarker = marker;
        }
      }
    }
    lodcurve_yScale = d3.scaleLinear().domain([0, maxlod * 1.05]).range([bottom, top]);
    yaxis = svg.append("g").attr("class", "trait_data").attr("id", "yaxis");
    ticks = lodcurve_yScale.ticks(6);
    yaxis.selectAll("empty").data(ticks).enter().append("line").attr("y1", function(d) {
      return lodcurve_yScale(d);
    }).attr("y2", function(d) {
      return lodcurve_yScale(d);
    }).attr("x1", left).attr("x2", right).attr("stroke", "white").attr("stroke-width", "1");
    yaxis.selectAll("empty").data(ticks).enter().append("text").text(function(d) {
      if (maxlod > 10) {
        return nodig(d);
      } else {
        return onedig(d);
      }
    }).attr("y", function(d) {
      return lodcurve_yScale(d);
    }).attr("x", left - pad.left * 0.1).style("text-anchor", "end").attr("fill", labelcolor);
    lineColors = d3.scaleOrdinal(d3.schemeCategory10);
    lodcurve = function(c, ls) {
      return d3.line().x(function(p) {
        return chrXScale[c](ls[p].start);
      }).y(function(p) {
        return lodcurve_yScale(Math.abs(ls[p].lod));
      });
    };
    curves = svg.append("g").attr("id", "curves").attr("class", "trait_data");
    for (i = r = 0, len6 = trait_data_set.length; r < len6; i = ++r) {
      trait_data = trait_data_set[i];
      ref5 = data.chrnames;
      for (s = 0, len7 = ref5.length; s < len7; s++) {
        c = ref5[s];
        markers_for_chromosome = (function(c) {
          var ls, m, ref6, results1;
          ref6 = trait_data.lodscores;
          results1 = [];
          for (m in ref6) {
            ls = ref6[m];
            if (ls.chr === c) {
              results1.push(m);
            }
          }
          return results1;
        })(c);
        markers_for_chromosome.sort(function(a, b) {
          return trait_data.lodscores[a].start - trait_data.lodscores[b].start;
        });
        prev_marker = null;
        start_chr = data.chr[c].start;
        end_chr = data.chr[c].end;
        markers_for_chromosome.forEach((function(_this) {
          return function(m) {
            trait_data.lodscores[m].prev = start_chr;
            trait_data.lodscores[m].next = end_chr;
            if (prev_marker) {
              trait_data.lodscores[m].prev = trait_data.lodscores[prev_marker].start;
              trait_data.lodscores[prev_marker].next = trait_data.lodscores[m].start;
            }
            return prev_marker = m;
          };
        })(this));
        curves.append("path").datum(markers_for_chromosome).attr("id", trait_data.ID + "-" + trait_data.experiment).attr("d", lodcurve(c, trait_data.lodscores)).attr("class", "thickline").attr("stroke", lineColors(i)).style("pointer-events", "none").attr("fill", "none");
        if (trait_data_set.length === 1) {
          pd = trait_data_set[0];
          if (pd.lodscores[peakmarker].chr === c) {
            peak_chr = pd.lodscores[peakmarker].chr;
            left_position = pd.lodscores[peakmarker].prev;
            right_position = pd.lodscores[peakmarker].next;
          }
        }
      }
    }
    svg.append("rect").attr("class", "trait_data").attr("x", left).attr("y", top).attr("height", h).attr("width", w).attr("class", "outerBox");
    if ((typeof dot !== "undefined" && dot !== null) && (dot.chr != null) && dot.chr !== 0) {
      dottip = d3.tip().attr("class", "d3-tip").offset([-10, 0]).html(function(EVENT, d) {
        return dot.label + " (" + dot.chr + ":" + dot.pos + ")";
      });
      svg.call(dottip);
      svg.append("circle").attr("class", "trait_data").attr("id", "circle").attr("cx", chrXScale[dot.chr](dot.pos)).attr("cy", top).attr("r", bigRad).attr("fill", pink).attr("stroke", darkblue).attr("stroke-width", 1).attr("opacity", 1).on("mouseover", function() {
        return dottip.show(event);
      }).on("mouseout", function() {
        return dottip.hide();
      });
    } else {
      for (i = u = 0, len8 = trait_data_set.length; u < len8; i = ++u) {
        trait_data = trait_data_set[i];
        if (trait_data.chr != null) {
          (function(trait_data) {
            return svg.append("circle").attr("id", "trait_" + trait_data.ID).attr("cx", chrXScale[trait_data.chr](trait_data.start)).attr("cy", top).attr("r", bigRad).attr("fill", lineColors(i)).attr("stroke", darkblue).attr("stroke-width", 1).attr("opacity", 1);
          })(trait_data);
        }
      }
    }
    svg.call(martip);
    fn = function(trait_data) {
      return svg.append("g").attr("id", "markerCircle").attr("class", "trait_data").selectAll("empty").data(Object.entries(trait_data.lodscores)).enter().append("circle").attr("class", "trait_data").attr("id", function(td) {
        return "marker_" + td[0];
      }).attr("cx", function(td) {
        return chrXScale[td[1].chr](td[1].start);
      }).attr("cy", function(td) {
        return lodcurve_yScale(Math.abs(td[1].lod));
      }).attr("r", bigRad).attr("fill", purple).attr("stroke", "none").attr("stroke-width", "2").attr("opacity", 0).on("mouseover", function(event, td) {
        var label;
        d3.select(this).attr("opacity", 1);
        if (multiexperiment) {
          label = trait_data.experiment;
        } else {
          label = trait_data.ID;
        }
        return martip.show(event, [td[0], td[1].chr + ":" + td[1].start, Math.round(td[1].lod * 100) / 100, label]);
      }).on("mouseout", (function(td) {
        d3.select(this).attr("opacity", 0);
        return martip.hide();
      })).on("click", function(event, td) {
        var el, end, start;
        el = document.getElementById("collapse-form");
        if ($("#network-modal").is(":visible")) {
          start = td[1].prev;
          end = td[1].next;
          document.getElementById("chromosome_" + active_peak_id).selectedIndex = td[1].chr - 1;
          document.getElementById("start_position_" + active_peak_id).value = start;
          document.getElementById("end_position_" + active_peak_id).value = end;
          create_selection_box(active_peak_id,td[1].chr, td[1].prev, td[1].next);
          return $('#ld_form').submit();
        } else {
          return location.href = "/AraQTL/coregulation/?experiment_name=" + trait_data.experiment + "&query=" + td[0];
        }
      }).on("contextmenu", function(event, td) {
        var el, end, start;
        event.preventDefault();
        el = document.getElementById("collapse-form");
        if ($("#network-modal").is(":visible")) {
          start = td[1].prev;
          end = td[1].next;
          document.getElementById("chromosome").selectedIndex = td[1].chr - 1;
          document.getElementById("start_position").value = start;
          document.getElementById("end_position").value = end;
          create_selection_box(td[1].chr, td[1].prev, td[1].next);
          return $('#ld_form').submit();
        }
      });
    };
    for (v = 0, len9 = trait_data_set.length; v < len9; v++) {
      trait_data = trait_data_set[v];
      fn(trait_data);
    }
    traitaxes = svg.append("g").attr("id", "trait_data_axes").attr("class", "trait_data");
    traitaxes.append("text").text(titletxt).attr("x", (left + right) / 2).attr("y", top - pad.top / 2).attr("fill", maincolor).style("font-size", "18px");
    if (bLegend) {
      legendtip = d3.tip().attr("class", "d3-tip").offset([-10, 150]).html(function(EVENT, d) {
        return d;
      });
      svg.call(legendtip);
      fn1 = function(trait_data, i) {
        return xlink.append("text").text(label).attr("x", leftLegend + 50).attr("y", topLegend + 18 * i + 15).attr("fill", titlecolor).style("text-anchor", "start").on("mouseover", function(event) {
          svg.selectAll('path').attr("opacity", 0.1);
          svg.selectAll('path#' + trait_data.ID + '-' + trait_data.experiment).attr("opacity", 1);
          if (legendtiptext) {
            return legendtip.show(event, legendtiptext);
          }
        }).on("mouseout", function() {
          svg.selectAll('path').attr("opacity", 1);
          return legendtip.hide();
        });
      };
      for (i = x = 0, len10 = trait_data_set.length; x < len10; i = ++x) {
        trait_data = trait_data_set[i];
        if (i < maxLegendLines) {
          legend.append("line").attr("x1", leftLegend + 10).attr("y1", topLegend + 18 * i + 15).attr("x2", leftLegend + 40).attr("y2", topLegend + 18 * i + 15).attr("class", "thickline").attr("stroke", lineColors(i));
        }
        linkUrl = "";
        legendtiptext = "";
        label = "";
        if (i === maxLegendLines) {
          label = "...";
        } else if (multiexperiment) {
          label = trait_data.experiment;
          linkUrl = "/AraQTL/multiplot/?query=" + trait_data.ID + "&experiment_name=" + trait_data.experiment;
          legendtiptext = "<i>click to only show this profile</i>";
        } else if (trait_data.ID) {
          if (trait_data.name) {
            label = trait_data.ID + " (" + trait_data.name + ")";
          } else {
            label = trait_data.ID;
          }
          linkUrl = "/AraQTL/multiplot/?query=" + trait_data.ID + "&experiment_name=" + trait_data.experiment;
          legendtiptext = "<i>click to only show this profile</i>";
        } else if (trait_data.name) {
          label = trait_data.name;
        }
        xlink = traitaxes.append("a").attr("xlink:href", linkUrl).attr("xlink:show", "new");
        fn1(trait_data, i);
        if (i === maxLegendLines) {
          break;
        }
      }
    }
    svg.append("rect").attr("x", left).attr("y", top).attr("height", h).attr("width", w).attr("class", "outerBox");
    return svg.append("rect").attr("x", leftLegend).attr("y", topLegend).attr("height", hLegend).attr("width", wLegend).attr("class", "outerBox");
  };
  $("body").css("cursor", "progress");
  promises = [];
  for (t in traits) {
    trait = traits[t];
    if (trait.show) {
      promises.push(d3.json("../traitdata/?experiment_name=" + trait.experiment + "&query=" + trait.trait_id.toUpperCase()));
    }
  }
  Promise.allSettled(promises).then((function(_this) {
    return function(results) {
      var data_sets;
      data_sets = [];
      results.forEach(function(result) {
        if (result.value) {
          return data_sets.push(result.value);
        } else {
          return console.log(result.error);
        }
      });
      return draw_trait(data_sets);
    };
  })(this));
  return $("body").css("cursor", "default");
};

draw_qtl_plot = function() {
  return d3.json("../media/species/" + species + ".json").then(draw);
};
