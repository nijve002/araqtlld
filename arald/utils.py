from string import Template

queryTemplate = Template("""
prefix obo: <http://purl.obolibrary.org/obo/>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>
prefix dc: <http://purl.org/dc/elements/1.1/>
prefix faldo: <http://biohackathon.org/resource/faldo#>
prefix resource: <http://semanticscience.org/resource/>
prefix core: <http://purl.uniprot.org/core/>
prefix so: <http://purl.obolibrary.org/obo/so#>
prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/> # changed the ensemble version from 33 to 102
prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
 
SELECT DISTINCT
STR(?regulator_label) AS ?label
STR(?regulator_id) AS ?id
STR(?interaction_label) AS ?interaction_evidence
STR(?evidence_score) AS ?score
STR(?db_source) AS ?database
str(?function) AS ?shared_BP_GO_term
?link
STR(?gene_goid) AS ?gene_go_id
STR(?target_goid) AS ?target_go_id
?begin_pos
?end_pos
WHERE{
    # Determine the target gene
    VALUES ?target_gene { ensembl:$accession  } .
    # determine the QTL interval
    VALUES ?ref{ TAIR10:$chromosome }
    FILTER (?begin_pos >= $start && ?end_pos <= $end ) .

    # get the list of candidate in the QTL interval
    ?gene a obo:SO_0001217 .
    ?gene faldo:location ?gene_loc .
    ?gene_loc faldo:begin ?begin ;
    faldo:end ?end ;
    faldo:reference ?ref .
    ?begin faldo:position ?begin_pos .
    ?end faldo:position ?end_pos .

    { SELECT * WHERE {
    # query the candidate that interact with the target gene
    ?gene (resource:SIO_000701|^resource:SIO_000701|resource:SIO_001154) ?target_gene ;
    obo:RO_0000056 ?interaction .
    ?target_gene obo:RO_0000056 ?interaction .
    ?interaction a ?link.
    OPTIONAL {?interaction resource:SIO_000300 ?evidence_score . }
    ?link rdfs:label ?interaction_label .
    ?interaction resource:SIO_000253 ?db_source.
    } }

    UNION

    { SELECT *
        FROM <https://www.genome.jp/kegg/>
        FROM <https://plantcyc.org/>
        WHERE {
    # Get gene with similar pathway annotation
    ?target_gene resource:SIO_000255 ?link .
    ?gene        resource:SIO_000255 ?link .
    ?link rdfs:label ?interaction_label .
        ?link resource:SIO_000253 ?db_source.
    } }

    UNION

    { SELECT *
        FROM <http://purl.obolibrary.org/obo/go.owl>
        FROM <http://geneontology.org/>
        WHERE {
    # Get gene with similar pathway annotation
    ?target_gene resource:SIO_000255 ?target_go .
    ?target_go oboInOwl:hasOBONamespace "biological_process"^^xsd:string ;
                 rdfs:label ?function .
    ?gene        resource:SIO_000255 ?gene_go.
    ?gene_go   oboInOwl:hasOBONamespace "biological_process"^^xsd:string .
    ?gene_go  (rdfs:subClassOf|^rdfs:subClassOf){,2} ?target_go ;
          rdfs:label ?interaction_label ;
          resource:SIO_000253 ?db_source .
    ?gene_go   oboInOwl:id ?gene_goid .
    ?target_go oboInOwl:id ?target_goid .
    } }

    # get the gene label
    OPTIONAL {?gene rdfs:label ?regulator_label .}
    ?gene dc:identifier ?regulator_id .
}
""")

queryTemplate2 = Template("""
prefix obo: <http://purl.obolibrary.org/obo/>  
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>  
prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>  
prefix dc: <http://purl.org/dc/elements/1.1/>  
prefix faldo: <http://biohackathon.org/resource/faldo#>  
prefix resource: <http://semanticscience.org/resource/>  
prefix core: <http://purl.uniprot.org/core/>  
prefix so: <http://purl.obolibrary.org/obo/so#>  
prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>  
prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/33/arabidopsis_thaliana/TAIR10/>  
prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>  
SELECT DISTINCT (str(?regulator) AS ?candidate_gene) (str(?interaction_label) AS ?interaction_evidence) (str(?function) AS ?shared_BP_GO_term) ?begin_pos ?end_pos ?score
WHERE{
    # determine the QTL interval
    VALUES ?ref{ $chromosome }
    FILTER (?begin_pos >= $start && ?end_pos <= $end) .  
    # get the list of candidate in the QTL interval  
    ?gene a                     obo:SO_0001217 .  
    ?gene faldo:location            ?gene_loc .  
    ?gene_loc faldo:begin           ?begin ;  
              faldo:end             ?end ;  
              faldo:reference       ?ref .  
              ?begin faldo:position ?begin_pos .  
              ?end faldo:position   ?end_pos .  
    # query the candidate that interact with the target gene  
    ?gene (resource:SIO_000701|resource:SIO_001154) ensembl:$accession ;  
          obo:RO_0000056 ?interaction .  
    ensembl:$accession obo:RO_0000056 ?interaction .  
    ?interaction a ?interaction_type .
    ?interaction resource:SIO_000300 ?score .
    ?interaction_type rdfs:label ?interaction_label .
    # query the candidate that shared bp go term  
    ?gene resource:SIO_000255 ?go1 .  
    ensembl:$accession resource:SIO_000255 ?go2 .  
    ?go2 oboInOwl:hasOBONamespace "biological_process"^^<http://www.w3.org/2001/XMLSchema#string> ;  
         (rdfs:subClassOf|^rdfs:subClassOf){,2} ?go1 ;  
         rdfs:label ?function .  
    # get the gene label  
    ?gene rdfs:label ?regulator .  
}
""")


queryTemplate_hotspot = Template("""

""")

#endpoint = "http://ara-ld:8890/sparql/"
#endpoint = "http://81bcc6a2b7f1:8890/sparql/"
endpoint = "http://ara-ld-dev:8890/sparql/"
