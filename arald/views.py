from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from SPARQLWrapper import SPARQLWrapper, JSON
from arald import utils
from json import dumps,loads,JSONDecodeError
from arald.utils_sparql import get_candidate_multiple_targets_interaction, get_interacting_genes_from_gene_lists, filter_interaction, get_interaction_from_single_or_multiple_qtls, QTL, network_df_to_json, get_gene_label, gene_list_to_nodes, get_genes_in_interval
from arald.utils_go import calculate_go_similarity, GODAG, TERMCOUNTS
from main.utils import get_cis_LOD_region
from django.views.decorators.csrf import csrf_exempt
import logging
import pandas as pd

logger = logging.getLogger('django')

# Create your views here.
from django.shortcuts import render

def app(request):
    return render(request, 'home.html')

def get_interaction_from_multiple_targets(request):
    # get values
    try:
        data = loads(request.body)
    except JSONDecodeError:
        return(JsonResponse({'status':'false','message':'cannot decode JSON'},status=404))
        
    experiment_name = data['experiment_name']
    
    targets = data['targets'].split(",")
  
    qtl = data['qtls'][0]
    start_position = qtl['start_position']
    end_position = qtl['end_position']
    chromosome = qtl['chromosome']

    # check all values are set
    if not all([targets, start_position, end_position, chromosome]):
        return(JsonResponse({'status':'false','message':'empty values'},status=404))

    logger.error(experiment_name,chromosome, start_position,end_position)
    cis_lod = get_cis_LOD_region(experiment_name,\
            chromosome, start_position,end_position )

    # Get candidate-target interactions
    network = get_candidate_multiple_targets_interaction(targets, int(chromosome), int(start_position), int(end_position))
    network = filter_interaction(network, go_score=4)
    network = network[network.gene_label1 != network.gene_label2] # remove self interaction

    # Get target-target interaction
    network2 = get_interacting_genes_from_gene_lists(targets, targets)
    network2 = filter_interaction(network2, go_score=4)
    candidates = get_genes_in_interval(int(chromosome), int(start_position), int(end_position))
    network2["loc_id1"] = 'target'
    network2["loc_id2"] = 'target'
    for _, row in network2.iterrows(): # Assign role to the genes
        if row['gene_id1'] in candidates:
            row['loc_id1'] = 'candidate'
        if row['gene_id2'] in candidates:
            row['loc_id2'] = 'candidate'
    network2.columns = network.columns

    # Combine both networks
    combined_network = pd.concat([network,network2])
    
    # replace NaN with -1, since JSON cannot handle NaN
    combined_network.fillna(-1, inplace = True)

    # Create and return JSON containing the network, list of eQTLs, and the target gene name
    ret_dict = {}
    ret_dict['results_sparql'] = network_df_to_json(combined_network) # convert the interaction df to JSON
    ret_dict['locs'] = sorted(pd.unique(['target'] + \
        combined_network[['loc_id1', 'loc_id2']].values.ravel().tolist()), reverse =True) # get the list of eQTL location
    ret_dict['targets'] = targets
    ret_dict['cis_lod'] = cis_lod
    ret_dict['total_candidate'] = len(cis_lod)
    return JsonResponse(ret_dict)

def get_interaction_from_single_target(request):
    # get values
    try:
        data = loads(request.body)
    except JSONDecodeError:
        return(JsonResponse({'status':'false','message':'cannot decode JSON'},status=404))

    target_gene = data['targets']
    experiment_name = data['experiment_name']

    # Define and create the list of eQTLs
    qtls = []
    cis_lod = {}
    for qtl in data['qtls']:
        #if qtl['start_position'] == qtl['end_position']:
        #    continue
        logger.error('{} {} {}'.format(qtl['chromosome'], qtl['start_position'],qtl['end_position']))
        qtls.append(QTL(target_gene,qtl['chromosome'], qtl['start_position'],qtl['end_position']))
        cis_lod.update(get_cis_LOD_region(experiment_name,\
            qtl['chromosome'], qtl['start_position'],qtl['end_position'] ))
    
    if not qtls:
        ret_dict = {} 
        ret_dict['target'] = target_gene
        return JsonResponse(ret_dict)

    # Create an interaction dataframe  
    res = get_interaction_from_single_or_multiple_qtls(qtls)
    res = filter_interaction(res, go_score=4)
    
    # replace NaN with -1, since JSON cannot handle NaN
    res.fillna(-1, inplace = True)

    # Create a list of candidate genes and convert them to JSON 
    all_candidates = list(cis_lod.keys())
    interacting_candidates = list(set(list(res.gene_id1) + list(res.gene_id2)))
    non_interacting_candidates = list(set(all_candidates) - set(interacting_candidates))
    #nodes = gene_list_to_nodes(non_interacting_candidates)

    # Create and return JSON containing the network, list of eQTLs, and the target gene name
    ret_dict = {}
    ret_dict['results_sparql'] =  network_df_to_json(res, get_gene_label(target_gene)) # convert the interaction df to JSON
    #ret_dict['results_sparql']["elements"].extend(nodes) 
    ret_dict['locs'] = sorted(pd.unique(['target'] + res[['loc_id1', 'loc_id2']].values.ravel().tolist()), reverse =True) # get the list of eQTL location
    ret_dict['target'] = target_gene
    ret_dict['cis_lod'] = cis_lod   
    ret_dict['total_candidate'] = len(cis_lod)
    return JsonResponse(ret_dict)

def get2interactions(request):
    # get values
    target_gene = request.GET.get('target_gene')
    experiment_name = request.GET.get('experiment_name')
    chromosome = request.GET.get('chromosome')
    start_position = request.GET.get('start')
    end_position = request.GET.get('end')

    # Define and create the list of eQTLs
    qtls = []
    cis_lod = {}
    
    qtls.append(QTL(target_gene,chromosome, start_position,end_position))
    cis_lod.update(get_cis_LOD_region(experiment_name,\
        chromosome, start_position,end_position))


    # Create an interaction dataframe  
    res = get_interaction_from_single_or_multiple_qtls(qtls)
    
    # replace NaN with -1, since JSON cannot handle NaN
    res.fillna(-1, inplace = True)

    # Create a list of candidate genes and convert them to JSON 
    all_candidates = list(cis_lod.keys())
    interacting_candidates = list(set(list(res.gene_id1) + list(res.gene_id2)))
    non_interacting_candidates = list(set(all_candidates) - set(interacting_candidates))
    #nodes = gene_list_to_nodes(non_interacting_candidates)

    # Create and return JSON containing the network, list of eQTLs, and the target gene name
    ret_dict = {}
    ret_dict['results_sparql'] =  network_df_to_json(res, get_gene_label(target_gene)) # convert the interaction df to JSON
    #ret_dict['results_sparql']["elements"].extend(nodes) 
    ret_dict['locs'] = sorted(pd.unique(['target'] + res[['loc_id1', 'loc_id2']].values.ravel().tolist()), reverse =True) # get the list of eQTL location
    ret_dict['target'] = target_gene
    ret_dict['cis_lod'] = cis_lod   
    return JsonResponse(ret_dict)


def get_interactions(request):
    # check the number of targets
    try:
        data = loads(request.body)
    except JSONDecodeError:
        return(JsonResponse({'status':'false','message':'cannot decode JSON'},status=404))

    targets = data['targets'].split(",")

    if len(targets) == 1:
        return get_interaction_from_single_target(request)
    else:
        return get_interaction_from_multiple_targets(request)
