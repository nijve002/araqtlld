from django.urls import  re_path
from arald.views import app
from arald.views import get_interactions, get2interactions

urlpatterns = [
    re_path(r'^$', app),
    re_path(r'^get_interactions', get_interactions, name='get_interactions'),
    re_path(r'^get2interactions', get2interactions, name='get2interactions'),
]
