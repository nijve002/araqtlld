# -*- coding: utf-8 -*-
"""
Created on Fri May 21 11:28:07 2021

Functions and modules for SPARQL querying.

@author: harta005
"""
### For GO semantic similarity calculation
from arald.utils_go import *

### For SPARQL querying
from SPARQLWrapper import SPARQLWrapper, TSV, CSV
import pandas as pd
pd.set_option('display.max_rows', 10)
import numpy as np
import logging
from itertools import combinations
import multiprocessing
from joblib import Parallel, delayed, parallel_backend
from main.utils import is_tf, get_gene_name, get_gene_prob

logger = logging.getLogger(__name__)

### Class definition
class QTL:
    def __init__(self, trait, chro, begin, end):
        self.trait = trait
        self.chro = chro
        self.begin = begin
        self.end = end

### GLOBAL VARIABLES
#ENDPOINT = "http://383e928f91c6:8890/sparql/"
#ENDPOINT = "http://6cc20fe6d375:8890/sparql/"
#ENDPOINT = "http://17e0d0c47d09:8890/sparql/"
ENDPOINT = "http://edc084cfb2da:8890/sparql/"

### Function definition for SPARQL querying
def sparql2csv(sparql): 
    '''
    Convert SPARQL query result to a dataframe.

    Parameters
    ----------
    sparql : SPARQLWrapper object
        SPARQL query
    Returns
    -------
    df : pandas dataframe

    '''
    sparql.setTimeout(1000000)
    try:
        sparql.setReturnFormat(TSV)
    except TimeOutError:
        sparql2csv(sparql)
    result = sparql.query().convert().decode("utf-8").split("\n")
    result.remove('')
    if len(result) == 2:
        return None
    lst = []
    for line in result:
        line = line.split("\t")
        line = [ele.strip('"') for ele in line]
        if len(line) > 1:
            lst.append(line)
        else:
            lst.extend(line)
    if type(lst[0]) == str:
        colname = [lst[0]]
    else:
        colname = lst[0]
    df = pd.DataFrame(lst[1:])
    df = df.iloc[:, :len(colname)].dropna()
    df.columns = colname
    return df

def get_gene_label(gene):
    """
    Convert Arabidopsis gene ID to gene label if exist.
    Make sure the SPARQL endpoint is defined correctly,
    e.g. http://localhost:8891/sparql/.

    Parameters
    ----------
    gene : str
        The gene ID, e.g. AT2G45660.

    Returns
    -------
    result : str
        The label for the gene ID, e.g. SOC1.
    """
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""  
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>          
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>      
    SELECT DISTINCT
    STR(?gene_label) AS ?label
    WHERE {{
        ensembl:{gene} rdfs:label ?gene_label
        }}
    """)
    sparql.setReturnFormat(CSV)
    result = sparql.query().convert().decode("utf-8").split("\n")[1].strip('"')
    if result == '':
        return gene
    return result

def is_tf_sparql(gene):
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""  
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>          
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>      
    prefix resource: <http://semanticscience.org/resource/>
    prefix obo: <http://purl.obolibrary.org/obo/>
    ASK WHERE {{ ensembl:{gene} resource:SIO_000255 obo:GO_0003700 }}
    """)
    sparql.setReturnFormat(CSV)
    result = sparql.query().convert().decode("utf-8").split("\n")[1].strip('"')
    if result == '':
        return int(0)
    return int(result)


def filter_interaction(df,
              string_score = 0,
              go_score = 0,
              aranet_score = 0,
              plantpan_score = 0.05,
              database_list = ['STRING', 'AraNet', 'AGRIS', 'PlantRegMap',
                          'PlantPan', 'AraCyc', 'KEGG', 'Gene Ontology', 'PlantCistromeDB']):
    """
    Filter the interaction dataframe by score and database.

    Parameters
    ----------
    df : pandas dataframe
        A dataframe object containing source, target, score, evidence and the 
        database of interaction.
    string_score : FLOAT, optional
        DESCRIPTION. The default is 600.
    go_score : FLOAT, optional
        DESCRIPTION. The default is 4.
    aranet_score : FLOAT, optional
        DESCRIPTION. The default is 5.
    plantpan_score : FLOAT, optional
        DESCRIPTION. The default is 0.05.
    database : STR, optional
        DESCRIPTION. The default is ['STRING', 'AraNet', 'AGRIS', 'PlantRegMap', 'PlantPan', 'AraCyc', 'KEGG', 'Gene Ontology'].
        
    Returns
    -------
    A filtered interaction dataframe.

    """
    filtered_df = df.copy()
    filtered_df = filtered_df[filtered_df['database'].isin(database_list)]
    filtered_df = filtered_df.drop(filtered_df[(filtered_df['database'] == 'Gene Ontology') & 
                                               (filtered_df['score'] <=  go_score)].index)
    filtered_df = filtered_df.drop(filtered_df[(filtered_df['database'] == 'STRING') & 
                                               (filtered_df['score'] <= string_score)].index)
    filtered_df = filtered_df.drop(filtered_df[(filtered_df['database'] == 'AraNet') & 
                                           (filtered_df['score'] <= aranet_score)].index)
    filtered_df = filtered_df.drop(filtered_df[(filtered_df['database'] == 'PlantPan') & 
                                       (filtered_df['score'] >= plantpan_score)].index)

    #filtered_df = filtered_df[filtered_df['database'].isin(database_list)]
    #for index, row in df.iterrows():        
    #    if row.database == 'Gene Ontology' and row.score < go_score:
    #        filtered_df = filtered_df.drop([index])
    #    if row.database == 'STRING' and row.score < string_score:
    #        filtered_df = filtered_df.drop([index])
    #    if row.database == 'PlantPan' and row.score > plantpan_score:
    #        filtered_df = filtered_df.drop([index])
    #    if row.database == 'AraNet' and row.score < aranet_score:
    #        filtered_df = filtered_df.drop([index])
    return filtered_df

def get_genes_in_interval(chro, begin, end):
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""
    prefix obo: <http://purl.obolibrary.org/obo/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix dc: <http://purl.org/dc/elements/1.1/>
    prefix faldo: <http://biohackathon.org/resource/faldo#>
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
    prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/>
      
    SELECT DISTINCT 
    (str(?gene_id) AS ?candidate_id)
    (str(?gene_label) AS ?candidate_label)
    WHERE{{
    
    # determine the QTL interval
    VALUES ?chr {{ TAIR10:{chro} }}
    FILTER (?begin_pos >= {begin} && ?end_pos <= {end}) .
    
    # get the list of candidate in the QTL interval
    ?gene a                         obo:SO_0001217 ;
          faldo:location            ?gene_loc .
    ?gene_loc faldo:begin           ?begin ;
              faldo:end             ?end ;
              faldo:reference       ?chr.
              ?begin faldo:position ?begin_pos .
              ?end faldo:position   ?end_pos .
    
    # get the gene label
    OPTIONAL {{ ?gene rdfs:label ?gene_label . }}
    ?gene dc:identifier ?gene_id .
    }}
    """)
    res = sparql2csv(sparql)
    return res.candidate_label.values.tolist()

def get_candidate_target_interaction(target_gene, chro, begin, end):
    '''
    Return candidate genes for an eQTL including their interaction with the
    target gene
    Make sure the SPARQL endpoint is defined correctly,
    e.g. http://localhost:1234/sparql/.

    Parameters
    ----------
    target_gene : str
        The gene associated with the eQTL.
    chro : int
        The chromosome of the eQTL.
    begin : int
        The start location of the eQTL interval.
    end : int
        The end location of the eQTL interval.

    Returns
    -------
    res : pandas dataframe
        A table containing a list of candidate genes for an eQTL including 
        their evidence.
    '''
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""
    prefix obo: <http://purl.obolibrary.org/obo/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>
    prefix dc: <http://purl.org/dc/elements/1.1/>
    prefix faldo: <http://biohackathon.org/resource/faldo#>
    prefix resource: <http://semanticscience.org/resource/>
    prefix core: <http://purl.uniprot.org/core/>
    prefix so: <http://purl.obolibrary.org/obo/so#>
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
    prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/>
    prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
    prefix obo2: <http://www.geneontology.org/formats/oboInOwl#>
    
    SELECT DISTINCT
    STR(?regulator_label) AS ?candidate_label
    STR(?regulator_id) AS ?candidate_id
    STR(?interaction_label) AS ?evidence
    STR(?evidence_score) AS ?score
    STR(?gene_go_id) AS ?candidate_go
    STR(?target_go_id) AS ?target_go
    STR(?db_source) AS ?database
    ?link
    ?interaction
    
    WHERE{{
        # Determine the target gene
        VALUES ?target_gene {{ ensembl:{target_gene} }} .
    
        # determine the QTL interval
        VALUES ?ref {{ TAIR10:{chro} }}
        FILTER (?begin_pos >= {begin} && ?end_pos <= {end} ) .
    
        # get the list of candidates in the QTL interval
        #?gene a obo:SO_0001217 .
        ?gene faldo:location ?gene_loc .
        ?gene_loc faldo:begin ?begin ;
        faldo:end ?end ;
        faldo:reference ?ref .
        ?begin faldo:position ?begin_pos .
        ?end faldo:position ?end_pos .
    
        {{ SELECT * WHERE {{
        # query the candidate that interact with the target gene
        ?gene (resource:SIO_000701|^resource:SIO_000701|resource:SIO_001154) ?target_gene ;
        obo:RO_0000056 ?interaction .
        ?target_gene obo:RO_0000056 ?interaction .
    
        ?interaction a ?link.
        OPTIONAL {{ ?interaction resource:SIO_000300 ?evidence_score . }}
        ?link rdfs:label ?interaction_label .
        ?interaction resource:SIO_000253 ?db_source.
        }} }}
        
        UNION
        
        {{ SELECT *
            FROM <https://www.genome.jp/kegg/>
            FROM <https://plantcyc.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?target_gene resource:SIO_000255 ?link .
        ?gene        resource:SIO_000255 ?link .
    
        ?link rdfs:label ?interaction_label .
            ?link resource:SIO_000253 ?db_source.
        }} }}
        
            UNION
    
        {{ SELECT *
            FROM <http://purl.obolibrary.org/obo/go.owl>
            FROM <http://geneontology.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?target_gene resource:SIO_000255 ?link .
            ?link        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .
        ?gene        resource:SIO_000255 ?gene_go        .
            ?gene_go        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .

        # ?link (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?gene_go.
        #     ?link rdfs:label ?interaction_label .
        #     ?link resource:SIO_000253 ?db_source.
        ?gene_go (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?link.
            ?gene_go rdfs:label ?interaction_label .
            ?gene_go resource:SIO_000253 ?db_source.

        ?gene_go obo2:id ?gene_go_id .
        ?link    obo2:id ?target_go_id .
        }} }}
    
    # get the gene label
    OPTIONAL {{ ?gene rdfs:label ?regulator_label . }}
    ?gene dc:identifier ?regulator_id .
    }}
    """)
    if begin == end:
        df = pd.DataFrame(columns=['candidate_label', 'candidate_id', 'evidence', 'score', 'candidate_go', 'target_go',
            'database', 'link', 'interaction'])
    else:
        df = sparql2csv(sparql)
    gene_label = get_gene_name(target_gene)
    df['target_label'] = gene_label
    df['target_id'] = target_gene
    df['score'] = df['score'].replace(r'^\s*$', np.nan, regex=True)
    df['score'] = df.score.astype(float)
    for index, row in df.iterrows():
        if not row.candidate_label:
            df.loc[index, ('candidate_label')] = row.candidate_id
        if row.database == 'Gene Ontology':
            df.loc[index, ("score")] = calculate_go_similarity(row.candidate_go, row.target_go, GODAG, TERMCOUNTS)\

    # Get candidate genes without interaction to the target genes
    # interacting_candidates = df.candidate_label.values.tolist()
    # all_candidates = get_genes_in_interval(chro, begin, end)
    # non_interacting_candidates = list(set(all_candidates) - set(interacting_candidates))
    # for gene in non_interacting_candidates:
    #     df = df.append({'candidate_label': gene}, ignore_index=True)
    return(df)

def get_gene_with_distant_eqtl(peak_data, chro, begin, end, lod):
    target_genes = (peak_data[(peak_data.chr == chro) & (peak_data.LOD_score >= lod) &
                          (peak_data.upstream_bp <= end) & 
                          (peak_data.downstream_bp >= begin)]['gene_name'])
    return target_genes.tolist()


def get_interacting_genes_from_gene_lists(gene_list1, gene_list2):
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""
    prefix obo: <http://purl.obolibrary.org/obo/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>
    prefix dc: <http://purl.org/dc/elements/1.1/>
    prefix faldo: <http://biohackathon.org/resource/faldo#>
    prefix resource: <http://semanticscience.org/resource/>
    prefix core: <http://purl.uniprot.org/core/>
    prefix so: <http://purl.obolibrary.org/obo/so#>
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
    prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/>
    prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>

    SELECT DISTINCT 
    STR(?label1) AS ?gene_label1
    STR(?id1) AS ?gene_id1
    STR(?goid1) AS ?go_id1
    STR(?label2) AS ?gene_label2
    STR(?id2) AS ?gene_id2
    STR(?goid2) AS ?go_id2
    STR(?interaction_label) AS ?evidence
    STR(?evidence_score) AS ?score
    STR(?db_source) AS ?database

    WHERE {{

    # determine the target gene
    VALUES ?genes1 {{ {'"' + '" "'.join(gene_list1) + '"'} }}
    VALUES ?genes2 {{ {'"' + '" "'.join(gene_list2) + '"'} }}

    # get the list of candidate in the QTL interval
    ?gene1 dc:identifier ?genes1 ;
           a obo:SO_0001217.
    ?gene2 dc:identifier ?genes2 ;
           a obo:SO_0001217.
    FILTER (?gene1 != ?gene2)

       {{ SELECT * WHERE {{
        # query the candidate that interact with the target gene
        ?gene1 (resource:SIO_000701|resource:SIO_001154) ?gene2;
               obo:RO_0000056 ?interaction .
        ?gene2 obo:RO_0000056 ?interaction .

        ?interaction a ?link.
        OPTIONAL {{ ?interaction resource:SIO_000300 ?evidence_score . }}
        ?link rdfs:label ?interaction_label .
        ?interaction resource:SIO_000253 ?db_source.
        }} }}

        UNION

        {{ SELECT *
            FROM <https://www.genome.jp/kegg/>
            FROM <https://plantcyc.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?gene1 resource:SIO_000255 ?link .
        ?gene2        resource:SIO_000255 ?link .

        ?link rdfs:label ?interaction_label .
            ?link resource:SIO_000253 ?db_source.
        }} }}

        UNION

        {{ SELECT *
            FROM <http://purl.obolibrary.org/obo/go.owl>
            FROM <http://geneontology.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?gene1 resource:SIO_000255 ?go1 .
            ?go1        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .
        ?gene2        resource:SIO_000255 ?go2        .
            ?go2        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .        
        FILTER (?go1 != obo:GO_0008150) # filter out biological process term
        FILTER (?go2 != obo:GO_0008150)
        ?go1 (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?go2 ;
             rdfs:label ?interaction_label ;
             resource:SIO_000253 ?db_source.
        ?go1 oboInOwl:id ?goid1 .
        ?go2 oboInOwl:id ?goid2 .
        }} }}

    # get the gene label
    OPTIONAL {{ ?gene1 rdfs:label ?label1 .}}
    OPTIONAL {{ ?gene2 rdfs:label ?label2 .}}
    ?gene1 dc:identifier ?id1 .
    ?gene2 dc:identifier ?id2 .
    }} 
    """)
    df = sparql2csv(sparql)
    df['score'] = df['score'].replace(r'^\s*$', np.nan, regex=True)
    df['score'] = df.score.astype(float)
    for index, row in df.iterrows():
        if not row.gene_label1:
            df.loc[index, ("gene_label1")] = row.gene_id1
        if not row.gene_label2:
            df.loc[index, ("gene_label2")] = row.gene_id2   
        if row.database == 'Gene Ontology':
            df.loc[index, ("score")] = calculate_go_similarity(row.go_id1, row.go_id2, GODAG, TERMCOUNTS)      
    return df

def get_candidate_multiple_targets_interaction(target_genes, chromosome, begin, end):
    sparql = SPARQLWrapper(ENDPOINT)
    sparql.setQuery(f"""
    prefix obo: <http://purl.obolibrary.org/obo/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>
    prefix dc: <http://purl.org/dc/elements/1.1/>
    prefix faldo: <http://biohackathon.org/resource/faldo#>
    prefix resource: <http://semanticscience.org/resource/>
    prefix core: <http://purl.uniprot.org/core/>
    prefix so: <http://purl.obolibrary.org/obo/so#>
    prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
    prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/>
    prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>

    SELECT DISTINCT
    STR(?candidate_label) AS ?gene_label1
    STR(?candidate_id) AS ?gene_id1
    STR(?goid1) AS ?go_id1
    STR(?target_label) AS ?gene_label2
    STR(?target_id) AS ?gene_id2
    STR(?goid2) AS ?go_id2
    STR(?interaction_label) AS ?evidence
    STR(?evidence_score) AS ?score
    STR(?db_source) AS ?database

    WHERE {{

    # determine the target gene based on the list
    VALUES ?target_genes  {{ {'"' + '" "'.join(target_genes) + '"'} }}

    ?target_gene dc:identifier ?target_genes ;
                 a obo:SO_0001217.

    # determine the candidate gene based on the QTL interval
    VALUES ?ref {{ TAIR10:{chromosome} }}
    FILTER (?begin_pos >= {begin} && ?end_pos <= {end} ) .

    # get the list of candidate in the QTL interval
    ?candidate_gene 	a	obo:SO_0001217 .
    ?candidate_gene faldo:location ?gene_loc .
    ?gene_loc faldo:begin ?begin ;
              faldo:end ?end ;
              faldo:reference ?ref .
    ?begin	faldo:position ?begin_pos .
    ?end	faldo:position ?end_pos .

    # Get the interaction between candidate and target
       {{ SELECT * WHERE {{
        # query the candidate that interact with the target gene
        ?candidate_gene (resource:SIO_000701|^resource:SIO_000701|resource:SIO_001154) ?target_gene ;
                        obo:RO_0000056 ?interaction .
        ?target_gene    obo:RO_0000056 ?interaction .

        ?interaction a ?link.
        OPTIONAL {{?interaction resource:SIO_000300 ?evidence_score . }}
        ?link rdfs:label ?interaction_label .
        ?interaction resource:SIO_000253 ?db_source.
        }} }}

        UNION

        {{ SELECT *
            FROM <https://www.genome.jp/kegg/>
            FROM <https://plantcyc.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?candidate_gene  resource:SIO_000255 ?link .
        ?target_gene     resource:SIO_000255 ?link .

        ?link rdfs:label ?interaction_label .
            ?link resource:SIO_000253 ?db_source.
        }} }}

        UNION

        {{ SELECT *
            FROM <http://purl.obolibrary.org/obo/go.owl>
            FROM <http://geneontology.org/>
            WHERE {{
        # Get gene with similar pathway annotation
        ?candidate_gene  resource:SIO_000255 ?go_candidate .
        ?go_candidate    oboInOwl:hasOBONamespace "biological_process"^^xsd:string .
        ?target_gene     resource:SIO_000255 ?go_target .
        ?go_target       oboInOwl:hasOBONamespace "biological_process"^^xsd:string .

        ?go_candidate (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?go_target ;
            rdfs:label ?interaction_label ;
            resource:SIO_000253 ?db_source.
        ?go_candidate oboInOwl:id ?goid1 .
        ?go_target    oboInOwl:id ?goid2 .
        }} }}

    # get the gene label
    OPTIONAL {{?candidate_gene rdfs:label ?candidate_label .}}
    OPTIONAL {{?target_gene rdfs:label ?target_label .}}
    ?candidate_gene dc:identifier ?candidate_id .
    ?target_gene dc:identifier ?target_id .
    }}
    """)
    df = sparql2csv(sparql)
    #df.columns = ['gene_label1', 'gene_id1', 'go_i', 'target_label', 'target_id', 'target_go', 'interaction_evidence', 'score', 'database']
    df['score'] = df['score'].replace(r'^\s*$', -1, regex=True)
    df['score'] = df.score.astype(float)
    df['loc_id1'] = 'candidate'
    df['loc_id2'] = 'target'
    for index, row in df.iterrows():
        if not row.gene_label1:
            df.loc[index, ("gene_label1")] = row.gene_id1
        if not row.gene_label2:
            df.loc[index, ("gene_label2")] = row.gene_id2   
        if row.database == 'Gene Ontology':
            df.loc[index, ("score")] = calculate_go_similarity(row.go_id1, row.go_id2, GODAG, TERMCOUNTS)     
    return df

def get_candidate_gene_list(qtl):
    candidates = get_candidate_target_interaction(qtl.trait, qtl.chro, qtl.begin, qtl.end)
    #candidates = filter_interaction(candidates)
    candidates = list(candidates['candidate_id'].drop_duplicates())
    qtl_id = f"""Chr: {qtl.chro}: {int(int(qtl.begin)/1000000)}-{int(int(qtl.end)/1000000)} Mb"""
    return (qtl_id, candidates)

def get_interacting_genes_from_two_QTLs(qtl1, qtl2):
    qtl_id1, candidates1 = get_candidate_gene_list(qtl1)
    qtl_id2, candidates2 = get_candidate_gene_list(qtl2)
    df = get_interacting_genes_from_gene_lists(candidates1, candidates2)
    df['loc_id1'] = qtl_id1
    df['loc_id2'] = qtl_id2
    return df

def get_interaction_from_single_or_multiple_qtls(qtls):
    mult_eqtl = None
    # Check the number of eQTL
    if len(qtls) > 1:
        # Get interacting genes from multiple eQTLs
        qtl_comb = combinations(qtls, 2)
        res = []
        with parallel_backend('threading', n_jobs=6):
            res.extend(Parallel()(delayed(get_interacting_genes_from_two_QTLs)(i[0], i[1]) for i in qtl_comb))
        mult_eqtl = pd.concat(res)
        #mult_eqtl = filter_interaction(mult_eqtl, go_score = 4)
    
        
    # Concat the mult_eqtl with interaction to the target gene
    for qtl in qtls:
        df = get_candidate_target_interaction(qtl.trait, qtl.chro, qtl.begin, qtl.end)
        #df = filter_interaction(df, go_score = 4)
        df = df[['candidate_label', 'candidate_id', 'evidence', 'score', 'candidate_go',
           'target_go', 'database', 'target_label','target_id']]
        df.columns = ['gene_label1', 'gene_id1', 'evidence', 'score', 'go_id1',
           'go_id2', 'database', 'gene_label2','gene_id2']
        df['loc_id1'] = f"""Chr: {qtl.chro}: {int(int(qtl.begin)/1000000)}-{int(int(qtl.end)/1000000)} Mb"""
        df['loc_id2'] = 'target'
        if mult_eqtl is None:
            mult_eqtl = df
        else:
            mult_eqtl = pd.concat([mult_eqtl, df])
            
    return mult_eqtl

def network_df_to_json(network, target_gene=""):
    network_json = {}
    network_json["elements"] = []
    data = [pd.concat([network.gene_id1,network.gene_id2]),\
            pd.concat([network.loc_id1,network.loc_id2]),\
            pd.concat([network.gene_label1,network.gene_label2])]
    headers = ["gene_id","loc_id","label"]
    genes = pd.concat(data, axis=1, keys=headers)
    #genes = list(set(list(network.gene_id1) + list(network.gene_id2)))
    #genes = [gene for gene in genes if str(genes) != 'nan']
    genes.reset_index()
    genes = genes.drop_duplicates()

    for index_gene, gene in genes.iterrows():
        if gene.loc_id == 'target':
            role = 'target'
        else:
            role = 'candidate'

        ele = {'data': {
            "id": gene.gene_id,
            "label": gene.label,
            "TAIR_link": "https://www.arabidopsis.org/servlets/TairObject?type=locus&name=" + gene.gene_id,
            "loc": gene.loc_id,
            "role": role,
            "tf": is_tf(gene.gene_id),
            "gene_prob": round(get_gene_prob(gene.gene_id), 4)
            }
            }
        network_json["elements"].append(ele)         

    # for gene in genes:
    #     if gene in list(network.gene_id1):
    #         loc = network[network.gene_id1 == gene]['loc_id1'].values[0]
    #         label = network[network.gene_id1 == gene]['gene_label1'].values[0]
    #     else:
    #         loc = network[network.gene_id2 == gene]['loc_id2'].values[0]
    #         label = network[network.gene_id2 == gene]['gene_label2'].values[0]
    #     if loc == 'target':
    #         role = 'target'
    #     else:
    #         role = 'candidate'
    #     ele = {'data': {
    #         "id": gene,
    #         "label": label,
    #         "TAIR_link": "https://www.arabidopsis.org/servlets/TairObject?type=locus&name=" + "AT2G45660",
    #         "loc": loc,
    #         "role": role,
    #         "tf": is_tf(gene)
    #         }
    #         }
    #     network_json["elements"].append(ele) 
    # for i in range(100):
    #     ele = {'data': {
    #         "id": "gene" + str(i),
    #         "loc": "loc",
    #         "role": "candidate"
    #         }
    #         }
    #     network_json["elements"].append(ele) 
    for _, row in network.iterrows():
        if pd.isna(row.gene_id2): # skip adding edges if one of the gene is a None value
            continue
        else:
            ele = {'data': {
                "id": row.gene_id1 + "-" + row.gene_id2 + "-" + row.evidence + "-" + row.database,
                "source": row.gene_id1,
                "target": row.gene_id2,
                "interaction_evidence" : row.evidence,
                "score": row.score,
                "database": row.database
                }}
            network_json["elements"].append(ele)
    if not network_json["elements"] and not target_gene: # return the target gene node if it is provided
        ele = [{'data': {'label': get_gene_name(target_gene), 'id': target_gene,'loc': 'target', 'role': 'target'}}]
        network_json["elements"].append(ele)
    return network_json

def gene_list_to_nodes(gene_list):
    nodes = []
    for gene in gene_list:
        ele = {'data': {
            "id": gene,
            "label": get_gene_name(gene),
            #"TAIR_link": ,
            "loc": "",
            "role": "candidate"
        }}
        nodes.append(ele)
    return nodes




def get_candidate_target_interaction_alternative(target_gene, chro, begin, end):
    '''
    Return candidate genes for an eQTL including their interaction with the
    target gene
    Make sure the SPARQL endpoint is defined correctly,
    e.g. http://localhost:1234/sparql/.

    Parameters
    ----------
    target_gene : str
        The gene associated with the eQTL.
    chro : int
        The chromosome of the eQTL.
    begin : int
        The start location of the eQTL interval.
    end : int
        The end location of the eQTL interval.

    Returns
    -------
    res : pandas dataframe
        A table containing a list of candidate genes for an eQTL including 
        their evidence.
    '''
    df = None
    if begin >= end:
        df = pd.DataFrame(columns=['candidate_label', 'candidate_id', 'evidence', 'score', 'candidate_go', 'target_go',
                'database', 'link', 'interaction'])
        return(df)

    interactions = ["resource:SIO_000701","^resource:SIO_000701","resource:SIO_001154"]
    for interaction in interactions:
        sparql = SPARQLWrapper(ENDPOINT)
        sparql.setQuery(f"""
        prefix obo: <http://purl.obolibrary.org/obo/>
        prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        prefix term: <http://rdf.ebi.ac.uk/terms/ensembl/>
        prefix dc: <http://purl.org/dc/elements/1.1/>
        prefix faldo: <http://biohackathon.org/resource/faldo#>
        prefix resource: <http://semanticscience.org/resource/>
        prefix core: <http://purl.uniprot.org/core/>
        prefix so: <http://purl.obolibrary.org/obo/so#>
        prefix ensembl: <http://rdf.ebi.ac.uk/resource/ensembl/>
        prefix TAIR10: <http://rdf.ebi.ac.uk/resource/ensembl/102/arabidopsis_thaliana/TAIR10/>
        prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
        prefix obo2: <http://www.geneontology.org/formats/oboInOwl#>
        
        SELECT DISTINCT
        STR(?regulator_label) AS ?candidate_label
        STR(?regulator_id) AS ?candidate_id
        STR(?interaction_label) AS ?evidence
        STR(?evidence_score) AS ?score
        STR(?gene_go_id) AS ?candidate_go
        STR(?target_go_id) AS ?target_go
        STR(?db_source) AS ?database
        ?link
        ?interaction
        
        WHERE{{
            # Determine the target gene
            VALUES ?target_gene {{ ensembl:{target_gene} }} .
        
            # determine the QTL interval
            VALUES ?ref {{ TAIR10:{chro} }}
            FILTER (?begin_pos >= {begin} && ?end_pos <= {end} ) .
        
            # get the list of candidates in the QTL interval
            ?gene a obo:SO_0001217 .
            ?gene faldo:location ?gene_loc .
            ?gene_loc faldo:begin ?begin ;
            faldo:end ?end ;
            faldo:reference ?ref .
            ?begin faldo:position ?begin_pos .
            ?end faldo:position ?end_pos .
        
            {{ SELECT * WHERE {{
            # query the candidate that interact with the target gene
            ?gene {interaction} ?target_gene ;
            obo:RO_0000056 ?interaction .
            ?target_gene obo:RO_0000056 ?interaction .
        
            ?interaction a ?link.
            OPTIONAL {{ ?interaction resource:SIO_000300 ?evidence_score . }}
            ?link rdfs:label ?interaction_label .
            ?interaction resource:SIO_000253 ?db_source.
            }} }}
            
            UNION
            
            {{ SELECT *
                FROM <https://www.genome.jp/kegg/>
                FROM <https://plantcyc.org/>
                WHERE {{
            # Get gene with similar pathway annotation
            ?target_gene resource:SIO_000255 ?link .
            ?gene        resource:SIO_000255 ?link .
        
            ?link rdfs:label ?interaction_label .
                ?link resource:SIO_000253 ?db_source.
            }} }}
            
                UNION
        
            {{ SELECT *
                FROM <http://purl.obolibrary.org/obo/go.owl>
                FROM <http://geneontology.org/>
                WHERE {{
            # Get gene with similar pathway annotation
            ?target_gene resource:SIO_000255 ?link .
                ?link        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .
            ?gene        resource:SIO_000255 ?gene_go        .
                ?gene_go        oboInOwl:hasOBONamespace "biological_process"^^xsd:string .

            # ?link (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?gene_go.
            #     ?link rdfs:label ?interaction_label .
            #     ?link resource:SIO_000253 ?db_source.
            ?gene_go (rdfs:subClassOf|^rdfs:subClassOf){{,2}} ?link.
                ?gene_go rdfs:label ?interaction_label .
                ?gene_go resource:SIO_000253 ?db_source.

            ?gene_go obo2:id ?gene_go_id .
            ?link    obo2:id ?target_go_id .
            }} }}
        
        # get the gene label
        OPTIONAL {{ ?gene rdfs:label ?regulator_label . }}
        ?gene dc:identifier ?regulator_id .
        }}
        """)

        new_df = sparql2csv(sparql)
        gene_label = get_gene_name(target_gene)
        new_df['target_label'] = gene_label
        new_df['target_id'] = target_gene
        new_df['score'] = new_df['score'].replace(r'^\s*$', np.nan, regex=True)
        new_df['score'] = new_df.score.astype(float)
        for index, row in new_df.iterrows():
            if not row.candidate_label:
                new_df.loc[index, ('candidate_label')] = row.candidate_id
            if row.database == 'Gene Ontology':
                new_df.loc[index, ("score")] = calculate_go_similarity(row.candidate_go, row.target_go, GODAG, TERMCOUNTS)
        
        if df is None:
            df = new_df
        else:
            df = pd.concat([df,new_df])

    # Get candidate genes without interaction to the target genes
    # interacting_candidates = df.candidate_label.values.tolist()
    # all_candidates = get_genes_in_interval(chro, begin, end)
    # non_interacting_candidates = list(set(all_candidates) - set(interacting_candidates))
    # for gene in non_interacting_candidates:
    #     df = df.append({'candidate_label': gene}, ignore_index=True)
    return(df)
