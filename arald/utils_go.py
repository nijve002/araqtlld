# -*- coding: utf-8 -*-
"""
Created on Fri May 21 11:28:07 2021

Functions and modules for GO semantic similarity calculations.

@author: harta005
"""

### For GO semantic similarity calculations
import pandas as pd
import requests
import goatools
from goatools.obo_parser import GODag
from goatools.semantic import resnik_sim
from goatools.semantic import lin_sim
import re
from itertools import product
import sys

## For GO semantic similarity calculation
def get_bp_goid():
    '''
    Return a list of GO term under Biological Process domain.
    
    Returns
    -------
    res : pandas dataframe
        A table containing a list of Biological Process GO term.
    '''
    sparql = SPARQLWrapper('http://localhost:1234/sparql/')
    sparql.setQuery("""    
    prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
    prefix obo2: <http://www.geneontology.org/formats/oboInOwl#>
    
    SELECT DISTINCT
    STR(?go_id) AS ?ID
    
    WHERE{{
    ?go oboInOwl:hasOBONamespace "biological_process"^^<http://www.w3.org/2001/XMLSchema#string> .
    ?go obo2:id ?go_id .
    }}           
    """)
    res = sparql2csv(sparql)
    return res

def gaf2dct():
    """
    Return a dict where the keys are genes and values are BP GO term associated with them.
    
    Returns
    -------
    res : dict
        A dict where the keys are genes and values are BP GO term associated with them.
    """
    url = 'http://current.geneontology.org/annotations/tair.gaf.gz'
    df = pd.read_csv(url, compression='gzip', sep='\t', skiprows=33, low_memory=False,
                     names=['db', 'db_object_id', 'db_object_symbol', 'qualifier',
                            'go_id', 'db_reference', 'evidence_code', 'with',
                            'aspect', 'db_object_name', 'db_object_synonym',
                            'db_object_type', 'taxon', 'date', 'assigned_by',
                            'annotation_extension', 'gene_product_id'])
    df = df[['go_id', 'db_object_synonym']] # filter for GO under Biological Process domain
    gaf_dct = {}
    for index, row in df.iterrows():
        regex = re.search(r'AT\dG\d{5}', row.db_object_synonym)
        go_id = row.go_id
        if regex:
            try:
                gaf_dct[regex.group()].append(go_id)
            except KeyError:
                gaf_dct[regex.group()] = [go_id]
    return gaf_dct

def calculate_go_similarity(go1, go2, godag, termcounts, method='resnik'):
    '''
    Return GO semantic similarity between two GO terms.

    
    Parameters
    ----------
    go1 : str
        A GO term
    go2 : int
        Another GO term
    godag : goatools.obo_parser.GODag object
        A GO directed acyclic graph
    termcounts : goatools.semantic.TermCounts object
        A count of GO terms
    method : str
        The semantic similarity method ('resnik' or 'lin')

    Returns
    -------
    sim : int
        The GO semantic similarity score.
    '''
    if method == 'resnik':
        try:
            sim = goatools.semantic.resnik_sim(go1, go2, godag, termcounts)
        except KeyError:
            sim = 0
    elif method == 'lin':
        sim = goatools.semantic.lin_sim(go1, go2, godag, termcounts)
    else:
        sim = 'method is not recognized'
    return sim

def calculate_gene_go_similarity(gene1, gene2, gaf_dict, godag, termcounts, method):
    '''
    Return the semantic similarity of two genes. The calculation is based on the maximum GO semantic similarity
    between GO terms of the two genes.
    
    Parameters
    ----------
    gene1 : str
        A gene
    gene2 : int
        Another gene
    godag : goatools.obo_parser.GODag object
        A GO directed acyclic graph
    termcounts : goatools.semantic.TermCounts object
        A count of GO terms
    method : str
        The semantic similarity method ('resnik' or 'lin')

    Returns
    -------
    sim : int
        The gene semantic similarity score.
    '''
    if all([gene1 in gaf_dict, gene2 in gaf_dict]):
        go1 = list(dict.fromkeys(gaf_dict[gene1]))
        go2 = list(dict.fromkeys(gaf_dict[gene2]))
        go_cart_prod = list(dict.fromkeys(list(product(go1, go2))))
        sims = list(map(lambda ele: calculate_go_similarity(ele[0], ele[1], godag, termcounts, method), go_cart_prod))
        sims = [0 if sim is None else sim for sim in sims]
        score = max(sims) 
    else:
        score = 0
    return round(score, 2)


#url = 'http://purl.obolibrary.org/obo/go.obo'
#r = requests.get(url, allow_redirects=True)
#open('go.obo', 'wb').write(r.content)
GODAG = goatools.obo_parser.GODag("/code/go.obo",prt=None) # GO graph used to determine similarity among GO terms
GAF_DICT = gaf2dct() # Dictionary of gene-GO terms
TERMCOUNTS = goatools.semantic.TermCounts(GODAG, GAF_DICT) # The count of each GO term in Arabidopsis genome
