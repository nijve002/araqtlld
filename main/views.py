from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings

from main.models import Experiment, ArraySpot

import csv

def index(request):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    mode = 'landing'

    if request.method == 'GET':
        if request.GET.get('mode'):
            mode = request.GET.get('mode')

    return render(request, 'index.html',{'experiments':sorted(experiments,
                                                            key=lambda x: x.pub_date, reverse=True),
                                            'mode' :mode})

def no_results(query, mode):
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)

    return render(None, 'index.html', {'experiments': sorted(experiments,
                                                            key=lambda x: x.pub_date, reverse=True),
                                             'query': query,
                                             'noresults': 1,
                                             'mode': mode})


def AGI_list(request):
    genelist = ArraySpot.objects.filter(experiment__species__species_name=settings.SPECIES).values_list('spot_id', flat=True)
    AGI_list = '\n'.join(genelist)

    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="ARAQTL_AGI_list.txt"'
    response.write(AGI_list)

    return response


