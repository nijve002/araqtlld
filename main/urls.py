from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^AGI_list$', views.AGI_list, name='AGI_list'),
]
