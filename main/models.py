# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Species(models.Model):
    species_name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=45)
    url = models.CharField(max_length=255)

class Chromosome(models.Model):
    name = models.CharField(max_length=50)
    start = models.IntegerField()
    end = models.IntegerField()
    species = models.ForeignKey(Species, on_delete=models.DO_NOTHING)

class Experiment(models.Model):
    species = models.ForeignKey(Species, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=50)
    phenotypes = models.CharField(max_length=50)
    type_of_array = models.CharField(max_length=50)
    sample_size = models.CharField(max_length=50)
    parental_strain = models.CharField(max_length=50)
    plant_parts = models.CharField(max_length=100)
    reference = models.CharField(max_length=200)
    pubmed = models.CharField(max_length=20)
    pub_date = models.DateTimeField()
    upload_user_id = models.IntegerField()
    array_file = models.CharField(max_length=100)
    marker_file = models.CharField(max_length=100)
    genotype_file = models.CharField(max_length=100)
    lod_file = models.CharField(max_length=100)
    pvalthld = models.DecimalField(max_digits=13, decimal_places=12)
    lodthld = models.DecimalField(max_digits=6, decimal_places=3)

class Line(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.DO_NOTHING)
    line_name = models.CharField(max_length=50)

class Marker(models.Model):
    name = models.CharField(max_length=15)
    chromosome = models.ForeignKey(Chromosome, on_delete=models.DO_NOTHING,null=True)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)
    experiment = models.ForeignKey(Experiment, on_delete=models.DO_NOTHING)

class GeneInfo(models.Model):
    species = models.ForeignKey(Species, on_delete=models.DO_NOTHING) 
    gene_id = models.CharField(max_length=30, blank=True, null=True, unique=True)
    description = models.TextField(blank=True,null=True)
    gene_name = models.CharField(max_length=30, blank=True, null=True, unique=False)
    chr = models.CharField(max_length=3, null=True)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)
    is_tf = models.BooleanField(default=False)
    gene_prob = models.DecimalField(max_digits=13, decimal_places=12, default=0)
        
class GeneAlias(models.Model):
    geneinfo = models.ForeignKey(GeneInfo, on_delete=models.DO_NOTHING)
    alias = models.CharField(max_length=200, blank=False, null=False, unique=False)

class Transcript(models.Model): 
    geneinfo = models.ForeignKey(GeneInfo, on_delete=models.DO_NOTHING, null=True)
    transcript_id = models.CharField(max_length=20, blank=True, null=True)
    chr = models.CharField(max_length=3)
    start = models.IntegerField(blank=True, null=True)
    end = models.IntegerField(blank=True, null=True)

class GO(models.Model):
    accession = models.CharField(primary_key=True,max_length=30,blank=False,null=False, unique=True)
    name = models.CharField(max_length=255,blank=False,null=False)
    definition = models.TextField()
    domain = models.CharField(max_length=255,blank=False,null=False)

class GeneGO(models.Model):
    geneinfo = models.ForeignKey(GeneInfo, on_delete=models.DO_NOTHING)
    term_accession = models.ForeignKey(GO,to_field='accession', db_column = 'term_accession', on_delete=models.DO_NOTHING)
    term_evidence_code = models.CharField(max_length=30,blank=False,null=True)

class ArraySpot(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.DO_NOTHING)
    geneinfo = models.ForeignKey(GeneInfo, on_delete=models.DO_NOTHING,null=True)
    spot_id = models.CharField(max_length=50)
    max_lod_score = models.FloatField(null=True)

class Metabolite(models.Model):
    name = models.CharField(max_length=255,blank=False,null=False)
    description = models.TextField(blank=True,null=True)

class Phenotype(models.Model):
    name = models.CharField(max_length=255,blank=False,null=False)
    description = models.TextField(blank=True,null=True)

class Phenotype_Experiment(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.DO_NOTHING)
    phenotype = models.ForeignKey(Phenotype, on_delete=models.DO_NOTHING)

class Metabolite_Experiment(models.Model):
    experiment = models.ForeignKey(Experiment, on_delete=models.DO_NOTHING)
    metabolite = models.ForeignKey(Metabolite, on_delete=models.DO_NOTHING)
