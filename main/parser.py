'''
Created on Apr 28, 2015

@author: yaya
'''
import json
import os

def outputJSON(lod_dic,path):
    '''
    description: output LOD profile of each probe individually  
    
    @type lod_dic: dictionary
    @param lod_dic: QTL mapping output: LOD profile dictionary
    
    @type path: string
    @param path: output path
    
    '''
    for ele in lod_dic:
        outputDic = {}
        outputDic['probe'] = ele
        outputDic['lod'] = lod_dic[ele]
        output_txt_name = '%s.json' % ele
        out_path  = os.path.join(path,output_txt_name)
        with open(out_path,'w') as fo:
            json.dump(outputDic,fo,indent=4)
