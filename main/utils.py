import json
import os

import sys

import datetime

from main.models import *
from math import factorial
from django.conf import settings

from scipy import stats
from statsmodels.sandbox.stats.multicomp import multipletests

import numpy as np
import pandas as pd

from sqlalchemy import create_engine

import sqlite3 as sql

import shutil


def GO_enrichment(gene_set, experiment_name, alpha=0.05): 
    gene_set_count = len(gene_set)

    if gene_set_count < 10:
        return {}
    
    gene_go = list(ArraySpot.objects\
    .filter(experiment__name = experiment_name)\
    .filter(max_lod_score__gte = 1)\
    .values_list("geneinfo__gene_id","geneinfo__genego__term_accession"))
        
    gene_GO = {}
    GO_gene = {}

    for gene, go_term in gene_go:
        if gene not in gene_GO: 
            gene_GO[gene] = [] 
        if go_term not in GO_gene: 
            GO_gene[go_term] = []
        gene_GO[gene].append(go_term) 
        GO_gene[go_term].append(gene)
        
    gene_count = len(gene_GO)
        
    GO_gene_set = {}
    for gene in gene_set:
        if gene not in gene_GO:
            continue
        for go_term in gene_GO[gene]:
            if go_term not in GO_gene_set:
                GO_gene_set[go_term] = []
            GO_gene_set[go_term].append(gene)

    # only consider GO terms that occur at least in 0.1% of the genes and at most in 25% of the genes
    min_count = 0.001 * gene_count
    max_count = 0.25 * gene_count

    go_term_pvalues = {}

    for go_term in GO_gene_set:
        overall_go_count = len(GO_gene[go_term])
        set_go_count = len(GO_gene_set[go_term])
        
        if overall_go_count < min_count or overall_go_count > max_count:
            continue
        
        a = set_go_count
        b = overall_go_count - a
        c = gene_set_count - a
        d = gene_count - a - b - c
        oddsratio, pvalue = stats.fisher_exact([[a, b], [c, d]])
        go_term_pvalues[go_term] = pvalue
        
    go_terms = go_term_pvalues.keys()
    pvalues = list(go_term_pvalues.values())

    if len(pvalues) == 0:
        return {}
        
    (reject,qvalues,alphacSidak,alphacBonf) = multipletests(pvalues, 0.01, method='fdr_bh')
    corrected_pvalues = dict(zip(go_terms,qvalues))

    filtered_go_terms = {go_term:qvalue for (go_term,qvalue) in corrected_pvalues.items() if qvalue < alpha }

    result = {}
    go_objects = GO.objects.filter(accession__in=filtered_go_terms.keys())
    for go in go_objects:
        result[go.accession] = [go,filtered_go_terms[go.accession]]
    return result
        
                       
def load_traitlist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    traits_pickle_path = os.path.join(experiment_data_path, '%s' % "traits.npy")
    if not os.path.exists(traits_pickle_path):
        create_pickles_for_experiment(experiment_name)
    traits = np.load(traits_pickle_path,allow_pickle=True)
    return traits


def load_markerlist_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    markers_pickle_path = os.path.join(experiment_data_path, '%s' % "markers.npy")
    if not os.path.exists(markers_pickle_path):
        create_pickles_for_experiment(experiment_name)
    markers = np.load(markers_pickle_path,allow_pickle=True)
    return markers


def get_lodscores_for_experiment_np(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod.npy")
    if not os.path.exists(lod_pickle_path):
        create_pickles_for_experiment(experiment_name)
    lod_scores = np.load(lod_pickle_path,allow_pickle=True)
    return lod_scores

def get_lodscores_for_experiment_pd(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_pickle_path = os.path.join(experiment_data_path, '%s' % "lod_df.npy")
    if not os.path.exists(lod_pickle_path):
        create_pickles_for_experiment(experiment_name)
    lod_scores = pd.read_pickle(lod_pickle_path)
    return lod_scores

def get_lodscores_for_trait_experiment(experiment_name,trait):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    db_path = os.path.join(experiment_data_path, "data.db")

    with sql.connect(db_path) as conn:
        lod_scores = pd.read_sql(f'SELECT * FROM lod WHERE trait="{trait}"',conn)

    lod_scores.index = ['lod']

    return lod_scores

def create_pickles_for_experiment(experiment_name):
    experiment_data_path = os.path.join(settings.MEDIA_ROOT, 'data/%s' % experiment_name)
    lod_df_pickle_path = os.path.join(experiment_data_path, 'lod_df.npy')
    lod_pickle_path = os.path.join(experiment_data_path, "lod.npy")
    traits_pickle_path = os.path.join(experiment_data_path, "traits.npy")
    markers_pickle_path = os.path.join(experiment_data_path, "markers.npy")
    
    lod_file = Experiment.objects.get(name=experiment_name).lod_file
    lod_file_path = os.path.join(experiment_data_path, '%s' % lod_file)
    lod_data = pd.read_csv(lod_file_path, sep='\t',index_col=0)
    lod_data.index = lod_data.index.str.upper()
    lod_data.to_pickle(lod_df_pickle_path)
   
    if Metabolite.objects.filter(metabolite_experiment__experiment__name = experiment_name).count() > 0:
        metabolite_lod_file_path = os.path.join(experiment_data_path, '%s' % "lod_metabolites.txt")
        lod_data = lod_data.append(pd.read_csv(metabolite_lod_file_path, sep='\t',index_col=0))
    
    if Phenotype.objects.filter(phenotype_experiment__experiment__name = experiment_name).count() > 0:
        phenotype_lod_file_path = os.path.join(experiment_data_path, '%s' % "lod_phenotypes.txt")
        lod_data = lod_data.append(pd.read_csv(phenotype_lod_file_path, sep='\t',index_col=0))
    
    #lod_data.drop(None, errors='ignore', inplace=True) # remove rows with no gene name
    lod_data.index = map(str.upper, lod_data.index)

    markers = lod_data.columns.values
    traits = lod_data.index.values
    data = np.asarray(lod_data).astype(np.float)

    db_path = os.path.join(experiment_data_path, 'data.db')

    with sql.connect(db_path) as conn:
        lod_data.to_sql('lod',conn,if_exists='replace',index_label='trait')
        conn.execute("CREATE INDEX trait on lod(trait)")

    np.save(markers_pickle_path, markers)
    np.save(lod_pickle_path, data)
    np.save(traits_pickle_path, traits)


def update_max_lod_in_database():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):

        traits = load_traitlist_for_experiment(experiment_name)
        lod_scores = get_lodscores_for_experiment_np(experiment_name)
        max_lod_trait = np.amax(np.fabs(lod_scores),axis=1)
        gene2max =dict(zip(traits,max_lod_trait))

        genes_for_experiment = ArraySpot.objects.filter(experiment__name=experiment_name)
        for gene in genes_for_experiment:
            if gene.spot_id in gene2max:
                gene.max_lod_score = gene2max[gene.spot_id]
            else:
                gene.max_lod_score = 0
            gene.save()


def create_pickles():
    for experiment_name in Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name",flat=True):
        create_pickles_for_experiment(experiment_name)


def get_markers_on_chr(experiment_name, chromosome):
    return Marker.objects.filter(experiment__name = experiment_name, chromosome__name = chromosome)


def get_closest_marker(experiment_name, chromosome, pos):
    pos = int(pos)
    markers = get_markers_on_chr(experiment_name, chromosome)
    minDist = -1
    closestMarker = None
    for marker in markers:
        dist = abs(pos - marker.start)
        if minDist == -1 or dist < minDist:
            minDist = dist
            closestMarker = marker

    return closestMarker

def get_cis_LOD_genes(experiment_name, gene_list):
    lod_scores = get_lodscores_for_experiment_pd(experiment_name)
    markers = {marker[0]: marker[1:] for marker in \
        Marker.objects.filter(experiment__name = experiment_name)\
        .values_list('chromosome','name','start')}

    genes = list(GeneInfo.objects.filter(gene_id__in = gene_list)\
        .values_list('gene_id','chr','start'))

    cis_LOD = dict()

    for gene in genes:
        gene_id = gene[0]
        gene_chr = gene[1]
        gene_pos = gene[2]

        closest_marker = min(markers[gene_chr],key=lambda x:abs(gene_pos - x[1]))[0]
        cis_LOD[gene_id] = lod_scores[closest_marker][gene_id]

    return cis_LOD

def get_cis_LOD_region(experiment_name,chromosome, start, end):
    lod_scores = get_lodscores_for_experiment_pd(experiment_name)
    markers = list(Marker.objects.filter(experiment__name = experiment_name, \
        chromosome = chromosome, start__range=(start, end)).values_list('name','start'))

    genes = list(GeneInfo.objects.filter(arrayspot__experiment__name = experiment_name, \
        chr = chromosome, start__range=(start, end)).values_list('gene_id','start'))

    cis_LOD = dict()

    if not markers:
        return cis_LOD

    for gene in genes:
        gene_id = gene[0]
        gene_pos = gene[1]

        closest_marker = min(markers,key=lambda x:abs(gene_pos - x[1]))[0]
        cis_LOD[gene_id] = lod_scores[closest_marker][gene_id]

    return cis_LOD


def get_cis_LOD(experiment_name):
    lod_scores = get_lodscores_for_experiment_pd(experiment_name) 

    genes = lod_scores.index.values
    
    cis_LOD = dict()
    
    gene_list = list(GeneInfo.objects.values_list('gene_id','chr','start'))
    gene_list = [(i,int(c),s) for (i,c,s) in gene_list if c and c.isdigit()]
    gene_list = sorted(gene_list, key = lambda x: (x[1], x[2]))
    
    marker_list = list(Marker.objects.filter(experiment__name = experiment_name).\
        values_list('name','chromosome','start'))
    marker_list = sorted(marker_list, key = lambda x: (x[1], x[2]))
    
    i_gene = 0
    i_marker = 0
    
    i_previous_marker = None
    
    while i_gene < len(gene_list) and i_marker < len(marker_list):
        if gene_list[i_gene][0] not in genes:
            i_gene += 1
            continue
        if gene_list[i_gene][1] < marker_list[i_marker][1]:
            i_gene += 1
            i_previous_marker = None
            continue
        if gene_list[i_gene][1] > marker_list[i_marker][1]:
            i_marker += 1
            i_previous_marker = None
            continue
        
        gene_id = gene_list[i_gene][0]
        gene_pos = gene_list[i_gene][2]
        
        marker_id = marker_list[i_marker][0]
        marker_pos = marker_list[i_marker][2]
        
        if gene_pos > marker_pos:
            if i_previous_marker == i_marker:
                cis_LOD[gene_id] = lod_scores.loc[gene_id,marker_id]
                gene_id += 1
            else:
                i_previous_marker = i_marker
                i_marker += 1
            continue
        
        if gene_pos == marker_pos:
            cis_LOD[gene_id] = lod_scores.loc[gene_id,marker_id]
            i_gene += 1
            continue
            
        if not i_previous_marker:
            cis_LOD[gene_id] = lod_scores.loc[gene_id,marker_id]
            i_gene += 1
            continue
        
        previous_marker_id = marker_list[i_previous_marker][0]    
        previous_marker_pos = marker_list[i_previous_marker][2]
        
        if abs(gene_pos - previous_marker_pos) < abs(gene_pos - marker_pos):
            cis_LOD[gene_id] = lod_scores.loc[gene_id,previous_marker_id]
        else: 
            cis_LOD[gene_id] = lod_scores.loc[gene_id,marker_id]
        i_gene += 1
        
    return cis_LOD


def get_neighboring_markers(experiment_name, marker_name):
    marker = Marker.objects.get(experiment__name = experiment_name, 
        name = marker_name)
    
    markers = Marker.objects.filter(experiment__name = experiment_name, 
        chromosome = marker.chromosome)
    
    before = None
    after = None
    for m in markers:
        if m.start < marker.start:
            if not before or before.start < m.start:
                before = m
        elif m.start > marker.start:
            if not after or after.start > m.start:
                after = m
    return (before,after)
                

def get_phenotype(query, experiment_name = None):
    phenotypes = Phenotype.objects.filter(name__iexact=query)
    if experiment_name:
        phenotypes = phenotypes.filter(phenotype_experiment__experiment__name = experiment_name)

        if phenotypes.count() == 1:
            return phenotypes[0]
        else:
            return None

    if phenotypes.count() == 1:
        return phenotypes[0]
    return None


def get_metabolite(query, experiment_name = None):
    metabolites = Metabolite.objects.filter(name__iexact=query)
    if experiment_name:
        metabolites = metabolites.filter(metabolite_experiment__experiment__name = experiment_name)

        if metabolites.count() == 1:
            return metabolites[0]
        else:
            return None

    if metabolites.count() == 1:
        return metabolites[0]
    return None


def get_gene(query, experiment_name = None):
    genes = GeneInfo.objects.filter(gene_id__iexact = query)
    if experiment_name:
        genes = genes.filter(arrayspot__experiment__name = experiment_name)

    if genes.count() == 1:
        return genes[0]

    genes = GeneInfo.objects.filter(gene_name__iexact=query)
    if experiment_name:
        genes = genes.filter(arrayspot__experiment__name = experiment_name)

    if genes.count() == 1:
        return genes[0]

    return None


def get_marker_objects(experiment_name):
    return Marker.objects.filter(experiment__name = experiment_name)


def get_species_for_experiment(experiment_name):
    return Species.objects.get(experiment__name = experiment_name).species_name


def get_marker_names(experiment_name):
    return Marker.objects.filter(experiment__name = experiment_name).values_list("name", flat=True)


def create_settings_json_for_experiment(experiment_name):
    output_dic = dict()

    species_name = get_species_for_experiment(experiment_name)
    output_dic['species'] = species_name

    url = Species.objects.filter(species_name=species_name).values_list('url', flat=True)
    if len(url) == 1:
        output_dic['url'] = url[0]
    else:
        output_dic['url'] = ""

    # only report chromosomes for with markers are available
    chromosomes = Chromosome.objects.filter(species__species_name=species_name).distinct()
    output_dic['chrnames'] = sorted(chromosomes.values_list('name', flat=True))

    chr_dic = dict()
    for chromosome in chromosomes:
        chr_dic[chromosome.name] = {"start": chromosome.start, "end": chromosome.end}
	
    output_dic["chr"] = chr_dic

    out_file = os.path.join(settings.MEDIA_ROOT,'data/%s/%s.json' % (experiment_name,"settings"))

    with open(out_file, 'w') as fo:
        json.dump(output_dic, fo, indent=4, ensure_ascii=True)

def safe_filename(name):
    keepcharacters = (' ','.','_')
    return "".join(c for c in name if c.isalnum() or c in keepcharacters).rstrip()


def check_for_new_experiment():
    data_path = os.path.join(settings.MEDIA_ROOT, 'data/')
    experiment_dirs = os.listdir(data_path)
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES).values_list("name", flat=True)
    for experiment_dir in experiment_dirs:
        if experiment_dir[0] == ".":
            continue
        if experiment_dir not in experiments:
            create_experiment(experiment_dir)
                                         

def load_marker_file_in_database(experiment_name):
    Marker.objects.filter(experiment__name=experiment_name).delete()
    experiment = Experiment.objects.get(name=experiment_name)
    marker_file_name = experiment.marker_file
    marker_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name,marker_file_name))
    marker_df = pd.read_csv(marker_file_path, sep='\t')

    # just to be sure
    marker_df.columns = marker_df.columns.str.lower()

    for index, row in marker_df.iterrows():
        marker = Marker()
        marker.name = row['marker']
        marker.start = float(row['start'])
        marker.end = float(row['start'])
        chromosome = Chromosome.objects.get(name=row['chr'],species__species_name=settings.SPECIES)
        marker.chromosome = chromosome
        marker.experiment = experiment
        try:
            marker.save()
        except ValueError as e:
            sys.stderr.write("cannot import marker line: %s (%s)")%(row,e.strerror)

def add_genes_for_experiment(experiment_name):
    # should be rewritten to use pandas/pickl
    ArraySpot.objects.filter(experiment__name = experiment_name).delete()
    experiment = Experiment.objects.get(name=experiment_name)
    species = Species.objects.get(species_name=settings.SPECIES)

    lod_file_name = experiment.lod_file
    lod_file_path = os.path.join(settings.MEDIA_ROOT, 'data/%s/%s' % (experiment_name, lod_file_name))
    
    with open(lod_file_path) as lod_file:
        header = lod_file.readline()
        for line in lod_file:
            fields = line.rstrip().split()
            gene = fields[0].upper()

            gis = GeneInfo.objects.filter(gene_id=gene)
            if len(gis) == 0:
                gi = GeneInfo()
                gi.gene_id = gene
                gi.species = species
                gi.save()

            gene_info = GeneInfo.objects.get(gene_id=gene)

            arrayspots = ArraySpot.objects.filter(experiment__name = experiment_name).filter(spot_id = gene)
            if len(arrayspots) == 0:
                arrayspot = ArraySpot()
                arrayspot.experiment = experiment
                arrayspot.spot_id = gene
                arrayspot.geneinfo = gene_info
                arrayspot.save()
                                
def create_experiment(experiment_name):
    experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (experiment_name))
    if not os.path.exists(experiment_dir):
        sys.stderr.write("Cannot create experiment, directory does not exist: %s\n" % experiment_dir)
        return

    species = Species.objects.get(species_name=settings.SPECIES)
    Experiment.objects.filter(name=experiment_name).delete()
    experiment = Experiment()
    experiment.name = experiment_name
    experiment.species = species
    experiment.marker_file = "marker.txt"
    experiment.array_file = "array.txt"
    experiment.lod_file = "lod.txt"
    experiment.pub_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    experiment.upload_user_id = 1
    experiment.pvalthld = 0.01
    experiment.lodthld = 3
    experiment.save()

    load_marker_file_in_database(experiment_name)
    add_genes_for_experiment(experiment_name)
    create_settings_json_for_experiment(experiment_name)
    create_pickles_for_experiment(experiment_name)

def rename_experiment(old_name,new_name):
    old_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (old_name))
    if not os.path.exists(old_experiment_dir):
        sys.stderr.write("Cannot rename experiment, directory does not exist: %s\n"%old_experiment_dir)
        return

    new_experiment_dir = os.path.join(settings.MEDIA_ROOT, 'data/%s/' % (new_name))
    if os.path.exists(new_experiment_dir):
        sys.stderr.write("Cannot rename experiment, new directory already exists: %s\n"%new_experiment_dir)
        return

    shutil.move(old_experiment_dir,new_experiment_dir)

    experiment = Experiment.objects.get(name=old_name)
    experiment.name = new_name
    experiment.save()

def delete_experiment(experiment_name):
    ArraySpot.objects.filter(experiment__name = experiment_name).delete()
    Marker.objects.filter(experiment__name=experiment_name).delete()
    Phenotype_Experiment.objects.filter(experiment__name = experiment_name).delete()
    Metabolite_Experiment.objects.filter(experiment__name = experiment_name).delete()
    Experiment.objects.filter(name = experiment_name).delete()


def is_tf(gene_id):
    try:
        gi = GeneInfo.objects.get(gene_id=gene_id)
        return gi.is_tf
    except  GeneInfo.DoesNotExist:
        return False

def get_gene_name(gene_id):
    try:
        gi = GeneInfo.objects.get(gene_id=gene_id)
        return gi.gene_name
    except GeneInfo.DoesNotExist:
        return ""

def get_gene_prob(gene_id):
    try:
        gi = GeneInfo.objects.get(gene_id=gene_id)
        return gi.gene_prob
    except GeneInfo.DoesNotExist:
        return 0

def add_gene_prob(gene_prob_file):
    gp = pd.read_csv(gene_prob_file)
    for gi in GeneInfo.objects.all():
        if sum(gi.gene_id == gp.ID):
            gi.gene_prob = float(gp[gi.gene_id == gp.ID].prob)