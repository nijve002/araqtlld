from django.shortcuts import render, HttpResponse, redirect
from django.conf import settings
from django.db.models import Q

from main.models import Experiment, GeneInfo, ArraySpot, GeneGO, GO, Species, Metabolite, Phenotype

import suggestions
import main

import urllib
import re

from urllib.parse import unquote

class trait:
     trait_id = ""
     type = ""
     description = ""
     name = ""
     chr = 0
     start = 0
     max_lod_score = 0

def multiplot(request):
    '''
    select a trait and experiment

    '''
    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    if request.method == 'GET':
        if 'query' in request.GET:
            if request.GET.get('experiment_name'):
                experiment_name = request.GET.get('experiment_name')
            else:
                experiment_name = "all"

            query = request.GET.get('query')
            query = urllib.parse.unquote(query).strip()
            query = re.sub(r',', r' ', query)
            query = re.sub(r'\s+', r' ', query)

            if not query:
                return main.views.no_results(query, "multiplot")

            if experiment_name == "all":
                if query.startswith("GO"):
                    # query is a GO term
                    experiment_name = experiments[0].name
                else:
                    return multiexperimentplot(request)

            gene_list = list()
            metabolite_list = list()
            phenotype_list = list()
            title = query

            if re.match("GO:", query, re.I):
                # GO term
                gene_ids = queryGO(experiment_name, query)
                gene_list = getGeneInfoForGeneIDs(gene_ids)
                if len(gene_list) > 0:
                    title = "%s (%s)" % (GO.objects.filter(accession=query).values_list("name", flat=True)[0], query)
                else:
                    return main.views.no_results(query,"multiplot")
            elif re.match("(chr)?(.+):(\d+)[.][.](\d+)",query,re.I):
                # chromosomal location
                mo = re.match("(chr)?(.+):(\d+)[.][.](\d+)",query,re.I)
                genes = GeneInfo.objects.filter(chr=mo.group(2)).filter(start__range=(mo.group(3),mo.group(4)))
                gene_list = genes.filter(arrayspot__experiment__name = experiment_name)
            else:
                query_fields = query.split()
                for q in query_fields:
                    genes = GeneInfo.objects.filter(arrayspot__experiment__name=experiment_name)\
                        .filter(Q(arrayspot__spot_id__iexact=q)|Q(gene_name__iexact=q)|Q(genealias__alias__iexact=q))\
                        .distinct()
                    if len(genes) == 1:
                        gene_list.append(genes[0])

                    metabolites =  Metabolite.objects.filter(name__iexact=q)
                    metabolites = metabolites.filter(metabolite_experiment__experiment__name = experiment_name)
                    if len(metabolites) > 0:
                        metabolite_list.append(metabolites[0])

                    phenotypes =  Phenotype.objects.filter(name__iexact=q)
                    phenotypes = phenotypes.filter(phenotype_experiment__experiment__name = experiment_name)
                    if len(phenotypes) > 0:
                        phenotype_list.append(phenotypes[0])

                #genelist = queryGenesForExperiment(exp_name, query)
                if len(gene_list) == 0 and len(metabolite_list) == 0 and len(phenotype_list) == 0:
                    ret = suggestions.views.index(request)
                    if ret:
                        return ret
                    else:
                        return main.views.no_results(query,"multiplot")

                elif len(gene_list) == 1:
                    gene = gene_list[0]
                    if gene.gene_name.strip():
                        title = f"{gene.gene_id} ({gene.gene_name})"
                    else:
                        title = f"{gene.gene_id}"

            trait_list = list()
            gene_list = set(gene_list)
            for gene in gene_list:
                t = trait()
                t.trait_id = gene.gene_id
                t.type = "gene"
                t.description = unquote(gene.description)
                t.name = gene.gene_name
                t.chr = gene.chr
                t.start = gene.start
                #t.max_lod_score = gene.max_lod_score
                trait_list.append(t)

            for metabolite in metabolite_list:
                t = trait()
                t.trait_id = metabolite.name
                t.type = "metabolite"
                t.description = unquote(metabolite.description)
                t.name = metabolite.name
                trait_list.append(t)

            for phenotype in phenotype_list:
                t = trait()
                t.trait_id = phenotype.name
                t.type = "phenotype"
                t.description = unquote(phenotype.description)
                t.name = phenotype.name
                trait_list.append(t)

            if len(trait_list) > 1:
                title = "%s traits"%len(trait_list)

            return render(request, 'multiplot.html', {'experiment_name': experiment_name,
                                                         'experiments': experiments,
                                                         'query': query,
                                                         'species': species_short_name,
                                                         'title': title,
                                                         'trait_list': trait_list
                                                         })
        else:
            return render(request, 'multiplot.html', {'experiments': experiments})


def multiexperimentplot(request):
    '''
    plot lod scores for one trait for all experiments

    '''

    experiments = Experiment.objects.filter(species__species_name=settings.SPECIES)
    species_short_name = Species.objects.get(species_name=settings.SPECIES).short_name

    query = request.GET.get('query')
    query = urllib.parse.unquote(query).strip().upper()

    genes = ArraySpot.objects.filter(geneinfo__gene_id=query)

    if genes.count() == 0:
        gi = GeneInfo.objects.filter(genealias__alias=query)
        if gi.count() > 1:
            ret = suggestions.views.index(request)
        if gi.count() == 1:
            query = gi[0].gene_id
        if gi.count() == 0:
            gi = GeneInfo.objects.filter(gene_name__iexact=query)

            if gi.count() == 0:
                gi = GeneInfo.objects.filter(description__icontains=query)

            if gi.count() == 1:
                query = gi[0].gene_id
            else:
                ret = suggestions.views.index(request)
                if ret:
                    return ret
                else:
                    return main.views.no_results(query,"multiplot")

    experiments4trait = Experiment.objects.filter(arrayspot__spot_id=query).values_list('name', flat=True)

    geneInfoList = GeneInfo.objects.filter(gene_id=query, species__species_name=settings.SPECIES)
    if (geneInfoList.count() == 0):
        return HttpResponse('<h1> Unknown gene %s </h1>' % query)
    gene = geneInfoList[0]
    t = trait()
    t.trait_id = gene.gene_id
    t.name = gene.gene_name
    t.start = gene.start
    t.chr = gene.chr
    t.description = unquote(gene.description)
    t.type = "gene"

    return render(request, 'multiplot.html', {'trait': t,
                                                 'experiments': experiments,
                                                 'query': query,
                                                 'experiment_name': "all",
                                                 'experiments4trait': experiments4trait,
                                                 'species': species_short_name})


def getGeneInfoForGeneIDs(geneIDs):
    geneInfoList = list()
    for geneID in geneIDs:
        if len(GeneInfo.objects.filter(gene_id=geneID)) == 1:
            geneInfoList.append(GeneInfo.objects.get(gene_id=geneID))

    return geneInfoList


def queryGenesForExperiment(experiment_name, query):
    geneIDs = query.split(" ")

    return ArraySpot.objects.filter(experiment__name=experiment_name).filter(spot_id__in=geneIDs).values_list(
        'spot_id', flat=True)


def queryGO(experiment_name, query):
    GO_ID = query

    genelist = list(GeneGO.objects.filter(term_accession=GO_ID).values_list('geneinfo__gene_id', flat=True))

    genelist = set(genelist)

    genelist = queryGenesForExperiment(experiment_name, ' '.join(genelist))
    return genelist


def network(request):
    return render(request, 'network.html')
