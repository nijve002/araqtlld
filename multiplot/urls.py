'''
Created on Jan 26, 2016

@author: harm nijveen
'''

from django.urls import include, re_path
from multiplot.views import multiplot, network
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = [
    re_path(r'^$', multiplot),
    re_path(r'^network/', network, name='network'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
