"""araqtl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, re_path, include
from django.conf.urls.static import static

urlpatterns = [
    re_path(r'^$', include('main.urls')),
    re_path(r'^main/', include('main.urls')),
    re_path(r'^cistrans/', include('cistrans.urls')),
    re_path(r'^coregulation/', include('coregulation.urls')),
    re_path(r'^correlation/', include('correlation.urls')),
    re_path(r'^multiplot/', include('multiplot.urls')),
    re_path(r'^suggestions/', include('suggestions.urls')),
    re_path(r'^download/', include('download.urls')),
    re_path(r'^arald/', include('arald.urls')),
    re_path(r'^traitdata/', include('traitdata.urls')),
]
