from django.http import JsonResponse, Http404

from django.db.models import Q

from main.models import Experiment, ArraySpot, Marker, GeneInfo, Metabolite, Phenotype, Phenotype_Experiment, Metabolite_Experiment
from main.utils import get_species_for_experiment, get_lodscores_for_trait_experiment

import urllib
import pandas as pd

def index(request):
    if request.method is not 'GET' and \
            'query' not in request.GET and 'experiment_name' not in request.GET:
            raise Http404("page does not exist")

    experiment_name = request.GET.get('experiment_name')

    try:
        experiment = Experiment.objects.get(name=experiment_name)
    except Experiment.DoesNotExist:
        raise Http404("Experiment does not exist")

    query = request.GET.get('query')
    trait = urllib.parse.unquote(query).strip()

    output_dict = {}
    output_dict["experiment"] = experiment_name
    
    if ArraySpot.objects.filter(spot_id=query, experiment__name=experiment_name).count() == 1:
        species_name = get_species_for_experiment(experiment_name)
        geneInfo = GeneInfo.objects.get(gene_id=query, species__species_name=species_name)
        output_dict["type"] = "gene"
        output_dict["ID"] = geneInfo.gene_id
        output_dict["name"] = geneInfo.gene_name
        output_dict["chr"] = geneInfo.chr
        output_dict["start"] = geneInfo.start
        output_dict["end"] = geneInfo.end
    elif Metabolite_Experiment.objects.filter(experiment__name=experiment_name,metabolite__name__iexact=trait).count() == 1:
        metabolites = Metabolite.objects.filter(name__iexact=trait)
        metabolite = metabolites[0]
        output_dict["type"] = "metabolite"
        output_dict["ID"] = metabolite.name
        output_dict["description"] = metabolite.description
    elif Phenotype_Experiment.objects.filter(experiment__name=experiment_name,phenotype__name__iexact=trait).count() == 1:
        phenotypes = Phenotype.objects.filter(name__iexact=trait)
        phenotype = phenotypes[0]
        output_dict["type"] = "phenotype"
        output_dict["ID"] = phenotype.name
        output_dict["description"] = phenotype.description
    else:
        raise Http404("trait experiment combination does not exist")
            
    lod_score = get_lodscores_for_trait_experiment(experiment_name,trait)
    marker_list = list(Marker.objects.filter(experiment__name=experiment_name).values('name','chromosome__name','start'))
    marker_df = pd.DataFrame(marker_list)
    marker_df.columns = ['name','chr','start']
    marker_df = marker_df.set_index('name')
    lod_marker = marker_df.merge(lod_score.T, left_index=True, right_index=True)
    lod_marker.to_dict(orient='index')
    output_dict["lodscores"] = lod_marker.to_dict(orient='index')

    return JsonResponse(output_dict)


def lod_scores(request):
    if request.method is not 'GET' and \
            'query' not in request.GET and 'experiment_name' not in request.GET:
            raise Http404("page does not exist")

    experiment_name = request.GET.get('experiment_name')

    try:
        experiment = Experiment.objects.get(name=experiment_name)
    except Experiment.DoesNotExist:
        raise Http404("Experiment does not exist")

    query = request.GET.get('query')
    trait = urllib.parse.unquote(query).strip()

    if ArraySpot.objects.filter(spot_id=query, experiment__name=experiment_name).count() != 1:
            raise Http404("trait experiment combination does not exist")
    
    lods = get_lodscores_for_trait_experiment(experiment_name,trait)

    species_name = get_species_for_experiment(experiment_name)
    geneInfo = GeneInfo.objects.get(gene_id=query, species__species_name=species_name)

    output_dict = {}
    output_dict["probe"] = geneInfo.gene_id
    output_dict["name"] = geneInfo.gene_name

    output_dict["lod"] = lods.values.flatten().tolist()[1:]

    return JsonResponse(output_dict)