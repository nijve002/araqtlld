'''
Created on Mei 22, 2021

@author: harm nijveen
'''

from django.urls import include, re_path
from traitdata.views import index, lod_scores
from django.conf import settings
from django.conf.urls.static import static
 
urlpatterns = [
    re_path(r'^$', index),
    re_path(r'^lod$', lod_scores),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
        
